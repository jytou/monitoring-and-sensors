# Monitoring and Sensors

This project aims to monitor many sensors in real-time while keeping a record of what happened in the past:
- computers and their interfaces, disks, temperatures, load, state of databases and software mirrors, etc.
- UPS status,
- be notified when some running programs crashed,
- sensors such as Zigbee sensors,
- smartplugs and such.

This started as a personal project back in 2014, and is still in development today.

Data is stored in a MySQL database. Additionally, a simple web interface also exists and will be published in a separate project.

You can find a sample configuration file in res/conf-sample.json.

The project lacks documentation so far, I'll add some if anyone is interested.