-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 24, 2020 at 11:47 PM
-- Server version: 10.1.44-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `healthwatch`
--

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE `actions` (
  `cur_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `disabled` tinyint(4) NOT NULL COMMENT '1 enabled, 0 disabled',
  `idwatch` int(11) NOT NULL COMMENT 'which card'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `measurement`
--

CREATE TABLE `measurement` (
  `id_watch` int(11) NOT NULL,
  `cur_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `prefs`
--

CREATE TABLE `prefs` (
  `name` varchar(35) NOT NULL,
  `double_value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `source`
--

CREATE TABLE `source` (
  `loc_short` varchar(1) NOT NULL,
  `loc_long` varchar(35) NOT NULL,
  `disabled` int(11) NOT NULL COMMENT '1=disabled, 0=enabled'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `threshold`
--

CREATE TABLE `threshold` (
  `id_watch` int(11) NOT NULL,
  `th_type` int(11) NOT NULL,
  `thresh` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `watch`
--

CREATE TABLE `watch` (
  `id` int(11) NOT NULL,
  `sensor_type` varchar(100) DEFAULT NULL COMMENT 'Full HR type of sensor',
  `st` varchar(35) DEFAULT NULL COMMENT 'Short name for sensor type',
  `loc_short` varchar(35) DEFAULT NULL COMMENT 'Location (machine alias)',
  `loc_long` varchar(100) DEFAULT NULL COMMENT 'Full machine name or Home for sensor stuff',
  `instance_short` varchar(35) DEFAULT NULL COMMENT 'A unique instance name at this location',
  `instance_long` varchar(100) DEFAULT NULL COMMENT 'Unique instance name for the location',
  `shortname` varchar(35) NOT NULL COMMENT 'full short name',
  `longname` text NOT NULL COMMENT 'full long name',
  `sensorname` varchar(35) DEFAULT NULL COMMENT 'Name of the sensor device (which possibly has multiple functions)',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=enabled, 1=disabled',
  `curval` double DEFAULT NULL COMMENT 'current value',
  `last_updated` timestamp NULL DEFAULT NULL COMMENT 'last seen',
  `interval_warn` int(11) DEFAULT NULL,
  `warn_min` double DEFAULT NULL,
  `warn_max` double DEFAULT NULL,
  `bound_min` double DEFAULT NULL,
  `bound_max` double DEFAULT NULL,
  `factor1` double DEFAULT NULL,
  `offset2` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `measurement`
--
ALTER TABLE `measurement`
  ADD KEY `id_watch` (`id_watch`,`cur_time`),
  ADD KEY `cur_time` (`cur_time`);

--
-- Indexes for table `prefs`
--
ALTER TABLE `prefs`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `watch`
--
ALTER TABLE `watch`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shortname` (`shortname`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `watch`
--
ALTER TABLE `watch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;