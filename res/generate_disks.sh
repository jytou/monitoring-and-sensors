#!/bin/bash
for disk in {a..z} ; do smartctl -a /dev/sd$disk|egrep -A1 "^Device Model:"|awk -F: '{print $2}'|awk '{$1=$1;print}'|awk '{printf (NR%2==0) ? "\"serialNumber\":\"" $0 "\", \"instanceLong\": \"xxx\"},\n" : "\t\t\t{\"short\":\"ndxxx\", \"name\":\"" $0 "\", "}' ; done
