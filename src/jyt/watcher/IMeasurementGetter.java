package jyt.watcher;

import java.util.Map;

public interface IMeasurementGetter
{
	/**
	 * @return interval (in seconds) at which this should be refreshed
	 */
	Integer getDefaultTemporization();
//	SensorType[] getSensorTypes();
	Map<String, Measurement> getData() throws GetterException;
	void disconnect() throws GetterException;
	void reconnect() throws GetterException;
}
