package jyt.watcher.aggregate;

public class AggregateComputer
{
	/**
	 * Returns the absolute humidity for the given relative humidity and temperature
	 * Source <a href="https://carnotcycle.wordpress.com/2012/08/04/how-to-convert-relative-humidity-to-absolute-humidity/">here</a>
	 * @param pRelativeHumidity
	 * @param pTemperature
	 * @return
	 */
	public static double computeAbsoluteHumidity(double pRelativeHumidity, double pTemperature)
	{
		return 6.112*Math.exp((17.67*pTemperature)/(pTemperature+243.5))*pRelativeHumidity*2.1674/(273.15+pTemperature);
	}

	public static void main(String[] args)
	{
		final double ah1 = computeAbsoluteHumidity(94, 6.2);
		final double ah2 = computeAbsoluteHumidity(55, 21.6);
		System.out.println("ah1=" + ah1 + ", ah2=" + ah2);
	}
}
