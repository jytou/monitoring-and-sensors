package jyt.watcher;

import jyt.watcher.MeasurementListenerOverheating.Card;

public interface IFeedbackListener extends IMeasurementListener
{
	void measurementSavedOk(int pUnsavedSize);
	void measurementSavedError(int pUnsavedSize, String pMessage, Throwable pThrowable);
	void stateSaveSuccessful();
	void stateSaveError(String pError, Throwable pThrowable);
	void setOverheatingListener(MeasurementListenerOverheating pOverheatingListener);
	void enablingCard(int pCard);
	void disablingCard(int pCard);
	void ignoringCard(int pCard);
	void changeCardStateError(Card pCard, String pMessage, Throwable pThrowable);
	void preferencesReadOk();
	void preferencesReadError(String pMessage, Throwable pThrowable);
	void readCGMinerError(String pMessage, Throwable pThrowable);
	void readCGMinerOK();
	void readGPUInfoOk(int pCardNb);
	void readGPUInfoError(int pCardNb, String pMessage, Throwable pThrowable);
	void mirrorStateOK();
	void mirrorStateDegraded(String pDegraded);
}
