package jyt.watcher;

public class GetterException extends Exception
{
	private static final long serialVersionUID = -2131675718247044099L;

	public GetterException(Throwable pThrowable)
	{
		super(pThrowable);
	}

	public GetterException(String pError)
	{
		super(pError);
	}
}
