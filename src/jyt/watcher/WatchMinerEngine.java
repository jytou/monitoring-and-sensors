package jyt.watcher;

import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WatchMinerEngine
{
	public static final String SENSORS_HOME_LONG = "Home";
	public static final String SENSORS_HOME_SHORT = "h";

	public static final String POWER_PLUG_MEROSS_LONG = "Power";
	public static final String POWER_PLUG_MEROSS_SHORT = "w";

	public static final String WATER_HEATER_LONG = "HeaterTemp";
	public static final String WATER_HEATER_SHORT = "t";

	private IMeasurementGetter[] mGetters;
	private List<IMeasurementListener> mListeners = new ArrayList<IMeasurementListener>();
	private IFeedbackListener mFeedbackListener;

	public WatchMinerEngine(IFeedbackListener pFeedbackListener, IMeasurementGetter[] pGetters)
	{
		super();
		mGetters = pGetters;
		mFeedbackListener = pFeedbackListener;
	}

	public void watch(Config pConf) throws GetterException, UnknownHostException, ClassNotFoundException, InterruptedException, SQLException
	{
		try
		{
//			final StateSaver stateSaver = StateSaver.getStateSaver(mFeedbackListener);
			for (IMeasurementListener listener : pConf.getListeners())
			{
				if (listener instanceof MeasurementListenerSaver)
				{
					final MeasurementListenerSaver saver = (MeasurementListenerSaver)listener;
					if (!Watched.sTestMode)
						mListeners.add(saver);
				}
				else
					mListeners.add(listener);
			}
			mListeners.add(mFeedbackListener);

			while (true)
			{
				for (IMeasurementGetter iMeasurementGetter : mGetters)
				{
					final Map<String, Measurement> data = getDataForGetter(iMeasurementGetter);
					if (!data.isEmpty())
						for (Measurement measurement : data.values())
							for (IMeasurementListener measurementListener : mListeners)
								measurementListener.found(measurement);
				}
				for (IMeasurementListener measurementListener : mListeners)
					measurementListener.endBatch();
				if (Watched.sTestMode)
					System.exit(0);
				Thread.sleep(30000);
			}
		}
		finally
		{
			System.err.println("THAT IS NOT GOOD!");
		}
	}

	private Map<String, Measurement> getDataForGetter(IMeasurementGetter pGetter)
	{
		Map<String, Measurement> data = new HashMap<String, Measurement>();
		try
		{
			data = pGetter.getData();
		}
		catch (GetterException e)
		{
			try
			{
				pGetter.reconnect();
			}
			catch (GetterException e1)
			{
				e1.printStackTrace();
			}
		}
		return data;
	}

}
