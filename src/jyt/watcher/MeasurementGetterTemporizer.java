package jyt.watcher;

import java.util.HashMap;
import java.util.Map;

public class MeasurementGetterTemporizer implements IMeasurementGetter
{
	private IMeasurementGetter mInternalGetter;
	private long mLastMeasurement = -1;
	private long mInterval;

	public MeasurementGetterTemporizer(IMeasurementGetter pInternalGetter, long pIntervalSeconds)
	{
		super();
		mInternalGetter = pInternalGetter;
		mInterval = pIntervalSeconds * 1000;
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		final Map<String, Measurement> res = new HashMap<>();
		final long currentTimeMillis = System.currentTimeMillis();
		if (mLastMeasurement + mInterval <= currentTimeMillis)
		{
			res.putAll(mInternalGetter.getData());
			mLastMeasurement = currentTimeMillis;
		}
		return res;
	}

	public IMeasurementGetter getInternalGetter()
	{
		return mInternalGetter;
	}

	@Override
	public void disconnect() throws GetterException
	{
		mInternalGetter.disconnect();
	}

	@Override
	public void reconnect() throws GetterException
	{
		mInternalGetter.reconnect();
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return null;
	}

}
