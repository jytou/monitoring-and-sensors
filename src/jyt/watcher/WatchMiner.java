package jyt.watcher;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

public class WatchMiner
{
	public static void main(String[] args) throws GetterException, ClassNotFoundException, InterruptedException, SQLException, FileNotFoundException, IOException
	{
		if (args.length > 1)
			Watched.sTestMode = true;
		final IFeedbackListener feedbackListener = new FeedbackListenerSysOutputter();
		final Config conf = new Config(args[0], feedbackListener);
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
			public void run()
			{
				try
				{
					Thread.sleep(200);
					System.out.println("Closing getters...");
					for (IMeasurementGetter getter : conf.getters())
						try
						{
							getter.disconnect();
						}
						catch (GetterException e)
						{
							System.out.println("Could not close getter " + getter.getClass().getName() + " due to exception " + e.getClass().getName() + " saying: \"" + e.getMessage() + "\"");
						}
				}
				catch (InterruptedException e)
				{
					Thread.currentThread().interrupt();
					e.printStackTrace();
				}
			}
		});

		new WatchMinerEngine(feedbackListener, conf.getters()).watch(conf);
	}
}
