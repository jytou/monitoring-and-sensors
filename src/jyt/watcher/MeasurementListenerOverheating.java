package jyt.watcher;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.jcraft.jsch.JSchException;

import jyt.watcher.getters.MeasurementGetterATIGPUCards;

/**
 * Old stuff that I keep around in case - not functioning right now
 * @author jytou
 */
public class MeasurementListenerOverheating implements IMeasurementListener
{
	private StateSaver mStateSaver;

	public static class Card
	{
		String name;
		double lastTemp;
		int nbTemps = 1;
		double[] temperatures = new double[10];
		double total = 0;
		int index = 0;
		double temperature;
		int state;

		public Card(String pName, double pTemperature, int pStatus)
		{
			super();
			name = pName;
			temperature = pTemperature;
			lastTemp = pTemperature;
			Arrays.fill(temperatures, 0);
			temperatures[0] = pTemperature;
			total = pTemperature;
			state = pStatus;
		}

		public int getState()
		{
			return state;
		}

		public String getName()
		{
			return name;
		}

		void addTemperature(double pTemp)
		{
			index++;
			if (index >= temperatures.length)
				index = 0;
			total -= temperatures[index];
			total += pTemp;
			temperatures[index] = pTemp;
			if (nbTemps < temperatures.length)
				nbTemps++;
			temperature = total / nbTemps;
			lastTemp = pTemp;
		}
		@Override
		public String toString()
		{
			String statusStr = "";
			switch (state)
			{
				case Watched.STATE_ENABLED:
					statusStr = "*";
					break;
				case Watched.STATE_DISABLED:
					statusStr = "-";
					break;
				case Watched.STATE_IGNORED:
					statusStr = "?";
					break;
				case Watched.STATE_SICK:
					statusStr = "!";
					break;

				default:
					break;
			}
			return name + statusStr + " (" + temperature + ")";
		}
	}

	public static class CardGroup
	{
		public Map<String, Card> cards = new HashMap<String, Card>();
		Long stabilizingTimestamp = null;
		public double meanTemp;
		public int nbEnabled;
	}

	private static final String[][] sGroups7 = new String[][]
	{
		new String[] {"mgt0", "mgt1", "mgt2"},
		new String[] {"mgt3", "mgt5", "mgt6"},
		new String[] {"mgt4"}
	};

	private static final String[][] sGroups5 = new String[][]
	{
		new String[] {"mgt0"},
		new String[] {"mgt1", "mgt3", "mgt4"},
		new String[] {"mgt2"}
	};

	private static final String[][] sGroups1 = new String[][]
	{
		new String[] {"mgt0"}
	};

	private static final double[][] DEFAULT_SWITCHON_TEMPS = new double[][]
	{
		new double[] {30, 37, 42},
		new double[] {39, 38, 45},
		new double[] {32}
	};

	public static String[][] sGroups;
	static
	{
		try
		{
			final Map<Integer, Double> temps = new MeasurementGetterATIGPUCards().getTemperatures();
			final int nbCards = temps.size() / 2;
			if (nbCards == 7)
				sGroups = sGroups7;
			else if (nbCards == 5)
				sGroups = sGroups5;
			else if (nbCards == 1)
				sGroups = sGroups1;
			else
				throw new RuntimeException("Inconsistent number of cards found: " + nbCards);
		}
		catch (GetterException e)
		{
			throw new RuntimeException(e);
		}
	}

	private CardGroup[] mCardGroups = new CardGroup[sGroups.length];
	private Map<String, CardGroup> mCardToGroup = new HashMap<String, MeasurementListenerOverheating.CardGroup>();
	private IFeedbackListener mFeedbackListener;

	public MeasurementListenerOverheating(StateSaver pStateSaver, IFeedbackListener pFeedbackListener)
	{
		super();
		mStateSaver = pStateSaver;
		mFeedbackListener = pFeedbackListener;
		for (int i = 0; i < mCardGroups.length; i++)
			mCardGroups[i] = new CardGroup();
		for (int i = 0; i < sGroups.length; i++)
			for (int j = 0; j < sGroups[i].length; j++)
				mCardToGroup.put(sGroups[i][j], mCardGroups[i]);
	}

	@Override
	public void found(Measurement pMeasurement)
	{
		Watched watched = Watched.getWatchedById(pMeasurement.getIdWatch());
		if (watched != null)
		{
			CardGroup group = mCardToGroup.get(watched.getShortName());
			if (group != null)
			{
				Card card = group.cards.get(watched.getShortName());
				if (card == null)
					group.cards.put(watched.getShortName(), new Card(watched.getShortName(), pMeasurement.getValue(), watched.getState()));
				else
				{
					card.state = watched.getState();
					card.addTemperature(pMeasurement.getValue());
				}
			}
		}
	}

	private int computeCardNumber(Card pCard)
	{
		return Integer.valueOf(pCard.getName().substring(3));
	}

	@Override
	public void endBatch()
	{
		final int stabilizingTime = (int) (Preferences.getPreferences(mFeedbackListener).getValue("stabilize", 120.0) * 1000);// stabilizing time
		//final double gpuEnableFactor = Preferences.getPreferences(mFeedbackListener).getValue("gpu_enable_factor", 7.0).doubleValue();
		//double gpuEnableThreshold = Preferences.getPreferences(mFeedbackListener).getValue("gpu_enable_threshold", 30.0).doubleValue();
		double gpuMaxValue = Preferences.getPreferences(mFeedbackListener).getValue("maxtemp", 50.0).doubleValue();// gpu max temp
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		final boolean isDaytime = (hour >= 7) && (hour < 23);
		if (isDaytime)
		// because electricity is expensive daytime, reduce the number of working cards when they get hot
		{
			//gpuEnableThreshold -= 5;
			gpuMaxValue -= 5;
		}
		final double gpuEmergencyThreshold = Preferences.getPreferences(mFeedbackListener).getValue("maxmaxtemp", 55.0).doubleValue();// gpu_emergency_threshold

		for (int i = 0; i < mCardGroups.length; i++)
		{
			CardGroup group = mCardGroups[i];
			int nbDisabled = 0;
			int nbEnabled = 0;
			double meanTemp = 0;
			for (Card card : group.cards.values())
			{
				if ((card.state == Watched.STATE_IGNORED) || (card.state == Watched.STATE_SICK))
				{
					nbDisabled++;
				}
				else
				{
					if ((card.state == Watched.STATE_ENABLED) && ((card.lastTemp > gpuEmergencyThreshold) || // emergency shutdown
						(card.temperature >= gpuMaxValue && ((group.stabilizingTimestamp == null) || (System.currentTimeMillis() - group.stabilizingTimestamp.longValue() > stabilizingTime))))) // wait for 2 minutes to stabilize temp
						changeCardStatus(group, card, Watched.STATE_DISABLED);
					if (card.state == Watched.STATE_ENABLED)
						nbEnabled++;
					else
						nbDisabled++;
				}
				meanTemp += card.temperature;
			}
			meanTemp /= group.cards.size();
			if ((nbDisabled > 0) && ((group.stabilizingTimestamp == null) || (System.currentTimeMillis() - group.stabilizingTimestamp.longValue() > stabilizingTime)))
			{
				double switchonTemp = Preferences.getPreferences(mFeedbackListener).getValue("etg" + i + "" + nbEnabled, DEFAULT_SWITCHON_TEMPS[i][nbEnabled]);
				if (isDaytime)
					switchonTemp -= 5;
				if (meanTemp < switchonTemp)
				// we have to switch on one card for this group
				{
					// look for the coolest disabled one
					Card coolestDisabled = null;
					for (Card card : group.cards.values())
					{
						if ((card.state != Watched.STATE_IGNORED) && (card.state != Watched.STATE_SICK))
							if ((card.state == Watched.STATE_DISABLED) && ((coolestDisabled == null) || (coolestDisabled.temperature > card.temperature)))
								coolestDisabled = card;
					}
					if (coolestDisabled != null)
					{
						changeCardStatus(group, coolestDisabled, Watched.STATE_ENABLED);
						nbEnabled++;
						nbDisabled--;
					}
				}
			}
			group.meanTemp = meanTemp;
			group.nbEnabled = nbEnabled;
		}
	}

	public CardGroup[] getCardGroups()
	{
		return mCardGroups;
	}

	public void ignoreCard(Watched pWatched, boolean pIgnore)
	{
		CardGroup group = mCardToGroup.get(pWatched.getShortName());
		Card card = group.cards.get(pWatched.getShortName());
		changeCardStatus(group, card, pIgnore ? Watched.STATE_IGNORED : Watched.STATE_DISABLED);
	}

	public void enableCard(Watched pWatched, boolean pEnable)
	{
		CardGroup group = mCardToGroup.get(pWatched.getShortName());
		Card card = group.cards.get(pWatched.getShortName());
		changeCardStatus(group, card, pEnable ? Watched.STATE_ENABLED : Watched.STATE_DISABLED);
	}

	private synchronized void changeCardStatus(CardGroup group, Card pCard, int pState)
	{
		group.stabilizingTimestamp = System.currentTimeMillis();
		try
		{
			switchCard(pCard, pState);
			pCard.state = pState;
			final Watched watched = Watched.getWatchedByShortName(pCard.name);
			watched.setState(pState);
			mStateSaver.toSave(watched);
			switch (pState)
			{
				case Watched.STATE_IGNORED:
				case Watched.STATE_SICK:
				case Watched.STATE_DISABLED:
					mFeedbackListener.disablingCard(computeCardNumber(pCard));
					break;
				case Watched.STATE_ENABLED:
					mFeedbackListener.enablingCard(computeCardNumber(pCard));
					break;
				default:
					break;
			}
		}
		catch (Exception e)
		{
			mFeedbackListener.changeCardStateError(pCard, "Exception while trying to disable or enable a card", e);
			e.printStackTrace();
		}
	}

	private void switchCard(Card pCard, int pStatus) throws JSchException, IOException, InterruptedException
	{
		synchronized (JSchConnector.class)
		{
//			System.out.println("Connecting for switch card");
			Thread.sleep(1000);
			JSchConnector jSchConnector = new JSchConnector("user", "localhost", "pass");
			final BufferedWriter writer = jSchConnector.getWriter();
			jSchConnector.writeLine("screen -x gpu");
			Thread.sleep(100);
			jSchConnector.sendChar('g');
			Thread.sleep(100);
			jSchConnector.sendChar(pStatus == Watched.STATE_ENABLED ? 'e' : 'd');
			Thread.sleep(100);
			int number = Integer.valueOf(pCard.name.substring(3));
			jSchConnector.sendChar(String.valueOf(number).charAt(0));
			jSchConnector.sendChar('\n');
			Thread.sleep(100);
			jSchConnector.sendChar(' ');
			Thread.sleep(100);
			jSchConnector.sendChar('\u0001');
			Thread.sleep(100);
			jSchConnector.sendChar('\u0004');
			Thread.sleep(100);
			jSchConnector.sendChar('\u0004');
			Thread.sleep(100);
			jSchConnector.close();
//			System.out.println("Disconnecting for switch card");
		}
	}
}
