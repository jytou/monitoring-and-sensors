package jyt.watcher;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnector
{
	private static String JDBC_DRIVER = "org.mariadb.jdbc.Driver";
	private static String JDBC_PASS = null;
	private static String JDBC_USER = null;
	private static String JDBC_URL = null;

	public static Connection connect() throws ClassNotFoundException, SQLException
	{
		Class.forName(JDBCConnector.JDBC_DRIVER);
		return DriverManager.getConnection(JDBCConnector.JDBC_URL, JDBCConnector.JDBC_USER, JDBCConnector.JDBC_PASS);
	}

	public static void init(String pUrl, String pUser, String pPass)
	{
		JDBC_PASS = pPass;
		JDBC_URL = pUrl;
		JDBC_USER = pUser;
	}
}
