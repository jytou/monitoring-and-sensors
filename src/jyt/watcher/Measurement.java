package jyt.watcher;

import java.io.Serializable;
import java.sql.Timestamp;

public class Measurement implements Serializable
{
	private int mIdWatch;
	private Timestamp mTimestamp;
	private double mValue;

	public Measurement(int pIdWatch, Timestamp pTimestamp, double pValue)
	{
		super();
		mIdWatch = pIdWatch;
		mTimestamp = pTimestamp;
		mValue = pValue;
	}

	public Measurement(String pWatchShortName, double pValue)
	{
		this(Watched.getWatchedByShortName(pWatchShortName).getId(), getCurrentTimeStamp(), pValue);
	}

	public int getIdWatch()
	{
		return mIdWatch;
	}

	public Timestamp getTimestamp()
	{
		return mTimestamp;
	}

	public double getValue()
	{
		return mValue;
	}

	public static Timestamp getCurrentTimeStamp()
	{
		return new Timestamp(System.currentTimeMillis());
	}

	@Override
	public String toString()
	{
		return mTimestamp.toString() + ": " + Watched.getWatchedById(mIdWatch).getShortName() + " " + mValue;
	}
}
