package jyt.watcher;

import java.sql.SQLException;

import com.eclipsesource.json.JsonObject;

public abstract class MeasurementGetterAbstract implements IMeasurementGetter
{
	protected String mShortHost;
	protected String mLongHost;
	protected String mShortName;
	protected String mLongName;
	protected String mSensorName;
	protected String mTypeLong;
	protected String mTypeShort;
	protected String mInstanceLong;
	protected String mInstanceShort;
	protected Integer mIntervalWarn;
	protected Double mBoundMin;
	protected Double mBoundMax;
	protected Double mWarnMin;
	protected Double mWarnMax;
	protected Double mFactor1;
	protected Double mOffset2;

	public MeasurementGetterAbstract(String pLongHost, String pShortHost, JsonObject pConfig, String pSensorTypeLong, String pSensorTypeShort)
	{
		super();
		mShortHost = pShortHost;
		mLongHost = pLongHost;
		mInstanceShort = pConfig.contains("instanceShort") ? pConfig.get("instanceShort").asString() : null;
		mInstanceLong = pConfig.contains("instanceLong") ? pConfig.get("instanceLong").asString() : mInstanceShort;
		mShortName = computeShortName(pConfig, pSensorTypeShort, mInstanceShort);
		mLongName = computeLongName(pConfig, pSensorTypeLong, mInstanceLong);
		mSensorName = computeSensorName(pConfig);
		mTypeLong = pSensorTypeLong;
		mTypeShort = pSensorTypeShort;
		mFactor1 = pConfig.contains("factor1") ? pConfig.get("factor1").asDouble() : null;
		mOffset2 = pConfig.contains("offset2") ? pConfig.get("offset2").asDouble() : null;
		setListenProperties(pConfig, null, null, null, null, null);
	}

	protected void setListenProperties(JsonObject pConfig, Integer pIntervalWarn, Double pBoundMin, Double pBoundMax, Double pWarnMin, Double pWarnMax)
	{
		if (pConfig.contains("intervalWarn"))
			mIntervalWarn = pConfig.getInt("intervalWarn", -1);
		if (pConfig.contains("min"))
			mBoundMin = pConfig.get("min").asDouble();
		else
			mBoundMin = pBoundMin;
		if (pConfig.contains("max"))
			mBoundMax = pConfig.get("max").asDouble();
		else
			mBoundMax = pBoundMax;
		if (pConfig.contains("minWarn"))
			mWarnMin = pConfig.get("minWarn").asDouble();
		else
			mWarnMin = pWarnMin;
		if (pConfig.contains("maxWarn"))
			mWarnMax = pConfig.get("maxWarn").asDouble();
		else
			mWarnMax = pWarnMax;
	}

	protected String computeSensorName(final JsonObject pConfig)
	{
		if (pConfig.contains("name"))
			return pConfig.get("name").asString();
		return null;
	}

	protected String computeLongName(final JsonObject pConfig, final String pSensorTypeLong, final String pInstanceLong)
	{
		if (pConfig.contains("long"))
			return pConfig.get("long").asString();
		return mLongHost + " " + pSensorTypeLong + (pInstanceLong == null ? "" : " " + pInstanceLong);
	}

	protected String computeShortName(final JsonObject pConfig, final String pSensorTypeShort, final String pInstanceShort)
	{
		if (pConfig.contains("short"))
			return pConfig.get("short").asString();
		return mShortHost + pSensorTypeShort + (pInstanceShort == null ? "" : pInstanceShort);
	}

	protected void registerDefault() throws SQLException
	{
		Watched.registerWatch(mShortName, mLongName, mSensorName, mLongHost, mShortHost, mTypeLong, mTypeShort, mInstanceLong, mInstanceShort, mIntervalWarn, mBoundMin, mBoundMax, mWarnMin, mWarnMax, mFactor1, mOffset2);
	}

	protected void registerDefault(final String pInstanceLong, final String pInstanceShort) throws SQLException
	{
		Watched.registerWatch(mShortName, mLongName, mSensorName, mLongHost, mShortHost, mTypeLong, mTypeShort, pInstanceLong, pInstanceShort, mIntervalWarn, mBoundMin, mBoundMax, mWarnMin, mWarnMax, mFactor1, mOffset2);
	}

	@Override
	public Integer getDefaultTemporization()
	{
		// By default, no temporization, it will be overriden if necessary
		return null;
	}
}
