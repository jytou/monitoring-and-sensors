package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;
import jyt.watcher.StringUtil;
import jyt.watcher.Watched;

/**
 * This requires sudo permissions for smartctl.
 * @author jytou
 */
public class MeasurementGetterNVME extends MeasurementGetterAbstract
{
	private Map<String, String> mKnownHDD = new HashMap<>();
	private Set<String> mToIgnore = new HashSet<>();
	private char[] mDisks;

	public MeasurementGetterNVME(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "NVME", "dt");
		if (pConfig.contains("ignore"))
			mToIgnore.addAll(Arrays.asList(StringUtil.split(pConfig.get("ignore").asString(), ",")));
		mDisks = pConfig.get("disks").asString().toCharArray();
		final JsonArray hdds = pConfig.get("hdds").asArray();
		for (JsonValue jsonValue : hdds)
		{
			final JsonObject sensorObject = jsonValue.asObject();
			if (sensorObject.contains("disabled") && sensorObject.getBoolean("disabled", true))
			{
				// add to toignore
				if (sensorObject.contains("serialNumber"))
					mToIgnore.add(sensorObject.get("serialNumber").asString());
				else if (sensorObject.contains("name"))
					mToIgnore.add(sensorObject.get("name").asString());
			}
			else
			{
				final String shortName = sensorObject.get("short").asString();
				final Integer intervalWarn = sensorObject.contains("intervalWarn") ? sensorObject.get("intervalWarn").asInt() : null;
				final Double boundMin = sensorObject.contains("min") ? sensorObject.get("min").asDouble() : null;
				final Double boundMax = sensorObject.contains("max") ? sensorObject.get("max").asDouble() : null;
				final Double warnMin = sensorObject.contains("minWarn") ? sensorObject.get("minWarn").asDouble() : null;
				final Double warnMax = sensorObject.contains("maxWarn") ? sensorObject.get("maxWarn").asDouble() : null;
				final String sensorName = sensorObject.get("name").asString();
				if (sensorObject.contains("serialNumber"))
					mKnownHDD.put(sensorName + " / " + sensorObject.get("serialNumber").asString(), shortName);
				else
					mKnownHDD.put(sensorName, shortName);
				final String sensorTypeLong = sensorObject.contains("typeLong") ? sensorObject.get("typeLong").asString() : "HDD Temperature";
				final String sensorTypeShort = sensorObject.contains("typeShort") ? sensorObject.get("typeShort").asString() : "dt";
				final String sensorInstanceLong = sensorObject.contains("instanceLong") ? sensorObject.get("instanceLong").asString() : sensorName;
				final String sensorInstanceShort = sensorObject.contains("instanceShort") ? sensorObject.get("instanceShort").asString() : shortName;
				final String longName = sensorObject.contains("long") ? sensorObject.get("long").asString() : pLongHost + " " + sensorTypeLong + " " + sensorInstanceLong;
				Watched.registerWatch(shortName, longName, sensorName, pLongHost, pShortHost, sensorTypeLong, sensorTypeShort, sensorInstanceLong, sensorInstanceShort, intervalWarn, boundMin, boundMax, warnMin, warnMax, null, null);
			}
		}
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		for (char diskChar : mDisks)
		{
			try
			{
				final Process p = Runtime.getRuntime().exec("sudo /usr/sbin/smartctl /dev/nvme" + diskChar + " -a");
				final InputStream is = p.getInputStream();
				final BufferedReader r = new BufferedReader(new InputStreamReader(is));
				final List<String> errors;
				try
				{
					String l;
					errors = new ArrayList<>();
					String shortName = null;
					String longName = null;
					while ((l = r.readLine()) != null)
					{
						if (l.startsWith("Model Number:"))
						{
							longName = l.substring("Model Number: ".length()).trim();
							if (mToIgnore.contains(longName))
								break;
							if (mKnownHDD.containsKey(longName))
								shortName = mKnownHDD.get(longName);
						}
						else if (l.startsWith("Serial Number:"))
						{
							final String serNum = l.substring("Serial Number: ".length()).trim();
							if (mToIgnore.contains(serNum))
								break;
							if ((shortName == null) && (longName != null))
							{
								final String fullName = longName + " / " + serNum;
								if (mKnownHDD.containsKey(fullName))
									shortName = mKnownHDD.get(fullName);
							}
							if (shortName == null)
								System.err.println("Unknown disk: " + longName + " / " + serNum);
						}
						else if ((l.startsWith("Temperature") && l.endsWith("Celsius")) && (shortName != null))
						{
							final StringTokenizer st = new StringTokenizer(l, " ", false);
							st.nextToken();
							final String temp = st.nextToken();
							try
							{
								final int temperature = Integer.parseInt(temp);
								res.put(shortName, new Measurement(shortName, temperature));
								break;
							}
							catch (NumberFormatException e)
							{
								errors.add("Could not parse temp " + temp + " for disk " + shortName);
							}
						}
					}
				}
				finally
				{
					r.close();
					p.destroy();
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
//		if (!errors.isEmpty())
//			throw new GetterException(errors.toString());
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 60*5;
	}
}
