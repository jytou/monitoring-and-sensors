package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;
import jyt.watcher.Watched;

public class MeasurementGetterNvidia extends MeasurementGetterAbstract
{
	private static final String SENSOR_TYPE_LONG = "Nvidia";
	private static final String SENSOR_TYPE_SHORT = "nv";

	private final static String FAN_SHORT = "gf";
	private final static String TEMP_SHORT = "gt";
	private final static String POWER_SHORT = "gw";
	private final static String MEM_USED_SHORT = "gm";
	private final static String LOAD_SHORT = "gl";

	private final static String FAN_LONG = "GPU Fan";
	private final static String TEMP_LONG = "Temperature";
	private final static String POWER_LONG = "Power";
	private final static String MEM_USED_LONG = "Used Memory";
	private final static String LOAD_LONG = "GPU Load";

	private final static Map<String, String> ALL_SENSORS = new HashMap<>();
	static
	{
		ALL_SENSORS.put(FAN_SHORT, FAN_LONG);
		ALL_SENSORS.put(TEMP_SHORT, TEMP_LONG);
		ALL_SENSORS.put(POWER_SHORT, POWER_LONG);
		ALL_SENSORS.put(MEM_USED_SHORT, MEM_USED_LONG);
		ALL_SENSORS.put(LOAD_SHORT, LOAD_LONG);
	}

	// [card number, card short name]
	private Map<String, String> mSupportedCardNumbers = new HashMap<>();

	public MeasurementGetterNvidia(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, SENSOR_TYPE_LONG, SENSOR_TYPE_SHORT);
		for (JsonValue card : pConfig.get("cards").asArray())
		{
			final JsonObject cardObject = card.asObject();
			final String cardNumber = cardObject.get("number").asString();
			final String instanceShort = cardObject.contains("instanceShort") ? cardObject.get("instanceShort").asString() : cardNumber;
			final String instanceLong = cardObject.contains("instanceLong") ? cardObject.get("instanceLong").asString() : cardNumber;
			mSupportedCardNumbers.put(cardNumber, instanceShort);
			final Integer intervalWarn = cardObject.contains("intervalWarn") ? cardObject.get("intervalWarn").asInt() : null;
			for (Entry<String, String> sensorEntry : ALL_SENSORS.entrySet())
			{
				final String a = sensorEntry.getKey();
				final Double boundMin = cardObject.contains("min."+a) ? cardObject.get("min."+a).asDouble() : null;
				final Double boundMax = cardObject.contains("max."+a) ? cardObject.get("max."+a).asDouble() : null;
				final Double warnMin = cardObject.contains("minWarn."+a) ? cardObject.get("minWarn."+a).asDouble() : null;
				final Double warnMax = cardObject.contains("maxWarn."+a) ? cardObject.get("maxWarn."+a).asDouble() : null;
				final String sensorTypeLong = sensorEntry.getValue();
				final String sensorTypeShort = sensorEntry.getKey();
				Watched.registerWatch(mShortHost + SENSOR_TYPE_SHORT + instanceShort + sensorTypeShort, mLongHost + " " + SENSOR_TYPE_LONG + " " + instanceLong + " " + sensorTypeLong, mInstanceLong, pLongHost, pShortHost, sensorTypeLong, sensorTypeShort, instanceLong, instanceShort, intervalWarn, boundMin, boundMax, warnMin, warnMax, null, null);
			}
		}
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		try
		{
			final Process p = Runtime.getRuntime().exec("/usr/bin/nvidia-smi");
			final BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String l;
			boolean inCards = false;
			try
			{
				while ((l = r.readLine()) != null)
				{
					if (!inCards)
					{
						if (l.startsWith("|======"))
							inCards = true;
					}
					else if (l.trim().isEmpty())
						break;
					else
					{
						// first line of the card
						final String cardNumStr = l.substring(1, 5).trim();
						final String cardShortName = mSupportedCardNumbers.get(cardNumStr);
						if (cardShortName != null)
						{
							// next line, read all infos
							l = r.readLine().replaceAll("[\\/\\|]", " ");
//							System.out.println(l);
							final String[] elems = l.split("( )+");
//							System.out.println(Arrays.toString(elems));
							final String fanPercentStr = elems[1];
							final int fanPercent = Integer.parseInt(fanPercentStr.substring(0, fanPercentStr.length() - 1));
							final String tempStr = elems[2];
							final int temp = Integer.parseInt(tempStr.substring(0, tempStr.length() - 1));
							final String powerStr = elems[4];
							final int power = Integer.parseInt(powerStr.substring(0, powerStr.length() - 1));
							final String memUsedStr = elems[6];
							final int memUsed = Integer.parseInt(memUsedStr.substring(0, memUsedStr.length() - 3));
							final String totalMemStr = elems[7];
							final int totalMem = Integer.parseInt(totalMemStr.substring(0, totalMemStr.length() - 3));
							final String loadStr = elems[8];
							final int load = Integer.parseInt(loadStr.substring(0, loadStr.length() - 1));
							final String shortName = mShortHost + SENSOR_TYPE_SHORT + cardShortName;
							res.put(shortName + FAN_SHORT,      new Measurement(shortName + FAN_SHORT,      fanPercent));
							res.put(shortName + TEMP_SHORT,     new Measurement(shortName + TEMP_SHORT,     temp));
							res.put(shortName + POWER_SHORT,    new Measurement(shortName + POWER_SHORT,    power));
							res.put(shortName + MEM_USED_SHORT, new Measurement(shortName + MEM_USED_SHORT, 100.0 * memUsed / totalMem));
							res.put(shortName + LOAD_SHORT,     new Measurement(shortName + LOAD_SHORT,     load));
							// read the separator to get ready for the next card
							while (!r.readLine().startsWith("+---"));
						}
						else
						{
							// skip other lines
							r.readLine();
							r.readLine();
						}
					}
				}
			}
			finally
			{
				r.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 2*60;
	}
}
