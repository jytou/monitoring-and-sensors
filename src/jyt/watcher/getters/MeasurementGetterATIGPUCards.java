package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import jyt.watcher.GetterException;
import jyt.watcher.IMeasurementGetter;
import jyt.watcher.Measurement;

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * WARNING: Currently not operational
 * @author jytou
 */
public class MeasurementGetterATIGPUCards implements IMeasurementGetter
{
	private static final int FIRST_GPU_LOAD_ID = 43;

	public Map<Integer, Double> getTemperatures() throws GetterException
	{
		Map<Integer, Double> res = new HashMap<Integer, Double>();
		try
		{
			Process p = Runtime.getRuntime().exec("aticonfig --odgt --odgc --adapter=ALL");
			try
			{
				InputStream is = p.getInputStream();
				BufferedReader r = new BufferedReader(new InputStreamReader(is));
				try
				{
					int curAdapter = -1;
					String l;
					while ((l = r.readLine()) != null)
					{
						if (l.startsWith("Adapter "))
							try
							{
								curAdapter = Integer.parseInt(l.substring(8, 9));
							}
							catch (NumberFormatException e)
							{
								throw new GetterException(e);
							}
						else if (l.trim().startsWith("Sensor"))
							try
							{
								res.put(curAdapter, Double.parseDouble(l.substring(l.indexOf('-') + 1, l.indexOf('C'))));
							}
							catch (NumberFormatException e)
							{
								e.printStackTrace();
							}
						else if (l.trim().startsWith("GPU load"))
							try
							{
								res.put(curAdapter + FIRST_GPU_LOAD_ID, Double.parseDouble(l.substring(l.indexOf(':') + 1, l.indexOf('%')).trim()));
							}
							catch (NumberFormatException e)
							{
								e.printStackTrace();
							}
					} 
				}
				finally
				{
					r.close();
				} 
			}
			finally
			{
				p.destroy();
			}
		}
		catch (IOException e)
		{
		}
		return res;
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		final Map<Integer, Double> temps = getTemperatures();
		final Map<String, Measurement> measurements = new HashMap<String, Measurement>(temps.size());
		for (Integer integer : temps.keySet())
		{
			final String name = integer.intValue() > 6 ? "mgl" + (integer.intValue() - FIRST_GPU_LOAD_ID) : "mgt" + integer;
			measurements.put(name, new Measurement(name, temps.get(integer)));
		}
		return measurements;
	}

	@Override
	public void disconnect() throws GetterException
	{
		
	}

	@Override
	public void reconnect() throws GetterException
	{
		
	}

	@Override
	public Integer getDefaultTemporization()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
