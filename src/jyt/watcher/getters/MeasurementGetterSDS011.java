package jyt.watcher.getters;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import javax.naming.CommunicationException;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import com.fazecast.jSerialComm.SerialPortInvalidPortException;

import jyt.driver.sds011.SDS011CommunicationException;
import jyt.driver.sds011.SDS011Driver;
import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;
import jyt.watcher.Watched;

public class MeasurementGetterSDS011 extends MeasurementGetterAbstract
{
	static class SDS011Measurement
	{
		double pm10;
		double pm25;
		LocalDateTime ts = LocalDateTime.now();
	}

	private static final int DEFAULT_TEMPORIZATION_SECONDS = 5*60;
	private static final int NB_SAMPLES = 5;
	private static final String SENSOR_TYPE_LONG = "SDS011";
	private static final String SENSOR_TYPE_SHORT = "sds011";

	private final static String PM25_SHORT = "25";
	private final static String PM10_SHORT = "10";

	private final static String PM25_LONG = "PM2.5";
	private final static String PM10_LONG = "PM10";

	private final static Map<String, String> ALL_SENSORS = new HashMap<>();
	static
	{
		ALL_SENSORS.put(PM25_SHORT, PM25_LONG);
		ALL_SENSORS.put(PM10_SHORT, PM10_LONG);
	}
	// these are synchronized on lock
	Object mLock = new Object();
	// [sensor short name, measurement]
	Map<String, SDS011Measurement> mLatestMeasurements = new HashMap<>();
	// null: last measurement time was consumed, -1: measure NOW!, any other value: the timestamp when the next value will be collected
	Long mNextMeasurementTime = -1L;
	Map<String, MeasuringThread> mCurrentMeasuringThreads = new HashMap<>();

	final static AtomicInteger mMeasuringThreadNumber = new AtomicInteger(0);

	class MeasuringThread extends Thread
	{
		private String mThreadSensorName;
		private int mThreadNumber;
		public MeasuringThread(String pSensorShortName, int pThreadNumber)
		{
			super("Measuring Thread " + pSensorShortName + " " + pThreadNumber);
			mThreadSensorName = pSensorShortName;
			mThreadNumber = pThreadNumber;
			log("Starting");
			synchronized (mLock)
			{
				final MeasuringThread thread = mCurrentMeasuringThreads.get(mThreadSensorName);
				if (thread != null)
					log("!!! WARNING !!!: another thread (#" + thread.mThreadNumber + ") is still running!");
				mCurrentMeasuringThreads.remove(mThreadSensorName);
			}
			start();
		}

		@Override
		public void run()
		{
			super.run();
			try
			{
				while (!interrupted())
				{
					long sleepTime = 100;
					synchronized (mLock)
					{
						if (mNextMeasurementTime == null)
							sleepTime = -1;
						else if (mNextMeasurementTime == -1)
							sleepTime = 0;
						else
						{
							final int oneMeasurementTime = 10000 + NB_SAMPLES * 3000;
							sleepTime = Math.max(0, mNextMeasurementTime - System.currentTimeMillis() - oneMeasurementTime);
							log("Current time: " + System.currentTimeMillis() + ", next measurement: " + mNextMeasurementTime + ", delta: " + (mNextMeasurementTime - System.currentTimeMillis()) + ", sleepTime=" + sleepTime + " ms (one measurement time is " + oneMeasurementTime + ")");
						}
					}
					if (sleepTime == -1)
					{
						try
						{
							log("Waiting for the next measurement time. Sleeping 10s");
							Thread.sleep(10000);
							continue;
						}
						catch (InterruptedException e)
						{
							log("Interrupted");
							return;
						}
					}
					if (sleepTime > 0)
						try
						{
							log("Sleeping " + sleepTime / 1000 + "s");
							Thread.sleep(sleepTime);
						}
						catch (InterruptedException e)
						{
							log("Interrupted");
							return;
						}
					// Start measuring for the sensors
					final String sensorPort = mSensors.get(mThreadSensorName);
					final double[][] vals = new double[NB_SAMPLES][];
					double pm10 = 0;
					double pm25 = 0;
					int tries = 0;
					while (tries++ < 5)
					{
						log("Starting measurement (try " + tries + ")");
						try (final SDS011Driver driver = new SDS011Driver("ttyUSB" + sensorPort))
						{
							driver.setSleepMode(false);
							driver.initQueryMode();
							try
							{
								for (int i = 0; i < NB_SAMPLES; i++)
								{
									Thread.sleep(2000);
									log("Getting sample " + i + "...");
									final double[] values = driver.queryData();
									vals[i] = values;
									pm10 += values[0];
									pm25 += values[1];
								}
								pm10 /= NB_SAMPLES;
								pm25 /= NB_SAMPLES;
							}
							finally
							{
								driver.setSleepMode(true);
							}
							break;
						}
						catch (InterruptedException | CommunicationException | SerialPortInvalidPortException | SDS011CommunicationException e)
						{
							if (interrupted())
								return;
							e.printStackTrace();
							pm25 = -1;
							pm10 = -1;
						}
					}
					if ((pm25 != -1) && (pm10 != -1))
					{
						final SDS011Measurement measurement = new SDS011Measurement();
						final double thresh10 = 4;
						final double thresh25 = 6;
						for (double[] sample : vals)
						{
							if (Math.abs(sample[0] - pm10) > thresh10)
							// invalidate data because value too different
							{
								final List<String> data = new ArrayList<>();
								for (double[] s : vals)
									data.add(String.format("%,.2f", s[0]));
								log("Invalidating for pm10: " + String.join(", ", data));
								pm10 = -1;
								break;
							}
						}
						for (double[] sample : vals)
						{
							if (Math.abs(sample[1] - pm25) > thresh25)
							// invalidate data because value too different
							{
								final List<String> data = new ArrayList<>();
								for (double[] s : vals)
									data.add(String.format("%,.2f", s[1]));
								log("Invalidating for pm25: " + String.join(", ", data));
								pm25 = -1;
								break;
							}
						}
						measurement.pm10 = pm10;
						measurement.pm25 = pm25;
						log("Measured: pm10=" + pm10 + ", pm25=" + pm25);
						synchronized (mLock)
						{
							mLatestMeasurements.put(mThreadSensorName, measurement);
							mNextMeasurementTime = null;
						}
					}
				}
			}
			finally
			{
				log("Closing");
				synchronized (mLock)
				{
					final MeasuringThread thread = mCurrentMeasuringThreads.get(mThreadSensorName);
					if (thread == this)
						mCurrentMeasuringThreads.remove(mThreadSensorName);
				}
			}
		}

		private void log(final String pMsg)
		{
			System.out.println("SDS011/" + mSensors.get(mThreadSensorName) + "/" + mThreadNumber + ": " + pMsg);
		}
	}

	// [short name, port]
	private Map<String, String> mSensors = new HashMap<>();

	public MeasurementGetterSDS011(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException, CommunicationException
	{
		super(pLongHost, pShortHost, pConfig, SENSOR_TYPE_LONG, SENSOR_TYPE_SHORT);
		final JsonArray sensorsArray = pConfig.get("sensors").asArray();
		for (JsonValue sensor : sensorsArray)
		{
			final JsonObject sensorObject = sensor.asObject();
			final String sensorPort = sensorObject.get("port").asString();
			String sensorShortName = mShortName + sensorPort;
			if (sensorObject.contains("short"))
				sensorShortName = sensorObject.get("short").asString();
			String sensorLongName = mLongName + sensorPort;
			if (sensorObject.contains("long"))
				sensorLongName = sensorObject.get("long").asString();
			final Integer intervalWarn = sensorObject.contains("intervalWarn") ? sensorObject.get("intervalWarn").asInt() : null;
			for (Entry<String, String> sensorEntry : ALL_SENSORS.entrySet())
			{
				final String a = sensorEntry.getKey();
				final Double boundMin = sensorObject.contains("min."+a) ? sensorObject.get("min."+a).asDouble() : null;
				final Double boundMax = sensorObject.contains("max."+a) ? sensorObject.get("max."+a).asDouble() : null;
				final Double warnMin = sensorObject.contains("minWarn."+a) ? sensorObject.get("minWarn."+a).asDouble() : null;
				final Double warnMax = sensorObject.contains("maxWarn."+a) ? sensorObject.get("maxWarn."+a).asDouble() : null;
				final String sensorTypeLong = sensorEntry.getValue();
				final String sensorTypeShort = sensorEntry.getKey();
				Watched.registerWatch(sensorShortName + a, sensorLongName + " " + sensorEntry.getValue(), mInstanceLong, pLongHost, pShortHost, sensorTypeLong, sensorTypeShort, mInstanceLong, mInstanceShort, intervalWarn, boundMin, boundMax, warnMin, warnMax, null, null);
			}
			mSensors.put(sensorShortName, sensorPort);
			mCurrentMeasuringThreads.put(sensorShortName, new MeasuringThread(sensorShortName, mMeasuringThreadNumber.getAndIncrement()));
		}
		System.out.println("SDS011: Waiting for sensors to get their first measurement...");
		while (true)
		{
			try
			{
				Thread.sleep(5000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			synchronized (mLock)
			{
				if (mLatestMeasurements.size() == sensorsArray.size())
					break;
				System.out.println("SDS011: Waiting (" + (mLatestMeasurements.size()) + " measures so far)");
			}
		}
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		Map<String, Long> nbRetries = new HashMap<>();
		for (String sensorShortName : mSensors.keySet())
		{
			SDS011Measurement measurement = null;
			synchronized (mLock)
			{
				measurement = mLatestMeasurements.remove(sensorShortName);
			}
			if (measurement != null)
			{
				if (measurement.pm10 != -1)
				{
					final String shortName = sensorShortName + PM10_SHORT;
					res.put(shortName, new Measurement(shortName, measurement.pm10));
				}
				if (measurement.pm25 != -1)
				{
					final String shortName = sensorShortName + PM25_SHORT;
					res.put(shortName, new Measurement(shortName, measurement.pm25));
				}
			}
			else
			{
				// could not acquire measurement
				// TODO maybe kill the measuring thread? or give it another chance
				Long retries = nbRetries.get(sensorShortName);
				if (retries == null)
					retries = 0L;
				if (retries >= 1)
				{
					System.err.println("SDS011: more than 3 retries on the driver - killing...");
					nbRetries = null;
					Thread thread;
					synchronized (mLock)
					{
						thread = mCurrentMeasuringThreads.get(sensorShortName);
					}
					// kill the thread
					thread.interrupt();
					System.out.println("... waiting for current SDS011 measuring thread to die...");
					try
					{
						// make sure it stopped
						thread.join();
					}
					catch (InterruptedException e)
					{
						// we should not be interrupted
					}
					System.out.println("Restarting new SDS011 measuring thread...");
					synchronized (mLock)
					{
						mCurrentMeasuringThreads.put(sensorShortName, new MeasuringThread(sensorShortName, mMeasuringThreadNumber.getAndIncrement()));
					}
				}
				else
				{
					nbRetries.put(sensorShortName, retries + 1);
					System.err.println("SDS011: " + retries + " retries for sensor " + sensorShortName);
				}
			}
		}
		synchronized (mLock)
		{
			mNextMeasurementTime = System.currentTimeMillis() + getDefaultTemporization() * 1000;
		}
		System.out.println("*** Current timestamp: " + System.currentTimeMillis() + ", next measuring " + mNextMeasurementTime);
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// nothing to do here
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return DEFAULT_TEMPORIZATION_SECONDS;
	}
}
