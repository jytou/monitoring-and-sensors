package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.eclipsesource.json.JsonObject;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;

/**
 * Any use of this requires sudo privileges to run the mdadm command.
 * @author jytou
 */
public class MeasurementGetterMdAdm extends MeasurementGetterAbstract
{
	private String mMirrorNumber;

	public MeasurementGetterMdAdm(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "Mdadm", "md");
		mMirrorNumber = pConfig.get("instanceShort").asString();
		setListenProperties(pConfig, 120, 0.9, 1.1, null, null);
		registerDefault("md" + String.valueOf(mMirrorNumber), String.valueOf(mMirrorNumber));
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		final Map<String, Measurement> res = new HashMap<>();
		try
		{
//			System.out.println("Executing " + "sudo /sbin/mdadm --detail /dev/md" + mMirrorNumber);
			final Process exec = Runtime.getRuntime().exec("sudo /sbin/mdadm --detail /dev/md" + mMirrorNumber);
			final BufferedReader r = new BufferedReader(new InputStreamReader(exec.getInputStream()));
			try
			{
				String l;
				while ((l = r.readLine()) != null)
				{
					l = l.trim();
					if (l.startsWith("State :"))
					{
						// is it clean?
						final String state = l.substring(l.indexOf(':') + 1).trim();
						if (state.equals("clean") || state.equals("active") || state.equals("active, resyncing") || state.equals("active, checking") || state.equals("clean, checking"))
						{
							res.put(mShortName, new Measurement(mShortName, 1));
//							mFeedbackListener.mirrorStateOK();
						}
						else
						{
							res.put(mShortName, new Measurement(mShortName, 0));
//							mFeedbackListener.mirrorStateDegraded(state);
						}
						break;
					}
				} 
			}
			finally
			{
				r.close();
			}
//			System.out.println("Done " + "sudo /sbin/mdadm --detail /dev/md" + mMirrorNumber);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
	}

	@Override
	public void reconnect() throws GetterException
	{
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 30*60;
	}
}
