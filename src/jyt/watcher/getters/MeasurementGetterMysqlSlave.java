package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.eclipsesource.json.JsonObject;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;
import jyt.watcher.Watched;

public class MeasurementGetterMysqlSlave extends MeasurementGetterAbstract
{
	private static final String LAG_SHORT = "l";
	private static final String STATUS_SHORT = "s";
	private static String JDBC_DRIVER = "org.mariadb.jdbc.Driver";
	private static String JDBC_URL = "jdbc:mariadb://";

	private Properties mConnectProps;
	private Connection mConnection;
	private String mURL;

	public MeasurementGetterMysqlSlave(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException, GetterException
	{
		super(pLongHost, pShortHost, pConfig, "Mysql Slave", "ms");
		mConnectProps = new Properties();
		mConnectProps.put("user", pConfig.get("user").asString());
		mConnectProps.put("password", pConfig.get("pass").asString());
		final String host = pConfig.get("host").asString();
		final String port = pConfig.contains("port") ? ":" + pConfig.get("port").asString() : "";
		mURL = JDBC_URL + host + port + "/" + (pConfig.contains("db") ? pConfig.get("db") : "");
		Watched.registerWatch(mShortName + STATUS_SHORT, mLongName + " Status", mLongName + " Status", mLongHost, mShortHost, mTypeLong, mTypeShort + STATUS_SHORT, host + port, null, mIntervalWarn, 0.9, 1.1, null, null, null, null);
		Watched.registerWatch(mShortName + LAG_SHORT, mLongName + " Lag", mLongName + " Lag", mLongHost, mShortHost, mTypeLong, mTypeShort + LAG_SHORT, host + port, null, mIntervalWarn, mBoundMin, mBoundMax, mWarnMin, mWarnMax, null, null);
		try
		{
			connect();
		}
		catch (GetterException e)
		{
			// ignore it, it's okay, maybe the database is not up right now
		}
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		if (mConnection == null)
			connect();
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		BufferedReader r = null;
		try
		{
			final Statement s = mConnection.createStatement();
			try
			{
				final ResultSet rs = s.executeQuery("show slave status");
				int tps = 0;
				boolean ko = false;
				try
				{
					if (rs.next())
					{
						ko |= (!"Yes".equals(rs.getString("Slave_IO_Running")));
						ko |= (!"Yes".equals(rs.getString("Slave_SQL_Running")));
						ko |= (!"0".equals(rs.getString("Last_Errno")));
						tps = rs.getInt("Seconds_Behind_Master");
					}
					else
						throw new GetterException("Got empty result for slave status, strange");
				}
				finally
				{
					rs.close();
				}
				res.put(mShortName + STATUS_SHORT, new Measurement(mShortName + STATUS_SHORT, ko ? 0 : 1));
				res.put(mShortName + LAG_SHORT, new Measurement(mShortName + LAG_SHORT, tps));
			}
			finally
			{
				s.close();
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			try
			{
				mConnection.close();
			}
			catch (SQLException e1)
			{
			}
			mConnection = null;
		}
		finally
		{
			if (r != null)
				try
				{
					r.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
		}
		return res;
	}

	private void connect() throws GetterException
	{
		try
		{
			Class.forName(JDBC_DRIVER);
			mConnection = DriverManager.getConnection(mURL, mConnectProps);
		}
		catch (ClassNotFoundException | SQLException e)
		{
			throw new GetterException(e);
		}
	}
	@Override
	public void disconnect() throws GetterException
	{
		if (mConnection != null)
			try
			{
				mConnection.close();
			}
			catch (SQLException e)
			{
				throw new GetterException(e);
			}
		mConnection = null;
	}

	@Override
	public void reconnect() throws GetterException
	{
		try
		{
			disconnect();
		}
		catch (GetterException e)
		{
			// ignore it!
		}
		connect();
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 60 * 10;
	}
}
