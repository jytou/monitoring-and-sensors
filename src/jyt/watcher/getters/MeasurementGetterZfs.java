package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.eclipsesource.json.JsonObject;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;

/**
 * Any use of this requires sudo privileges to run the zpool status command.
 * @author jytou
 */
public class MeasurementGetterZfs extends MeasurementGetterAbstract
{
	public MeasurementGetterZfs(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "Zfs", "zfs");
		setListenProperties(pConfig, 120, 0.9, 1.1, null, null);
		registerDefault("zfsall", "all");
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		final Map<String, Measurement> res = new HashMap<>();
		try
		{
			final Process exec = Runtime.getRuntime().exec("sudo zpool status");
			try (BufferedReader r = new BufferedReader(new InputStreamReader(exec.getInputStream())))
			{
				String l;
				boolean ok = true;
				while ((l = r.readLine()) != null)
				{
					l = l.trim();
					if (l.startsWith("state:"))
					{
						// is it clean?
						final String state = l.substring(l.indexOf(':') + 1).trim();
						if (!state.equals("ONLINE"))
						{
							ok = false;
							break;
						}
					}
				}
				res.put(mShortName, new Measurement(mShortName, ok ? 1 : 0));
			}
		}
		catch (IOException e)
		{
			res.put(mShortName, new Measurement(mShortName, 0));
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
	}

	@Override
	public void reconnect() throws GetterException
	{
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 30*60;
	}
}
