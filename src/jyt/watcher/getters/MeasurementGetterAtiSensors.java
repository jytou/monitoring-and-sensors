package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import jyt.watcher.GetterException;
import jyt.watcher.IMeasurementGetter;
import jyt.watcher.Measurement;

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * WARNING: Currently not operational
 * @author jytou
 */
public class MeasurementGetterAtiSensors implements IMeasurementGetter
{
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		try
		{
			Process p = Runtime.getRuntime().exec("aticonfig --odgt --adapter=ALL");
			InputStream is = p.getInputStream();
			BufferedReader r = new BufferedReader(new InputStreamReader(is));
			try
			{
				int curAdapter = -1;
				String l;
				while ((l = r.readLine()) != null)
				{
					if (l.startsWith("Adapter "))
						try
						{
							curAdapter = Integer.parseInt(l.substring(8, 9));
						}
						catch (NumberFormatException e)
						{
							throw new GetterException(e);
						}
					else if (l.trim().startsWith("Sensor"))
					{
						try
						{
							final double value = Double.parseDouble(l.substring(l.indexOf('-'), l.indexOf('C')));
							final String name = "mgt" + curAdapter;
							res.put(name, new Measurement(name, value));
						}
						catch (NumberFormatException e)
						{
							e.printStackTrace();
						}
					}
				} 
			}
			finally
			{
				r.close();
			}
		}
		catch (IOException e)
		{
			throw new GetterException(e);
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public Integer getDefaultTemporization()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
