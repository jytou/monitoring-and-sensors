package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;
import jyt.watcher.Watched;

/**
 * Uses the command df to get disk usage information
 * @author jytou
 */
public class MeasurementGetterDiskSpace extends MeasurementGetterAbstract
{
	private static final String[] DEFAULT_IGNORE = new String[] {"/dev/loop", "/dev ", "tmpfs ", "udev ", "//", "/boot/"};
	// [path, short name]
	private Map<String, String> mKnownPaths = new HashMap<>();
	// [path, boolean]
	private Map<String, Boolean> mUsePercent = new HashMap<>();
	// if null, don't check that we monitor all
	private Set<String> mToIgnore = null;
	private String mAllShortName = null;

	public MeasurementGetterDiskSpace(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "DiskSpace", "ds");
		if (pConfig.contains("checkAll"))
		{
			final JsonObject checkAllObject = pConfig.get("checkAll").asObject();
			mToIgnore = new HashSet<>();
			if (checkAllObject.contains("ignore"))
				for (JsonValue ignoreValue : checkAllObject.get("ignore").asArray())
					mToIgnore.add(ignoreValue.asString());
			final String shortName = checkAllObject.get("short").asString();
			final Double boundMin = -1.0;
			final Double boundMax = 1.0;
			final Double warnMin = -1.0;
			final Double warnMax = 1.0;
			final String sensorName = checkAllObject.get("name").asString();
			final String sensorTypeLong = checkAllObject.contains("typeLong") ? checkAllObject.get("typeLong").asString() : "All paths included";
			final String sensorTypeShort = checkAllObject.contains("typeShort") ? checkAllObject.get("typeShort").asString() : "api";
			final String sensorInstanceLong = checkAllObject.contains("instanceLong") ? checkAllObject.get("instanceLong").asString() : sensorName;
			final String sensorInstanceShort = checkAllObject.contains("instanceShort") ? checkAllObject.get("instanceShort").asString() : shortName;
			final String longName = checkAllObject.contains("long") ? checkAllObject.get("long").asString() : pLongHost + " " + sensorTypeLong + " " + sensorInstanceLong;
			mAllShortName = shortName;
			Watched.registerWatch(shortName, longName, sensorName, pLongHost, pShortHost, sensorTypeLong, sensorTypeShort, sensorInstanceLong, sensorInstanceShort, 30*60, boundMin, boundMax, warnMin, warnMax, null, null);
		}
		final JsonArray paths = pConfig.get("paths").asArray();
		for (JsonValue jsonValuePath : paths)
		{
			final JsonObject pathObject = jsonValuePath.asObject();
			if (pathObject.contains("disabled") && pathObject.getBoolean("disabled", true))
			// add to toignore
				mToIgnore.add(pathObject.get("path").asString());
			else
			{
				final String shortName = pathObject.get("short").asString();
				final boolean usePercent = pathObject.contains("type") ? pathObject.get("type").asString().equals("percent") : false;
				mUsePercent.put(pathObject.get("path").asString(), usePercent);
				final Double boundMin = extractBound(pathObject, "min");
				final Double boundMax = usePercent ? new Double(200) : extractBound(pathObject, "max");
				final Double warnMin = extractBound(pathObject, "minWarn");
				final Double warnMax = usePercent ? new Double(150) : extractBound(pathObject, "maxWarn");
				final String sensorName = pathObject.get("name").asString();
				if (pathObject.contains("path"))
					mKnownPaths.put(pathObject.get("path").asString(), shortName);
				else
					throw new RuntimeException("Path is needed for " + sensorName);
				final String sensorTypeLong = pathObject.contains("typeLong") ? pathObject.get("typeLong").asString() : "Disk Available Space";
				final String sensorTypeShort = pathObject.contains("typeShort") ? pathObject.get("typeShort").asString() : "as";
				final String sensorInstanceLong = pathObject.contains("instanceLong") ? pathObject.get("instanceLong").asString() : sensorName;
				final String sensorInstanceShort = pathObject.contains("instanceShort") ? pathObject.get("instanceShort").asString() : shortName;
				final String longName = pathObject.contains("long") ? pathObject.get("long").asString() : pLongHost + " " + sensorTypeLong + " " + sensorInstanceLong;
				Watched.registerWatch(shortName, longName, sensorName, pLongHost, pShortHost, sensorTypeLong, sensorTypeShort, sensorInstanceLong, sensorInstanceShort, 30*60, boundMin, boundMax, warnMin, warnMax, null, null);
			}
		}
	}

	private static Double extractBound(final JsonObject pPathObject, final String pParam)
	{
		if (pPathObject.contains(pParam))
		{
			final JsonValue value = pPathObject.get(pParam);
			if (value.isString())
			{
				String str = value.asString().toLowerCase();
				double factor = 1;
				if (str.endsWith("t"))
				{
					factor = 1024d * 1024d * 1024d * 1024d;
					str = str.substring(0, str.length() - 1);
				}
				else if (str.endsWith("g"))
				{
					factor = 1024d * 1024d * 1024d;
					str = str.substring(0, str.length() - 1);
				}
				else if (str.endsWith("m"))
				{
					factor = 1024d * 1024d;
					str = str.substring(0, str.length() - 1);
				}
				else if (str.endsWith("k"))
				{
					factor = 1024d;
					str = str.substring(0, str.length() - 1);
				}
				try
				{
					return Double.parseDouble(str) * factor;
				}
				catch (NumberFormatException e)
				{
					throw new RuntimeException("Unrecognized number " + str + " for parameter " + pParam);
				}
			}
			else if (value.isNumber())
				return value.asDouble();
		}
		return null;
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<>();
		try
		{
			final Process p = Runtime.getRuntime().exec("/bin/df");
			final InputStream is = p.getInputStream();
			final BufferedReader r = new BufferedReader(new InputStreamReader(is));
			final List<String> errors;
			try
			{
				String l;
				errors = new ArrayList<>();

				while ((l = r.readLine()) != null)
				{
					if (l.startsWith("Filesystem"))
						continue;
					boolean toIgnore = false;
					for (String ignore : DEFAULT_IGNORE)
					{
						if (l.startsWith(ignore))
						{
							toIgnore = true;
							break;
						}
					}
					if (toIgnore)
						continue;

					final List<String> elements = new ArrayList<>();
					final StringTokenizer st = new StringTokenizer(l, " ", false);
					while (st.hasMoreTokens())
						elements.add(st.nextToken());
//					final String fsType = elements.get(0);
					final long avail = Long.parseLong(elements.get(3)) * 1024L;
					final int percentUse = Integer.parseInt(elements.get(4).substring(0, elements.get(4).length() - 1));
					final String currentPath = elements.get(5);
					boolean found = false;
					for (Entry<String, String> pathEntry : mKnownPaths.entrySet())
					{
						final String path = pathEntry.getKey();
						final String name = pathEntry.getValue();
						if (path.equals(currentPath))
						{
							res.put(name, new Measurement(name, mUsePercent.get(currentPath).booleanValue() ? Integer.valueOf(100 - percentUse).doubleValue() : Long.valueOf(avail).doubleValue()));
							found = true;
							break;
						}
					}
					if ((!found) && (mToIgnore != null))
					// the line was not found in the directories we are watching - let's see if we can safely ignore it
					{
						toIgnore = false;
						for (String ignore : mToIgnore)
							if (Pattern.compile(ignore).matcher(l).find())
							{
								toIgnore = true;
								break;
							}
						if (!toIgnore)
							errors.add(currentPath);
					}
				}
				if (mAllShortName != null)
				{
					if (!errors.isEmpty())
						System.out.println("Filesystems not checked: " + String.join(", ", errors));
					res.put(mAllShortName, new Measurement(mAllShortName, errors.size()));
				}
			}
			finally
			{
				r.close();
				p.destroy();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
//		if (!errors.isEmpty())
//			throw new GetterException(errors.toString());
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 60*5;
	}
}
