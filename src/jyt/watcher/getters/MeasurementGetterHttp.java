package jyt.watcher.getters;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.hc.client5.http.auth.AuthScope;
import org.apache.hc.client5.http.auth.UsernamePasswordCredentials;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.impl.auth.BasicCredentialsProvider;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpHost;

import com.eclipsesource.json.JsonObject;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;

public class MeasurementGetterHttp extends MeasurementGetterAbstract
{
	private String mURL;
	private String mProxyHost;
	private int mProxyPort;
	private String mProxyUser;
	private char[] mProxyPassword;
	private int mTimeoutResult;
	private int mErrorResult;
	private int mOkResult;

	public MeasurementGetterHttp(final String pLongHost, final String pShortHost, final JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "HTTP Query", "http");
		mURL = pConfig.get("url").asString();
		if (pConfig.contains("proxy"))
		{
			final JsonObject proxyObject = pConfig.get("proxy").asObject();
			mProxyHost = proxyObject.get("host").asString();
			mProxyPort = proxyObject.getInt("port", 80);
			if (proxyObject.contains("user"))
			{
				mProxyUser = proxyObject.get("user").asString();
				mProxyPassword = proxyObject.get("password").asString().toCharArray();
			}
		}
		mErrorResult = pConfig.getInt("errorResult", -5000);
		mTimeoutResult = pConfig.getInt("timeoutResult", -10000);
		mOkResult = pConfig.getInt("okHttpCode", 200);
		registerDefault();
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		try
		{
			RequestConfig requestConfig;
			CloseableHttpClient httpClient;
			if (mProxyHost != null)
			{
				final BasicCredentialsProvider credsProvider = new BasicCredentialsProvider();
				credsProvider.setCredentials(new AuthScope(mProxyHost, mProxyPort), new UsernamePasswordCredentials(mProxyUser, mProxyPassword));
				httpClient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
				final HttpHost proxy = new HttpHost("http", mProxyHost, mProxyPort);
				requestConfig = RequestConfig.custom().setConnectionRequestTimeout(5, TimeUnit.SECONDS).setResponseTimeout(5, TimeUnit.SECONDS).setConnectTimeout(5, TimeUnit.SECONDS).setProxy(proxy).build();
			}
			else
			{
				httpClient = HttpClients.createDefault();
				requestConfig = RequestConfig.custom().build();
			}

			try
			{
				final String protocol = mURL.substring(0, mURL.indexOf("://"));
				final String url = mURL.substring(protocol.length() + 3);
				final String host = url.substring(0, url.indexOf('/') == -1 ? url.length() : url.indexOf('/'));
				final String path = url.substring(host.length());
				final HttpHost target = new HttpHost(protocol, host);
				final HttpGet request = new HttpGet(path);
				request.setConfig(requestConfig);
				long startTime = System.nanoTime();
				try (CloseableHttpResponse response = httpClient.execute(target, request))
				{
					long elapsed = (System.nanoTime() - startTime) / 1000000;
					if (response.getCode() != mOkResult)
						res.put(mShortName, new Measurement(mShortName, mErrorResult));
					else
						res.put(mShortName, new Measurement(mShortName, elapsed));
				} 
			}
			finally
			{
				httpClient.close();
			}
		}
		catch (IOException e)
		{
			res.put(mShortName, new Measurement(mShortName, mTimeoutResult));
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 60 * 2;
	}
}
