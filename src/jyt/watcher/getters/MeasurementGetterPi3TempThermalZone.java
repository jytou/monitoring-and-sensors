package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.eclipsesource.json.JsonObject;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;

public class MeasurementGetterPi3TempThermalZone extends MeasurementGetterAbstract
{
	final private double mFactor;

	public MeasurementGetterPi3TempThermalZone(final String pLongHost, final String pShortHost, final JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "Temperature", "cput");
		mFactor = pConfig.getDouble("factor", 1000);
		setListenProperties(pConfig, 120, null, null, null, null);
		registerDefault();
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		try
		{
			BufferedReader r = new BufferedReader(new InputStreamReader(new FileInputStream("/sys/class/thermal/thermal_zone0/temp")));
			try
			{
				String l;
				if ((l = r.readLine()) != null)
				{
					try
					{
						double value = Double.parseDouble(l) / mFactor;
						res.put(mShortName, new Measurement(mShortName, value));
					}
					catch (NumberFormatException e)
					{
						throw new GetterException(e);
					}
				}
			}
			finally
			{
				r.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 60 * 2;
	}
}
