package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;
import jyt.watcher.Watched;

/**
 * Any use of this requires privileges to run smartctl with sudo for every single monitored disk.
 * @author jytou
 */
public class MeasurementGetterZpoolSpace extends MeasurementGetterAbstract
{
	// [zpool, short name]
	private Map<String, String> mKnownZpools = new HashMap<>();
	// [path, boolean]
	private Map<String, Boolean> mUsePercent = new HashMap<>();
	// if null, don't check that we monitor all
	private Set<String> mToIgnore = null;
	private String mAllShortName = null;

	public MeasurementGetterZpoolSpace(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "ZpoolSpace", "zs");
		if (pConfig.contains("checkAll"))
		{
			final JsonObject checkAllObject = pConfig.get("checkAll").asObject();
			mToIgnore = new HashSet<>();
			if (pConfig.contains("ignore"))
				for (JsonValue ignoreValue : checkAllObject.get("ignore").asArray())
					mToIgnore.add(ignoreValue.asString());
			final String shortName = checkAllObject.get("short").asString();
			final Double boundMin = -1.0;
			final Double boundMax = 1.0;
			final Double warnMin = -1.0;
			final Double warnMax = 1.0;
			final String sensorName = checkAllObject.get("name").asString();
			final String sensorTypeLong = checkAllObject.contains("typeLong") ? checkAllObject.get("typeLong").asString() : "All pools included";
			final String sensorTypeShort = checkAllObject.contains("typeShort") ? checkAllObject.get("typeShort").asString() : "api";
			final String sensorInstanceLong = checkAllObject.contains("instanceLong") ? checkAllObject.get("instanceLong").asString() : sensorName;
			final String sensorInstanceShort = checkAllObject.contains("instanceShort") ? checkAllObject.get("instanceShort").asString() : shortName;
			final String longName = checkAllObject.contains("long") ? checkAllObject.get("long").asString() : pLongHost + " " + sensorTypeLong + " " + sensorInstanceLong;
			mAllShortName = shortName;
			Watched.registerWatch(shortName, longName, sensorName, pLongHost, pShortHost, sensorTypeLong, sensorTypeShort, sensorInstanceLong, sensorInstanceShort, 30*60, boundMin, boundMax, warnMin, warnMax, null, null);
		}
		final JsonArray pools = pConfig.get("pools").asArray();
		for (JsonValue jsonValuePath : pools)
		{
			final JsonObject pathObject = jsonValuePath.asObject();
			final String poolName = pathObject.get("pool").asString();
			if (pathObject.contains("disabled") && pathObject.getBoolean("disabled", true))
			// add to toignore
				mToIgnore.add(poolName);
			else
			{
				final String shortName = pathObject.get("short").asString();
				final Double boundMin = extractBound(pathObject, "min");
				final Double boundMax = extractBound(pathObject, "max");
				final Double warnMin = extractBound(pathObject, "minWarn");
				final Double warnMax = extractBound(pathObject, "maxWarn");
				final String sensorName = pathObject.get("name").asString();
				mKnownZpools.put(poolName, shortName);
				final String type = pathObject.get("type").asString();
				mUsePercent.put(poolName, "percent".equals(type));
				final String sensorTypeLong = pathObject.contains("typeLong") ? pathObject.get("typeLong").asString() : "Zpool Space";
				final String sensorTypeShort = pathObject.contains("typeShort") ? pathObject.get("typeShort").asString() : "zs";
				final String sensorInstanceLong = pathObject.contains("instanceLong") ? pathObject.get("instanceLong").asString() : sensorName;
				final String sensorInstanceShort = pathObject.contains("instanceShort") ? pathObject.get("instanceShort").asString() : shortName;
				final String longName = pathObject.contains("long") ? pathObject.get("long").asString() : pLongHost + " " + sensorTypeLong + " " + sensorInstanceLong;
				Watched.registerWatch(shortName, longName, sensorName, pLongHost, pShortHost, sensorTypeLong, sensorTypeShort, sensorInstanceLong, sensorInstanceShort, 30*60, boundMin, boundMax, warnMin, warnMax, null, null);
			}
		}
	}

	private static Double extractBound(final JsonObject pPathObject, final String pParam)
	{
		if (pPathObject.contains(pParam))
		{
			final JsonValue value = pPathObject.get(pParam);
			if (value.isString())
			{
				String str = value.asString().toLowerCase();
				double factor = 1;
				if (str.endsWith("t"))
				{
					factor = 1024d * 1024d * 1024d * 1024d;
					str = str.substring(0, str.length() - 1);
				}
				else if (str.endsWith("g"))
				{
					factor = 1024d * 1024d * 1024d;
					str = str.substring(0, str.length() - 1);
				}
				else if (str.endsWith("m"))
				{
					factor = 1024d * 1024d;
					str = str.substring(0, str.length() - 1);
				}
				else if (str.endsWith("k"))
				{
					factor = 1024d;
					str = str.substring(0, str.length() - 1);
				}
				try
				{
					return Double.parseDouble(str) * factor;
				}
				catch (NumberFormatException e)
				{
					throw new RuntimeException("Unrecognized number " + str + " for parameter " + pParam);
				}
			}
			else if (value.isNumber())
				return value.asDouble();
		}
		return null;
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<>();
		try
		{
			final Map<String, Long> used = getValues("used");
			final Map<String, Long> available = getValues("available");
			final Set<String> searched = new HashSet<>(mKnownZpools.keySet());
			searched.removeAll(used.keySet());
			res.put(mAllShortName, new Measurement(mAllShortName, searched.size()));
			for (Entry<String, Long> usedEntry : used.entrySet())
			{
				final String name = usedEntry.getKey();
				final Long usedValue = usedEntry.getValue();
				if (mKnownZpools.containsKey(name))
				{
					final String shortName = mKnownZpools.get(name);
					final Long availableValue = available.get(name);
					if (availableValue != null)
						res.put(name, new Measurement(shortName, mUsePercent.get(name).booleanValue() ? 100.0 * usedValue / (usedValue + availableValue) : availableValue));
				}
				else
					System.err.println("WARNING: pool " + name + " is not in the list of watched pools");
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
//		if (!errors.isEmpty())
//			throw new GetterException(errors.toString());
		return res;
	}

	public Map<String, Long> getValues(final String pType) throws IOException
	{
		final Map<String, Long> res = new HashMap<>();
		final Process p = Runtime.getRuntime().exec("/sbin/zfs get -Hp " + pType);
		try
		{
			try (final InputStream is = p.getInputStream())
			{
				try (final BufferedReader r = new BufferedReader(new InputStreamReader(is)))
				{
					String l;
					while ((l = r.readLine()) != null)
					{
						final String[] elements = l.split("\t");
						final String entry = elements[0];
						if (entry.contains("/"))
						// blatantly ignore anything that is not a simple pool
							continue;
						boolean toIgnore = false;
						for (String ignore : mToIgnore)
							if (Pattern.compile(ignore).matcher(entry).find())
							{
								toIgnore = true;
								break;
							}
						if (!toIgnore)
							res.put(entry, Long.valueOf(elements[2]));
					}
				}
			}
		}
		finally
		{
			p.destroy();
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 60*5;
	}
}
