package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;
import jyt.watcher.StringUtil;
import jyt.watcher.Watched;

public class MeasurementGetterLmSensors extends MeasurementGetterAbstract
{
	// Map of [groupName, [lineStart, [shortName]]] => same line start, different things
	private Map<String, Map<String, List<String>>> mSensorConf = new HashMap<>();

	public MeasurementGetterLmSensors(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "LmSensors", "ls");
		for (JsonValue group : pConfig.get("groups").asArray())
		{
			final JsonObject groupObject = group.asObject();
			final String groupName = groupObject.get("name").asString();
			final Map<String, List<String>> sensors = new HashMap<>();
			mSensorConf.put(groupName, sensors);
			for (JsonValue sensor : groupObject.get("sensors").asArray())
			{
				final JsonObject sensorObject = sensor.asObject();
				final String lineStart = sensorObject.get("line").asString();
				final String shortName = sensorObject.get("short").asString();
				final String longName = sensorObject.get("long").asString();
				final Integer intervalWarn = sensorObject.contains("intervalWarn") ? sensorObject.get("intervalWarn").asInt() : null;
				final Double boundMin = sensorObject.contains("min") ? sensorObject.get("min").asDouble() : null;
				final Double boundMax = sensorObject.contains("max") ? sensorObject.get("max").asDouble() : null;
				final Double warnMin = sensorObject.contains("minWarn") ? sensorObject.get("minWarn").asDouble() : null;
				final Double warnMax = sensorObject.contains("maxWarn") ? sensorObject.get("maxWarn").asDouble() : null;
				final String sensorName = sensorObject.contains("name") ? sensorObject.get("name").asString() : null;
				final String sensorTypeLong = sensorObject.contains("typeLong") ? sensorObject.get("typeLong").asString() : longName;
				final String sensorTypeShort = sensorObject.contains("typeShort") ? sensorObject.get("typeShort").asString() : shortName;
				final String instanceTypeLong = sensorObject.contains("instanceLong") ? sensorObject.get("instanceLong").asString() : longName;
				final String instanceTypeShort = sensorObject.contains("instanceShort") ? sensorObject.get("instanceShort").asString() : shortName;
				Watched.registerWatch(shortName, longName, sensorName, pLongHost, pShortHost, sensorTypeLong, sensorTypeShort, instanceTypeLong, instanceTypeShort, intervalWarn, boundMin, boundMax, warnMin, warnMax, null, null);
				List<String> shortNames = sensors.get(lineStart);
				if (shortNames == null)
					sensors.put(lineStart, shortNames = new ArrayList<>());
				shortNames.add(shortName);
			}
		}
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		try
		{
			final Process p = Runtime.getRuntime().exec("sensors");
			final BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream(), StandardCharsets.UTF_8));
			String l;
			Map<String, Iterator<String>> currentGroup = null;
			String currentGroupName = null;
			try
			{
				while ((l = r.readLine()) != null)
				{
					if (currentGroup == null)
					{
						currentGroup = new HashMap<>();
						currentGroupName = l.trim();
						final Map<String, List<String>> groupEntry = mSensorConf.get(l.trim());
						if (groupEntry == null)
							System.err.println("!!! WARNING UNKNOWN GROUP " + l);
						else
							for (Entry<String, List<String>> entry : groupEntry.entrySet())
								currentGroup.put(entry.getKey(), entry.getValue().iterator());
					}
					else if (l.trim().isEmpty())
					{
						if ((currentGroup != null) && (!currentGroup.isEmpty()))
							System.err.println("!!! WARNING !!! Remaining following keys in group " + currentGroupName + ": " + String.join(", ", currentGroup.keySet()));
						currentGroup = null;
					}
					else
					{
						final int colonPos = l.indexOf(':');
						if (colonPos != -1)
						{
							final String currentShortName = l.substring(0, colonPos);
							final Iterator<String> possibleTargetNamesIterator = currentGroup.get(currentShortName);
							if ((possibleTargetNamesIterator != null) && possibleTargetNamesIterator.hasNext())
							{
								final String shortName = possibleTargetNamesIterator.next();
								if (!possibleTargetNamesIterator.hasNext())
									currentGroup.remove(currentShortName);
								if (shortName != null)
								{
									String value = StringUtil.split(l.substring(colonPos + 1), " ")[0];
									if (value.endsWith("°C"))
										value = value.substring(0, value.length() - 2);
									try
									{
										res.put(shortName, new Measurement(shortName, Double.parseDouble(value)));
									}
									catch (NumberFormatException e)
									{
										e.printStackTrace();
									}
								}
							}
						}
					}
				}
			}
			finally
			{
				if ((currentGroup != null) && (!currentGroup.isEmpty()))
					System.err.println("!!! WARNING !!! Remaining following keys in group " + currentGroupName + ": " + String.join(", ", currentGroup.keySet()));
				r.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 60*2;
	}
}
