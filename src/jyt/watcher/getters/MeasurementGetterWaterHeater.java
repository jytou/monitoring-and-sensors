package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.eclipsesource.json.JsonObject;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;
import jyt.watcher.Watched;

/**
 * This is a very specific class to watch the water temperature of my heater..
 * @author jytou
 */
public class MeasurementGetterWaterHeater extends MeasurementGetterAbstract
{
	private static final String SENSOR_TYPE_LONG = "Water Heater Temperature";
	private static final String SENSOR_TYPE_SHORT = "wht";
	private static final String AMBIENT_TEMP_SHORT_NAME = SENSOR_TYPE_SHORT + "a";
	private static final String HEATED_TEMP_SHORT_NAME = SENSOR_TYPE_SHORT + "h";
	private static final String OUTSIDE_TEMP_SHORT_NAME = SENSOR_TYPE_SHORT + "o";
	private final Comparator<Double> mComparator = new Comparator<Double>()
	{
		@Override
		public int compare(Double v1, Double v2)
		{
			return v1.compareTo(v2);
		}
	};
	private Map<String, Double> mPreviousValues = new HashMap<>();

	public MeasurementGetterWaterHeater(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "HeaterTemp", "ht");
		Watched.registerWatch(OUTSIDE_TEMP_SHORT_NAME, SENSOR_TYPE_LONG + " Outside", SENSOR_TYPE_SHORT, "HeaterTemp", "ht", SENSOR_TYPE_LONG, SENSOR_TYPE_SHORT, null, null, 100, 5.0, 40.0, 10.0, 35.0, null, null);
		Watched.registerWatch(HEATED_TEMP_SHORT_NAME, SENSOR_TYPE_LONG + " Heater", SENSOR_TYPE_SHORT, "HeaterTemp", "ht", SENSOR_TYPE_LONG, SENSOR_TYPE_SHORT, null, null, 100, 30.0, 60.0, 25.0, 65.0, null, null);
		Watched.registerWatch(AMBIENT_TEMP_SHORT_NAME, SENSOR_TYPE_LONG + " Ambiant", SENSOR_TYPE_SHORT, "HeaterTemp", "ht", SENSOR_TYPE_LONG, SENSOR_TYPE_SHORT, null, null, 100, 19.0, 35.0, 16.0, 40.0, null, null);
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		final Map<String, Measurement> res = new HashMap<>();
		final List<Double> values = new ArrayList<>();
		readValue(values, 0, OUTSIDE_TEMP_SHORT_NAME);
		readValue(values, 1, AMBIENT_TEMP_SHORT_NAME);
		readValue(values, 2, HEATED_TEMP_SHORT_NAME);
		if (values.size() == 3)
		{
			values.sort(mComparator);
			res.put(OUTSIDE_TEMP_SHORT_NAME, new Measurement(OUTSIDE_TEMP_SHORT_NAME, values.get(0)));
			res.put(AMBIENT_TEMP_SHORT_NAME, new Measurement(AMBIENT_TEMP_SHORT_NAME, values.get(1)));
			res.put(HEATED_TEMP_SHORT_NAME, new Measurement(HEATED_TEMP_SHORT_NAME, values.get(2)));
			mPreviousValues.put(OUTSIDE_TEMP_SHORT_NAME, values.get(0));
			mPreviousValues.put(AMBIENT_TEMP_SHORT_NAME, values.get(1));
			mPreviousValues.put(HEATED_TEMP_SHORT_NAME, values.get(2));
		}
		else if ((!values.isEmpty()) && (mPreviousValues.size() >= 1))
		{
			final Set<String> candidates = new HashSet<>(mPreviousValues.keySet());
			for (Double val : values)
			{
				final String closest = findClosest(candidates, val);
				if (closest != null)
				{
					res.put(closest, new Measurement(closest, val));
					mPreviousValues.put(closest, val);
				}
			}
		}
		else if (!values.isEmpty())
		{
			values.sort(mComparator);
			res.put(OUTSIDE_TEMP_SHORT_NAME, new Measurement(OUTSIDE_TEMP_SHORT_NAME, values.get(0)));
			mPreviousValues.put(OUTSIDE_TEMP_SHORT_NAME, values.get(0));
			if (values.size() == 2)
			{
				res.put(AMBIENT_TEMP_SHORT_NAME, new Measurement(AMBIENT_TEMP_SHORT_NAME, values.get(1)));
				mPreviousValues.put(AMBIENT_TEMP_SHORT_NAME, values.get(1));
			}
		}
		return res;
	}

	private String findClosest(Set<String> pCandidates, Double pVal)
	{
		double dist = Double.MAX_VALUE;
		String found = null;
		for (String candidate : pCandidates)
		{
			final double d = Math.abs(mPreviousValues.get(candidate) - pVal);
			if ((d < 4) && (d < dist))
			{
				dist = d;
				found = candidate;
			}
		}
		if (found != null)
		{
			pCandidates.remove(found);
			return found;
		}
		return null;
	}

	public void readValue(List<Double> pValues, int num, String shortName) throws GetterException
	{
		try
		{
			final Process p = Runtime.getRuntime().exec("/home/pi/temperv14/usb-thermometer/pcsensor -d" + num);
			try
			{
				try (final InputStream is = p.getInputStream())
				{
					try (final BufferedReader r = new BufferedReader(new InputStreamReader(is)))
					{
						String l = null;
						while ((l = r.readLine()) != null)
						{
							final String[] elements = l.split(" ");
							final String stringCelsiusTemp = elements[4];
//							System.out.println(stringCelsiusTemp);
							if (stringCelsiusTemp.endsWith("C"))
							{
								final double temp = Double.parseDouble(stringCelsiusTemp.substring(0, stringCelsiusTemp.length() - 1));
								if ((temp > 0) && (temp < 90))
									pValues.add(temp);
								break;
							}
							else
								System.out.println(l);
						}
					}
				}
			}
			finally
			{
				p.destroy();
			}
		}
		catch (NumberFormatException | IOException e)
		{
			e.printStackTrace();
			throw new GetterException(e);
		}
	}

	@Override
	public void disconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 30;
	}
}
