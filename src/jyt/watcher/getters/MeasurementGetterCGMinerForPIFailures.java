package jyt.watcher.getters;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.jcraft.jsch.JSchException;

import jyt.watcher.GetterException;
import jyt.watcher.IMeasurementGetter;
import jyt.watcher.JSchConnector;
import jyt.watcher.Measurement;

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * WARNING: currently not operational
 * @author jytou
 */
public class MeasurementGetterCGMinerForPIFailures implements IMeasurementGetter
{
	private JSchConnector mConnector = null;
	private double mCurrentStability = 3;
	private long mLastTick = 0;

	public MeasurementGetterCGMinerForPIFailures() throws GetterException
	{
		super();
		new Thread("PICGReaderForFailures")
		{
			public void run()
			{
				int nbvalues = 0;
				int[] values = new int[30];
				int curvaluepos = 0;
				while (true)
				{
					try
					{
						if (mConnector == null)
						{
							reconnect();
							try
							{
								mConnector.writeLine("sudo screen -x gs");
							}
							catch (IOException e)
							{
								disconnect();
								throw new GetterException(e);
							}
						}
						String l;
						try
						{
							while ((l = mConnector.readLine()) != null)
							{
								l = l.trim();
								int pos = 0;
								while (pos != -1)
								{
									int posaccepted = l.indexOf("Accepted", pos);
									int posrejected = l.indexOf("Rejected", pos);
									if (posaccepted == -1)
										if (posrejected == -1)
											pos = -1;// nothing to do here
										else
											pos = posrejected;
									else
										if (posrejected == -1)
											pos = posaccepted;
										else
											pos = Math.min(posaccepted, posrejected);
									if (pos != -1)
									{
										int gsdpos = l.indexOf("GSD ", pos);
										if (gsdpos != -1)
										{
											try
											{
												int gsdnum = Integer.parseInt(l.substring(gsdpos + 4, gsdpos + 5));
												if (gsdnum > 2)
													throw new NumberFormatException();
												//System.out.println("New gridseed found " + gsdnum);
												synchronized (MeasurementGetterCGMinerForPIFailures.this)
												{
													mLastTick = System.currentTimeMillis();
													values[curvaluepos] = gsdnum;
													curvaluepos++;
													if (curvaluepos >= values.length)
														curvaluepos = 0;
													if (nbvalues < values.length)
														nbvalues++;
													if (nbvalues >= 15)
													{
														// collect statistics
														int[] nbeach = new int[3];
														nbeach[0] = 0;
														nbeach[1] = 0;
														nbeach[2] = 0;
														for (int i = 0; i < nbvalues; i++)
															nbeach[values[i]]++;
														double total = 0;
														for (int i = 0; i < nbeach.length; i++)
															total += Math.min(1, (3.0 * nbeach[i]) / nbvalues);
														mCurrentStability = total;
													}
												}
											}
											catch (NumberFormatException e)
											{
												// could not find the number, leave it!
											}
										}
										pos++;
									}
								}
							}
							mConnector.sendChar('\u0001');
							mConnector.sendChar('\u0004');
						}
						catch (IOException e)
						{
							disconnect();
						}
					}
					catch (Exception e)
					{
						if (mConnector != null)
							try
							{
								disconnect();
							}
							catch (Exception e2)
							{
							}
						mConnector = null;
						try
						{
							Thread.sleep(10000);
						}
						catch (InterruptedException e1)
						{
						}
					}
				}
			}
		}.start();
	}

	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		synchronized (this)
		{
			res.put("gbst", new Measurement("gbst", mCurrentStability));
			res.put("gbstt", new Measurement("gbstt", (System.currentTimeMillis() - mLastTick) / 1000));
		}
		return res;
	}

	public void disconnect() throws GetterException
	{
		try
		{
			if (mConnector != null)
				mConnector.sendChar('\u0004');
		}
		catch (IOException e)
		{
			throw new GetterException(e);
		}
		finally
		{
			mConnector = null;
		}
	}

	@Override
	public void reconnect() throws GetterException
	{
		try
		{
			mConnector = new JSchConnector("user", "localhost", "pass");
		}
		catch (JSchException e)
		{
			throw new GetterException(e);
		}
		catch (IOException e)
		{
			throw new GetterException(e);
		}
	}

	@Override
	public Integer getDefaultTemporization()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
