package jyt.watcher.getters;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import com.jcraft.jsch.JSchException;

import jyt.watcher.GetterException;
import jyt.watcher.IFeedbackListener;
import jyt.watcher.IMeasurementGetter;
import jyt.watcher.JSchConnector;
import jyt.watcher.Measurement;
import jyt.watcher.StateSaver;
import jyt.watcher.Watched;

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!
 * WARNING: currently not operational
 * @author jytou
 */
public class MeasurementGetterCGMiner implements IMeasurementGetter
{
	private JSchConnector mConnector = null;
	private IFeedbackListener mFeedbackListener;
	private StateSaver mStateSaver;

	public MeasurementGetterCGMiner(IFeedbackListener pFeedbackListener, StateSaver pStateSaver) throws GetterException
	{
		super();
		mFeedbackListener = pFeedbackListener;
		mStateSaver = pStateSaver;
	}

	public Map<String, Measurement> getData() throws GetterException
	{
		final Map<String, Measurement> res = new HashMap<String, Measurement>();
		final Semaphore semaphore = new Semaphore(0);
		final GetterException[] exception = new GetterException[] {null};
		Thread thread = new Thread()
		{
			public void run()
			{
				try
				{
					synchronized (JSchConnector.class)
					{
						reconnect();
						try
						{
//							try
//							{
//								mConnector.writeLine((WatchMinerEngine.Host.PI.equals(mHost) ? "sudo " : "") + "screen -x " + (WatchMinerEngine.Host.JYDESK.equals(mHost) ? "gpu" : "gs"));
//								mFeedbackListener.readCGMinerOK();
//							}
//							catch (IOException e)
//							{
//								mFeedbackListener.readCGMinerError("Could not connect to GS: " + e.getMessage(), e);
//								throw new GetterException(e);
//							}
							String l;
							String original = null;
							boolean foundGPUs = false;
							try
							{
								int nbLines = 0;
//								System.err.println("***************** BEGIN READ ********************");
								while ((l = mConnector.readLine()) != null)
								{
									original = l;
									l = l.trim();
//									System.out.println(l);
									if (l.startsWith("(5s)"))
									{
										try
										{
											String curSpeed = l.substring(l.indexOf(':') + 1, l.indexOf(' ')).trim();
//											System.out.println("Found speed " + curSpeed);
											double unit = 0.000001;
											if (curSpeed.endsWith("M"))
											{
												curSpeed = curSpeed.substring(0, curSpeed.length() - 1);
												unit = 1;
											}
											else if (curSpeed.endsWith("K"))
											{
												curSpeed = curSpeed.substring(0, curSpeed.length() - 1);
												unit = 0.001;
											}
											final double value = Double.parseDouble(curSpeed);
//											final String name = (WatchMinerEngine.Host.JYDESK.equals(mHost) ? "mg" : "gb") + "s";
//											res.put(name, new Measurement(name, value));
//											mFeedbackListener.readCGMinerOK();
										}
										catch (Exception e)
										{
											// ignore silently
											mFeedbackListener.readCGMinerError("Error in measuring the overall speed " + e.getClass().getName() + " said :" + e.getMessage(), e);
										}
									}
									else
									{
										if (l.startsWith("GPU "))
										{
											int number = -1;
											double value;
											String name;
											boolean breakIt = false;
											try
											{
												number = Integer.parseInt(l.substring(l.indexOf(' ') + 1, l.indexOf(':')).trim());
//												System.out.println("Found GPU " + number);
												foundGPUs = true;
												breakIt = (number == 6);
												value = Double.parseDouble(l.substring(l.indexOf(':') + 1, l.indexOf('C')).trim());
												name = "mgt" + number;
												res.put(name, new Measurement(name, value));
												final String speed = l.substring(l.indexOf('|') + 1, l.indexOf('/')).trim();
												if ("OFF".equals(speed))
												{
													value = 0;
													final Watched watched = Watched.getWatchedByShortName(name);
													if (watched.getState() == Watched.STATE_ENABLED)
													{
														watched.setState(Watched.STATE_DISABLED);
														mStateSaver.toSave(watched);
														mFeedbackListener.disablingCard(number);
													}
												}
												else if ("SICK".equals(speed))
												{
													value = 0;
													final Watched watched = Watched.getWatchedByShortName(name);
													if (watched.getState() != Watched.STATE_SICK)
													{
														watched.setState(Watched.STATE_SICK);
														mStateSaver.toSave(watched);
														mFeedbackListener.ignoringCard(number);
													}
												}
												else
												{
													if (speed.endsWith("M"))
														value = Double.parseDouble(speed.substring(0, speed.length() - 1));
													else
														// Whoopsie, it's not M, could be K, so speed near 0
														value = 0;
													final Watched watched = Watched.getWatchedByShortName(name);
													if (watched.getState() == Watched.STATE_DISABLED)
													{
														watched.setState(Watched.STATE_ENABLED);
														mStateSaver.toSave(watched);
														mFeedbackListener.enablingCard(number);
													}
												}
												name = "mgs" + number;
												res.put(name, new Measurement(name, value));
												mFeedbackListener.readGPUInfoOk(number);
											}
											catch (Exception e)
											{
												mFeedbackListener.readGPUInfoError(number, "Error in measuring one GPU speed " + e.getClass().getName() + " said :" + e.getMessage(), e);
											}
											finally
											{
												if (breakIt)
													break;
											}
										}
										else if (l.startsWith("GSD "))
										{
											boolean breakIt = false;
											try
											{
												int number = Integer.parseInt(l.substring(l.indexOf(' ') + 1, l.indexOf(':')).trim());
												foundGPUs = true;
												breakIt = (number == 2);
												final String speed = l.substring(l.indexOf('|') + 1, l.indexOf('/') - 1).trim();
												double value = Double.parseDouble(speed);
												final String name = "gbs" + number;
												res.put(name, new Measurement(name, value));
											}
											catch (Exception e)
											{
												System.err.println("Error in measuring one GSD speed " + e.getClass().getName() + " said :" + e.getMessage());
												//mFeedbackListener. TODO this should come here
												//e.printStackTrace();
											}
											finally
											{
												if (breakIt)
													break;
											}
										}
										else if (foundGPUs && (l.startsWith("-----")))
										{
											// we've reached the end of the gpus: break
											break;
										}
									}
									if (nbLines++ > 100)
										// we shouldn't get that far... if we did then we missed something
										break;
								}
							}
							catch (IOException e)
							{
								mFeedbackListener.readCGMinerError("IOException while reading data from CGMiner: " + e.getMessage(), e);
								throw new GetterException(e);
							}
							mFeedbackListener.readCGMinerOK();
						}
						finally
						{
							disconnect();
						}
					}
				}
				catch (GetterException ge)
				{
					exception[0] = ge;
					mFeedbackListener.readCGMinerError("Timed out when reading CGMiner", ge);
				}
				finally
				{
					semaphore.release();
				}
			}
		};
		thread.start();
		try
		{
//			semaphore.acquire();
			boolean acquired = semaphore.tryAcquire(5, TimeUnit.SECONDS);
			if (!acquired)
			{
				thread.interrupt();
				disconnect();
			}
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		return res;
	}

	public synchronized void disconnect() throws GetterException
	{
		if (mConnector != null)
		{
			mConnector.close();
			mConnector = null;
//			System.out.println("Disconnecting for read CGMiner");
		}
	}

	@Override
	public void reconnect() throws GetterException
	{
		disconnect();
//		try
//		{
//			mConnector = new JSchConnector(WatchMinerEngine.Host.JYDESK.equals(mHost) ? "jytou" : "pi", "localhost", "");
//		}
//		catch (JSchException e)
//		{
//			throw new GetterException(e);
//		}
//		catch (IOException e)
//		{
//			throw new GetterException(e);
//		}
//		System.out.println("Connecting for read CGMiner");
	}

	@Override
	public Integer getDefaultTemporization()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
