package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.eclipsesource.json.JsonObject;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;
import jyt.watcher.Watched;

/**
 * Requires upsc to be installed and correctly configured.
 * @author jytou
 */
public class MeasurementGetterUPS extends MeasurementGetterAbstract
{
	private static final String LOAD_SHORT = "l";
	private static final String CHARGE_SHORT = "c";
	private String mUPSName;

	public MeasurementGetterUPS(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "UPS Status", "ups");
		mUPSName = pConfig.get("ups").asString();
		Watched.registerWatch(mShortName + CHARGE_SHORT, mLongName + " charge", mSensorName + " charge", mLongHost, mShortHost, mTypeLong, mTypeShort + CHARGE_SHORT, mUPSName, null, mIntervalWarn, null, null, null, null, null, null);
		Watched.registerWatch(mShortName + LOAD_SHORT, mLongName + " load", mSensorName + " load", mLongHost, mShortHost, mTypeLong, mTypeShort + LOAD_SHORT, mUPSName, null, mIntervalWarn, mBoundMin, mBoundMax, mWarnMin, mWarnMax, null, null);
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		try
		{
			final Process exec = Runtime.getRuntime().exec("/bin/upsc " + mUPSName);
			final BufferedReader r = new BufferedReader(new InputStreamReader(exec.getInputStream()));
			try
			{
				String l;
				while ((l = r.readLine()) != null)
				{
					l = l.trim();
					if (l.startsWith("battery.charge:"))
					{
						try
						{
							final int charge = Integer.parseInt(l.substring(l.indexOf(':') + 1).trim());
							res.put(mShortName + CHARGE_SHORT, new Measurement(mShortName + CHARGE_SHORT, charge));
						}
						catch (NumberFormatException e)
						{
							res.put(mShortName + CHARGE_SHORT, new Measurement(mShortName + CHARGE_SHORT, -1));
						}
					}
					else if (l.startsWith("ups.load:"))
					{
						try
						{
							final int load = Integer.parseInt(l.substring(l.indexOf(':') + 1).trim());
							res.put(mShortName + LOAD_SHORT, new Measurement(mShortName + LOAD_SHORT, load));
						}
						catch (NumberFormatException e)
						{
							res.put(mShortName + LOAD_SHORT, new Measurement(mShortName + LOAD_SHORT, -1));
						}
					}
				} 
			}
			finally
			{
				r.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// nothing to do here
	}
}
