package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.eclipsesource.json.JsonObject;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;

/**
 * Requires vcgencmd to be installed. There is another (generic) way of getting the temperature on a RaspberryPi, use {@link MeasurementGetterPi3TempThermalZone} for this.
 * @author jytou
 */
public class MeasurementGetterPi3TempVCGen extends MeasurementGetterAbstract
{
	public MeasurementGetterPi3TempVCGen(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "PI3 VCGen Temperature", "pi3vt");
		setListenProperties(pConfig, 120, 0.0, 80.0, null, 75.0);
		registerDefault();
	}


	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		try
		{
			Process p = Runtime.getRuntime().exec("/opt/vc/bin/vcgencmd measure_temp");
			InputStream is = p.getInputStream();
			BufferedReader r = new BufferedReader(new InputStreamReader(is));
			try
			{
				String l;
				if ((l = r.readLine()) != null)
				{
					if (l.startsWith("temp"))
					{
						try
						{
							double value = Double.parseDouble(l.substring(l.indexOf('=') + 1, l.indexOf('\'')));
							res.put(mShortName, new Measurement(mShortName, value));
						}
						catch (NumberFormatException e)
						{
							throw new GetterException(e);
						}
					}
				} 
			}
			finally
			{
				r.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}
}
