package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;
import jyt.watcher.StringUtil;
import jyt.watcher.Watched;

/**
 * @author jytou
 * @deprecated use {@link MeasurementGetterHDD} instead
 */
public class MeasurementGetterOldHDDTemp extends MeasurementGetterAbstract
{
	private Map<String, String> mKnownHDD = new HashMap<>();
	private Set<String> mToIgnore = new HashSet<>();
	private String mDisks;

	public MeasurementGetterOldHDDTemp(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "HDD", "d");
		final StringBuilder sb = new StringBuilder();
		if (pConfig.contains("ignore"))
			mToIgnore.addAll(Arrays.asList(StringUtil.split(pConfig.get("ignore").asString(), ",")));
		final String disks = pConfig.get("disks").asString();
		for (int i = 0; i < disks.length(); i++)
			sb.append(' ').append("/dev/sd").append(disks.charAt(i));
		mDisks = sb.toString();
		final JsonArray hdds = pConfig.get("hdds").asArray();
		for (JsonValue jsonValue : hdds)
		{
			final JsonObject sensorObject = jsonValue.asObject();
			final String shortName = sensorObject.get("short").asString();
			final Integer intervalWarn = sensorObject.contains("intervalWarn") ? sensorObject.get("intervalWarn").asInt() : null;
			final Double boundMin = sensorObject.contains("min") ? sensorObject.get("min").asDouble() : null;
			final Double boundMax = sensorObject.contains("max") ? sensorObject.get("max").asDouble() : null;
			final Double warnMin = sensorObject.contains("minWarn") ? sensorObject.get("minWarn").asDouble() : null;
			final Double warnMax = sensorObject.contains("maxWarn") ? sensorObject.get("maxWarn").asDouble() : null;
			final String sensorName = sensorObject.get("name").asString();
			mKnownHDD.put(sensorName, shortName);
			final String sensorTypeLong = sensorObject.contains("typeLong") ? sensorObject.get("typeLong").asString() : "HDD Temperature";
			final String sensorTypeShort = sensorObject.contains("typeShort") ? sensorObject.get("typeShort").asString() : "hdt";
			final String sensorInstanceLong = sensorObject.contains("instanceLong") ? sensorObject.get("instanceLong").asString() : sensorName;
			final String sensorInstanceShort = sensorObject.contains("instanceShort") ? sensorObject.get("instanceShort").asString() : shortName;
			final String longName = sensorObject.contains("long") ? sensorObject.get("long").asString() : pLongHost + " " + sensorTypeLong + " " + sensorInstanceLong;
			Watched.registerWatch(shortName, longName, sensorName, pLongHost, pShortHost, sensorTypeLong, sensorTypeShort, sensorInstanceLong, sensorInstanceShort, intervalWarn, boundMin, boundMax, warnMin, warnMax, null, null);
		}
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		try
		{
			final Process p = Runtime.getRuntime().exec("sudo /usr/sbin/hddtemp" + mDisks);
			final InputStream is = p.getInputStream();
			final BufferedReader r = new BufferedReader(new InputStreamReader(is));
			final List<String> errors;
			try
			{
				String l;
				errors = new ArrayList<>();
				while ((l = r.readLine()) != null)
				{
					final StringTokenizer st = new StringTokenizer(l, ":", false);
					st.nextToken();// skip the "/dev/sd." part
					final String hdName = st.nextToken().trim();
					if (mKnownHDD.containsKey(hdName))
					{
						final String shortName = mKnownHDD.get(hdName);
						final String temp = st.nextToken().trim();
						try
						{
							final int value = Integer.parseInt(temp.substring(0, temp.length() - 2));
							res.put(shortName, new Measurement(shortName, value));
						}
						catch (NumberFormatException e)
						{
							errors.add("Could not parse temp " + temp + " for disk " + hdName);
						}
					}
					else if (!mToIgnore.contains(hdName))
						System.err.println("Unknown HDD: " + hdName);
				} 
			}
			finally
			{
				r.close();
			}
			if (!errors.isEmpty())
				throw new GetterException(errors.toString());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 60*5;
	}
}
