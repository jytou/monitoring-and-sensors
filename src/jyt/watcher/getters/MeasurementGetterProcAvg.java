package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.eclipsesource.json.JsonObject;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;

public class MeasurementGetterProcAvg extends MeasurementGetterAbstract
{
	public MeasurementGetterProcAvg(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "CPU Load", "cl");
		setListenProperties(pConfig, 120, null, null, null, null);
		registerDefault();
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		BufferedReader r = null;
		try
		{
			r = new BufferedReader(new InputStreamReader(new FileInputStream("/proc/loadavg")));
			String l;
			if ((l = r.readLine()) != null)
			{
				try
				{
					double value = Double.parseDouble(l.substring(0, l.indexOf(' ')));
					res.put(mShortName, new Measurement(mShortName, value));
				}
				catch (NumberFormatException e)
				{
					throw new GetterException(e);
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (r != null)
				try
				{
					r.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// nothing to do here
	}
}
