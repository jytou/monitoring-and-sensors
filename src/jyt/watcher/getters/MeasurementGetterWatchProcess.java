package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.eclipsesource.json.JsonObject;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;

public class MeasurementGetterWatchProcess extends MeasurementGetterAbstract
{
	private String[] mWatchedProcesses;
	private boolean mAsRoot;
	private String mTargetUser = null;

	public MeasurementGetterWatchProcess(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "Watch Processes", "proc");
		final String processes = pConfig.get("watched").asString();
		mWatchedProcesses = processes.split(",");
		mAsRoot = pConfig.contains("asRoot");
		if (pConfig.contains("user"))
			mTargetUser = pConfig.get("user").asString();
		setListenProperties(pConfig, 120, -0.5, 0.5, null, null);
		mBoundMin = -0.5;
		mBoundMax = 0.5;
		registerDefault();
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		final Map<String, Measurement> res = new HashMap<>();
		try
		{
			final Process exec = Runtime.getRuntime().exec((mAsRoot ? "sudo " : "") + "/bin/ps " + (mTargetUser == null ? "-aeux" : "-u " + mTargetUser));
			final BufferedReader r = new BufferedReader(new InputStreamReader(exec.getInputStream()));
			try
			{
				String l;
				Set<String> watched = new HashSet<>(Arrays.asList(mWatchedProcesses));
				while ((l = r.readLine()) != null)
				{
					l = l.trim();
					for (String w : watched)
						if (Pattern.compile(w).matcher(l).find())
						{
							watched.remove(w);
							break;
						}
				}
				res.put(mShortName, new Measurement(mShortName, watched.size()));
			}
			finally
			{
				r.close();
			}
			exec.destroy();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
	}

	@Override
	public void reconnect() throws GetterException
	{
	}
}
