package jyt.watcher.getters;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonObject.Member;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;
import jyt.watcher.WatchMinerEngine;
import jyt.watcher.Watched;

import com.eclipsesource.json.JsonValue;

public class MeasurementGetterDeconzSensors extends MeasurementGetterAbstract
{
	private static final String SENSOR_PREFIX = "sen";

	private class KnownType
	{
		String fieldAccessName;
		String shortNameCode;
		double factor;
		public KnownType(String pFieldAccessName, String pShortNameCode, double pFactor)
		{
			super();
			fieldAccessName = pFieldAccessName;
			shortNameCode = pShortNameCode;
			factor = pFactor;
		}
	}
	// Known sensors
	private Map<String, String> mKnownSensors = new HashMap<>();
	private Map<String, String> mUniqueIdsToNames = new HashMap<>();
	private Map<String, KnownType> mKnownTypes = new HashMap<>();
	private Map<String, String> mLatest = new HashMap<>();
	private String mURL;

	public MeasurementGetterDeconzSensors(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "Deconz", "dz");
		mURL = pConfig.get("url").asString();
		for (JsonValue sensor : pConfig.get("sensors").asArray())
		{
			final JsonObject sensorObject = sensor.asObject();
			final String shortName = sensorObject.get("short").asString();
			final String sensorName = sensorObject.get("name").asString();
			final String uniqueId = sensorObject.getString("uniqueid", null);
			if (uniqueId != null)
			{
				System.out.println("Registering sensor " + sensorName + " as uniqueid " + uniqueId);
				mKnownSensors.put(uniqueId, shortName);
//				mUniqueIdsToNames.put(uniqueId, sensorName);
			}
			else
				mKnownSensors.put(sensorName, shortName);
			final String types = sensorObject.get("type").asString();
			String longType;
			Integer intervalWarn = null;
			Double boundMin = null;
			Double boundMax = null;
			Double warnMin = null;
			Double warnMax = null;
			for (int i = 0; i < types.length(); i++)
			{
				Double factor1 = null;
				Double offset2 = null;
				final char currentType = types.charAt(i);
				switch (currentType)
				{
				case 'b':
					longType = "Battery";
					intervalWarn = null;
					boundMin = 30.0;
					boundMax = 130.0;
					warnMin = 40.0;
					warnMax = 130.0;
					break;
				case 'h':
					longType = "Humidity";
					intervalWarn = null;
					boundMin = 0.0;
					boundMax = 100.0;
					warnMin = 15.0;
					warnMax = 95.0;
					break;
				case 'p':
					longType = "Pressure";
					intervalWarn = null;
					boundMin = 940.0;
					boundMax = 1040.0;
					warnMin = 960.0;
					warnMax = 1028.0;
					break;
				case 't':
					longType = "Temperature";
					intervalWarn = null;
					boundMin = -20.0;
					boundMax = 50.0;
					warnMin = -10.0;
					warnMax = 40.0;
					break;
				case 'w':
					longType = "Water";
					intervalWarn = null;
					boundMin = -0.5;
					boundMax = 0.5;
					warnMin = -0.5;
					warnMax = 0.5;
					break;
				case 'l':
					longType = "Light";
					intervalWarn = null;
					boundMin = 0.0;
					boundMax = 100000.0;
					warnMin = 0.0;
					warnMax = 100000.0;
					break;
				case 'o':
					longType = "Open/Close";
					intervalWarn = null;
					boundMin = -100.0;
					boundMax = 200.0;
					warnMin = -100.0;
					warnMax = 200.0;
					break;
				default:
					throw new RuntimeException("Unknown sensor type : " + currentType);
				}
				boundMin = sensorObject.getDouble("min." + currentType, boundMin);
				boundMax = sensorObject.getDouble("max." + currentType, boundMax);
				warnMin = sensorObject.getDouble("minWarn." + currentType, warnMin);
				warnMax = sensorObject.getDouble("maxWarn." + currentType, warnMax);
				if (sensorObject.contains(currentType + "Factor1"))
					factor1 = sensorObject.get(currentType + "Factor1").asDouble();
				if (sensorObject.contains(currentType + "Offset2"))
					offset2 = sensorObject.get(currentType + "Offset2").asDouble();
				Watched.registerWatch(SENSOR_PREFIX + shortName + currentType, "Sensor " + sensorName + " " + longType, sensorName, WatchMinerEngine.SENSORS_HOME_LONG, WatchMinerEngine.SENSORS_HOME_SHORT, longType, String.valueOf(currentType), null, null, intervalWarn, boundMin, boundMax, warnMin, warnMax, factor1, offset2);
				if ('w' == currentType)
				{
					if (sensorObject.contains("tFactor1"))
						factor1 = sensorObject.get("tFactor1").asDouble();
					else
						factor1 = null;
					if (sensorObject.contains("tOffset2"))
						offset2 = sensorObject.get("tOffset2").asDouble();
					else
						offset2 = null;
					Watched.registerWatch(SENSOR_PREFIX + shortName + "t", "Sensor " + sensorName + " Temperature", sensorName, WatchMinerEngine.SENSORS_HOME_LONG, WatchMinerEngine.SENSORS_HOME_SHORT, "Temperature", "t", null, null, null, -20.0, 50.0, -10.0, 40.0, factor1, offset2);
				}
				else if ('l' == currentType)
					Watched.registerWatch(SENSOR_PREFIX + shortName + "x", "Sensor " + sensorName + " Lux", sensorName, WatchMinerEngine.SENSORS_HOME_LONG, WatchMinerEngine.SENSORS_HOME_SHORT, "Lux Light", "x", null, null, intervalWarn, boundMin, boundMax, warnMin, warnMax, factor1, offset2);
				else if ('o' == currentType)
				{
					if (sensorObject.contains("tFactor1"))
						factor1 = sensorObject.get("tFactor1").asDouble();
					else
						factor1 = null;
					if (sensorObject.contains("tOffset2"))
						offset2 = sensorObject.get("tOffset2").asDouble();
					else
						offset2 = null;
					Watched.registerWatch(SENSOR_PREFIX + shortName + "t", "Sensor " + sensorName + " Temperature", sensorName, WatchMinerEngine.SENSORS_HOME_LONG, WatchMinerEngine.SENSORS_HOME_SHORT, "Temperature", "t", null, null, null, -20.0, 50.0, -10.0, 40.0, factor1, offset2);
				}
			}
		}
		mKnownTypes.put("ZHAPressure", new KnownType("pressure", "p", 1));
		mKnownTypes.put("ZHATemperature", new KnownType("temperature", "t", 0.01));
		mKnownTypes.put("ZHAHumidity", new KnownType("humidity", "h", 0.01));
		mKnownTypes.put("ZHAWater", new KnownType("water", "w", 1));
		mKnownTypes.put("ZHALightLevel", new KnownType("light", "l", 1));
		mKnownTypes.put("ZHAOpenClose", new KnownType("open", "o", 1));
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		final Map<String, Measurement> res = new HashMap<String, Measurement>();
		JsonValue json = null;
		try
		{
			final HttpURLConnection conn = (HttpURLConnection)new URL(mURL).openConnection();
//			final BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//			String input;
//			StringBuilder jsonString = new StringBuilder();
//			while ((input = br.readLine()) != null)
//				jsonString.append(input);
//			br.close();
			json = Json.parse(new InputStreamReader(conn.getInputStream()));
			conn.disconnect();
			final JsonObject sensors = (json.asObject()).get("sensors").asObject();
			sensors.forEach(new Consumer<Member>()
			{
				@Override
				public void accept(Member s)
				{
					final JsonObject sensorData = s.getValue().asObject();
					final String sensorType = sensorData.getString("type", null);
					final String sensorName = sensorData.getString("name", null);
					final String sensorUniqueId = sensorData.getString("uniqueid", null);
					if ((sensorType != null) && (sensorName != null))
					{
						String sensor = mKnownSensors.get(sensorName);
						if ((sensor == null) && (sensorUniqueId != null))
						// This sensor's name was not found, maybe it is using the uniqueId instead.
						{
							sensor = mKnownSensors.get(sensorUniqueId.substring(0, sensorUniqueId.indexOf('-')));
//							if (sensor != null)
//							{
//								System.out.println("Sensor uniqueid " + sensor + " remapped to " + mUniqueIdsToNames.get(sensor));
//								sensor = mUniqueIdsToNames.get(sensor);
//							}
						}
						final KnownType typeData = mKnownTypes.get(sensorType);
						if ((sensor != null) && (typeData != null))
						{
							final JsonObject configObject = sensorData.get("config").asObject();
							final boolean on = configObject.getBoolean("on", false);
							final boolean reachable = configObject.getBoolean("reachable", false);
							final JsonObject sensorStateObject = sensorData.get("state").asObject();
							final String timing = sensorStateObject.getString("lastupdated", null);
							if (!reachable)
								System.err.println("Sensor " + sensor + " is unreachable!");
							else if (on && (timing != null))
							{
								// first battery
								if (configObject.contains("battery"))
								{
									final JsonValue batval = configObject.get("battery");
									if ((batval != null) && (!batval.isNull()))
									{
										final double bat = batval.asDouble();
										if (bat != -1)
										{
											final String powDataShortName = SENSOR_PREFIX + sensor + "b";
											addData(res, powDataShortName, timing, bat);
										}
									}
								}
								if ("water".equals(typeData.fieldAccessName))
								{
									final boolean water = sensorStateObject.get("water").asBoolean();
									addData(res, SENSOR_PREFIX + sensor + typeData.shortNameCode, timing, water ? 100 : 0);
									// because temperature is also stored but in a different way, just get it as an additional field
									final double temp = configObject.getDouble("temperature", -1);
									if (temp != -1)
										addData(res, SENSOR_PREFIX + sensor + "t", timing, temp * 0.01);
								}
								else if ("open".equals(typeData.fieldAccessName))
								{
									final boolean open = sensorStateObject.get("open").asBoolean();
									addData(res, SENSOR_PREFIX + sensor + typeData.shortNameCode, timing, open ? 100 : 0);
//									System.out.println("Door is " + (open ? "open" : "closed"));
									// temperature seems to be very unreliable on those sensors
									final double temp = configObject.getDouble("temperature", -1);
									if (temp != -1)
										addData(res, SENSOR_PREFIX + sensor + "t", timing, temp * 0.01);
								}
								else if ("light".equals(typeData.fieldAccessName))
								{
									final int light = sensorStateObject.get("lightlevel").asInt();
									addData(res, SENSOR_PREFIX + sensor + "l", timing, light);
									// add the lux value as well
									final int lux = sensorStateObject.get("lux").asInt();
									if (lux != -1)
										addData(res, SENSOR_PREFIX + sensor + "x", timing, lux);
								}
								else
								{
									final JsonValue jsonVal = sensorStateObject.get(typeData.fieldAccessName);
									if ((jsonVal != null) && (!jsonVal.isNull()))
									{
										final double data = jsonVal.asDouble();
										if (data != -1)
											addData(res, SENSOR_PREFIX + sensor + typeData.shortNameCode, timing, data * typeData.factor);
									}
								}
							}
						}
					}
				}
			});
		}
		catch (Exception e)
		{
			System.err.println(e.getClass().getName() + " saying " + e.getMessage());
			if (json != null)
				System.err.println(json.toString());
			e.printStackTrace();
		}
		return res;
	}

	protected void addData(Map<String, Measurement> pRes, String pShortName, String pTiming, double pValue)
	{
		final String latest = mLatest.get(pShortName);
		if ((latest == null) || (latest.compareTo(pTiming) < 0))
		{
			Timestamp time = Timestamp.valueOf(LocalDateTime.now().minusHours(2));
			try
			{
				time = Timestamp.valueOf(LocalDateTime.parse(pTiming, DateTimeFormatter.ISO_DATE_TIME).plusHours(1));
			}
			catch (Exception e)
			{
				System.err.println("Could not read time for " + pShortName);
			}
			pRes.put(pShortName, new Measurement(Watched.getWatchedByShortName(pShortName).getId(), time, pValue));
			mLatest.put(pShortName, pTiming);
		}
	}

	@Override
	public void disconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}
}
