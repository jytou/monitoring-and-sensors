package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.eclipsesource.json.JsonObject;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;

public class MeasurementGetterPing extends MeasurementGetterAbstract
{
	private String mHost;
	private int mNbRequests;

	public MeasurementGetterPing(final String pLongHost, final String pShortHost, final JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "Network Ping", "np");
		mHost = pConfig.get("host").asString();
		mNbRequests = pConfig.getInt("requests", 5);
		registerDefault();
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		String s = null;
		try
		{
			ProcessBuilder pb = new ProcessBuilder(Arrays.asList(new String[] {"/bin/ping", "-c", "" + mNbRequests, mHost}));
			Process process = pb.start();

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
			BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

			// read the output from the command
			// read any errors from the attempted command
			while ((s = stdError.readLine()) != null)
			{
				// if there is any, that is not good
				res.put(mShortName, new Measurement(mShortName, -1));
				break;
			}

			if (res.isEmpty())
			{
				double percentLoss = 0;
				while ((s = stdInput.readLine()) != null)
				{
					final int percentPos = s.indexOf("% ");
					if (percentPos != -1)
					{
						final String percentStr = s.substring(s.lastIndexOf(' ', percentPos), percentPos).trim();
						percentLoss = Double.valueOf(percentStr);
					}
					if (s.startsWith("rtt "))
					{
						final String results = s.split("=")[1];
						final String avgStr = results.split("/")[1];
						final double val = Double.valueOf(avgStr);
						res.put(mShortName, new Measurement(mShortName, val*(100.0-percentLoss)/100.0 + 4000.0*percentLoss/100.0));
						break;
					}
				}
				if (res.isEmpty())
					res.put(mShortName, new Measurement(mShortName, -10000));
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (NumberFormatException e)
		{
			if (s != null)
				System.out.println("####### ERROR FOR PING ON LINE : " + s);
			throw e;
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 60 * 2;
	}
}
