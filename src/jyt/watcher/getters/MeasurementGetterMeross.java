package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;
import jyt.watcher.WatchMinerEngine;
import jyt.watcher.Watched;

/**
 * This fully depends on a python script using the MerosIot library.
 * @author jytou
 */
public class MeasurementGetterMeross extends MeasurementGetterAbstract
{
	private static final String POWER_START = "POWER = ";
	private static final String LINE_START = "Current consumption data for ";
	private static final String SENSOR_TYPE_LONG = "Meross Plugs";
	private static final String SENSOR_TYPE_SHORT = "pow";

	private ProcessWatcherThread mWatcherThread = null;
	enum ProcessStatus {STARTED, ENDED, QUIT};
	static class ObjectWithProcess
	{
		Process p;
		ProcessStatus status;
		public ObjectWithProcess(Process pP, ProcessStatus pStatus)
		{
			super();
			p = pP;
			status = pStatus;
		}
	}

	static class ProcessWatcherThread extends Thread
	{
		BlockingQueue<ObjectWithProcess> mProcessQueue = new LinkedBlockingQueue<>(1);
		public void run()
		{
			try
			{
				while (true)
				{
					final ObjectWithProcess obj = mProcessQueue.take();
					if (ProcessStatus.QUIT.equals(obj.status))
						break;
					if (obj.status.equals(ProcessStatus.STARTED))
					{
						final ObjectWithProcess finished = mProcessQueue.poll(1, TimeUnit.MINUTES);
						if (finished == null)
						{
							if (obj.p.isAlive())
							{
								System.err.println("*************** Killing process after 1 minute timeout!... **********");
								obj.p.destroy();
							}
						}
					}
				}
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	private Set<String> mSupportedCardNumbers = new HashSet<>();
	private String mCommand;

	public MeasurementGetterMeross(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, SENSOR_TYPE_LONG, SENSOR_TYPE_SHORT);
		mWatcherThread = new ProcessWatcherThread();
		mWatcherThread.start();
		mCommand = pConfig.get("command").asString();
		for (JsonValue card : pConfig.get("plugs").asArray())
		{
			final JsonObject cardObject = card.asObject();
			final String plugNumber = cardObject.get("number").asString();
			final String plugName = cardObject.get("name").asString();
			mSupportedCardNumbers.add(plugNumber);
			final Integer intervalWarn = cardObject.contains("intervalWarn") ? cardObject.get("intervalWarn").asInt() : null;
			final Double boundMin = cardObject.contains("min") ? cardObject.get("min").asDouble() : null;
			final Double boundMax = cardObject.contains("max") ? cardObject.get("max").asDouble() : null;
			final Double warnMin = cardObject.contains("minWarn") ? cardObject.get("minWarn").asDouble() : null;
			final Double warnMax = cardObject.contains("maxWarn") ? cardObject.get("maxWarn").asDouble() : null;
			Watched.registerWatch(SENSOR_TYPE_SHORT + "apow" + plugNumber, SENSOR_TYPE_LONG + " " + plugName, SENSOR_TYPE_SHORT, WatchMinerEngine.POWER_PLUG_MEROSS_LONG, WatchMinerEngine.POWER_PLUG_MEROSS_SHORT, SENSOR_TYPE_LONG, SENSOR_TYPE_SHORT, null, null, intervalWarn, boundMin, boundMax, warnMin, warnMax, null, null);
		}
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		try
		{
			final Process p = Runtime.getRuntime().exec(mCommand);
			mWatcherThread.mProcessQueue.put(new ObjectWithProcess(p, ProcessStatus.STARTED));
			final BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream(), StandardCharsets.UTF_8));
			String l;
			try
			{
				while ((l = r.readLine()) != null)
				{
					if (l.startsWith(MeasurementGetterMeross.LINE_START))
					{
						final String plugNumber = l.substring(LINE_START.length(), l.indexOf(' ', LINE_START.length()));
						if (mSupportedCardNumbers.contains(plugNumber))
						{
							final int start = l.indexOf(MeasurementGetterMeross.POWER_START) + POWER_START.length();
							final int end = l.indexOf(" W", start);
							if (end > 0)
							{
								final String powerStr = l.substring(start, end);
								final double power = Double.parseDouble(powerStr);
								res.put(SENSOR_TYPE_SHORT + "apow" + plugNumber, new Measurement(SENSOR_TYPE_SHORT + "apow" + plugNumber, power));
							}
						}
					}
				}
			}
			finally
			{
				r.close();
			}
			mWatcherThread.mProcessQueue.put(new ObjectWithProcess(p, ProcessStatus.ENDED));
		}
		catch (IOException | InterruptedException e)
		{
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public synchronized void disconnect() throws GetterException
	{
		final ProcessWatcherThread w = mWatcherThread;
		if (w != null)
		{
			try
			{
				w.mProcessQueue.put(new ObjectWithProcess(null, ProcessStatus.QUIT));
				mWatcherThread = null;
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void reconnect() throws GetterException
	{
		// Nothing to do here
	}

	@Override
	public Integer getDefaultTemporization()
	{
		return 2*60;
	}
}
