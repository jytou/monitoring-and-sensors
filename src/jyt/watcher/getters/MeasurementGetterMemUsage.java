package jyt.watcher.getters;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.eclipsesource.json.JsonObject;

import jyt.watcher.GetterException;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;
import jyt.watcher.StringUtil;
import jyt.watcher.Watched;

public class MeasurementGetterMemUsage extends MeasurementGetterAbstract
{
	private static final String RAM_SHORT = "r";
	private static final String SWAP_SHORT = "s";

	public MeasurementGetterMemUsage(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "Used Memory", "mm");
		final JsonObject memObject = pConfig.asObject();
		final double boundMinRam = memObject.getDouble("min.r", -100.0);
		final double boundMaxRam = memObject.getDouble("max.r", 95.0);
		final double boundMinWarnRam = memObject.getDouble("minWarn.r", -100.0);
		final double boundMaxWarnRam = memObject.getDouble("maxWarn.r", 85.0);
		final double boundMinSwap = memObject.getDouble("min.s", -100.0);
		final double boundMaxSwap = memObject.getDouble("max.s", 80.0);
		final double boundMinWarnSwap = memObject.getDouble("minWarn.s", -100.0);
		final double boundMaxWarnSwap = memObject.getDouble("maxWarn.s", 60.0);

		Watched.registerWatch(mShortName + RAM_SHORT, mLongName + " RAM", mLongName + " RAM", mLongHost, mShortHost, mTypeLong, mTypeShort + RAM_SHORT, mLongHost + " Mem", null, mIntervalWarn, boundMinRam, boundMaxRam, boundMinWarnRam, boundMaxWarnRam, null, null);
		Watched.registerWatch(mShortName + SWAP_SHORT, mLongName + " RAM+SWAP", mLongName + " RAM+SWAP", mLongHost, mShortHost, mTypeLong, mTypeShort + SWAP_SHORT, mLongHost + " MemSwap", null, mIntervalWarn, boundMinSwap, boundMaxSwap, boundMinWarnSwap, boundMaxWarnSwap, null, null);
	}

	@Override
	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		BufferedReader r = null;
		try
		{
			r = new BufferedReader(new InputStreamReader(new FileInputStream("/proc/meminfo")));
			String l;
			long swapFree = 0;
			long swapTotal = 0;
			long memTotal = 0;
			long memFree = 0;
			long cacheTotal = 0;
			while ((l = r.readLine()) != null)
			{
				final String[] s = StringUtil.split(l, " ");
				if (l.startsWith("MemTotal"))
					memTotal = Long.parseLong(s[1]);
				else if (l.startsWith("MemFree"))
					memFree = Long.parseLong(s[1]);
				else if (l.startsWith("Cached"))
					cacheTotal = Long.parseLong(s[1]);
				else if (l.startsWith("SwapTotal"))
					swapTotal = Long.parseLong(s[1]);
				else if (l.startsWith("SwapFree"))
					swapFree = Long.parseLong(s[1]);
			}
			// Because cache is not really used memory
			memFree += cacheTotal;
			res.put(mShortName + RAM_SHORT, new Measurement(mShortName + RAM_SHORT, 100.0 - 100.0 * memFree / memTotal));
			if (swapTotal > 0)
			{
				memTotal += swapTotal;
				memFree += swapFree;
			}
			res.put(mShortName + SWAP_SHORT, new Measurement(mShortName + SWAP_SHORT, 100.0 - 100.0 * memFree / memTotal));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (r != null)
				try
				{
					r.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
		}
		return res;
	}

	@Override
	public void disconnect() throws GetterException
	{
		// nothing to do here
	}

	@Override
	public void reconnect() throws GetterException
	{
		// nothing to do here
	}
}
