package jyt.watcher.getters;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.eclipsesource.json.JsonObject;
import com.jcraft.jsch.JSchException;

import jyt.watcher.GetterException;
import jyt.watcher.JSchConnector;
import jyt.watcher.Measurement;
import jyt.watcher.MeasurementGetterAbstract;

/**
 * !!!!!!!!!!!!!!!!!!!!
 * WARNING : Currently not functional
 * @author jytou
 */
public class MeasurementGetterPiUSBTempers extends MeasurementGetterAbstract
{
	private Map<Integer, Double> mCurrentTemps = new HashMap<Integer, Double>();
	private long mLastMeasurement = -1;
	private TemperWatcher mWatcher;
	private Throwable mInError = null;
	private JSchConnector mConnector = null;

	private class TemperWatcher extends Thread
	{
		@Override
		public void run()
		{
			super.run();
			String l;
			try
			{
				while ((l = mConnector.readLine()) != null)
				{
					if (l.startsWith("There is no screen"))
					{
						mInError = new GetterException("No such screen");
						break;
					}
					else
						synchronized (mCurrentTemps)
						{
							String[] array = l.split(" ");
							mCurrentTemps.clear();
							if (array.length == 25)
								for (int i = 0; i < 5; i++)
									mCurrentTemps.put(i, Double.parseDouble(array[i * 5 + 2]));
							mLastMeasurement = System.currentTimeMillis();
						}
				}
			}
			catch (IOException e)
			{
				mInError = e;
			}
		}
	}

	public MeasurementGetterPiUSBTempers(String pLongHost, String pShortHost, JsonObject pConfig) throws SQLException
	{
		super(pLongHost, pShortHost, pConfig, "USB Tempers", "ut");
		setListenProperties(pConfig, 120, 0.0, 40.0, null, null);
		// TODO Needing to register different sensors
	}

	public Map<String, Measurement> getData() throws GetterException
	{
		Map<String, Measurement> res = new HashMap<String, Measurement>();
		if (mInError != null)
		{
			Throwable error = mInError;
			mInError = null;
			reconnect();
			throw new GetterException(error);
		}
		if (mLastMeasurement != -1)
			if (System.currentTimeMillis() - mLastMeasurement > 120000)
			{
				reconnect();
				throw new GetterException("No value for 2 minutes");
			}
			else
				synchronized (mCurrentTemps)
				{
					for (Integer tempNum : mCurrentTemps.keySet())
					{
						String name = "gbt" + tempNum.intValue();
						res.put(name, new Measurement(name, mCurrentTemps.get(tempNum)));
					}
				}
		return res;
	}

	public void disconnect() throws GetterException
	{
		if (mWatcher != null)
		{
			mWatcher.interrupt();
			mWatcher = null;
		}
		try
		{
			if (mConnector != null)
				mConnector.sendChar((char) 4);
		}
		catch (IOException e)
		{
			throw new GetterException(e);
		}
		finally
		{
			mConnector = null;
		}
	}

	@Override
	public void reconnect() throws GetterException
	{
		disconnect();
		try
		{
			mConnector = new JSchConnector("user", "localhost", "pass");
		}
		catch (JSchException e)
		{
			throw new GetterException(e);
		}
		catch (IOException e)
		{
			throw new GetterException(e);
		}
		try
		{
			mConnector.writeLine("sudo screen -x temp");
		}
		catch (IOException e)
		{
			throw new GetterException(e);
		}
		mWatcher = new TemperWatcher();
		mWatcher.start();
	}
}
