package jyt.watcher;

public interface IMeasurementListener
{
	void found(Measurement pMeasurement);
	void endBatch();
}
