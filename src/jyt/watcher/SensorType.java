package jyt.watcher;

import java.util.UUID;

/**
 * Was part of a (currently) abandonned project.
 * @author jytou
 */
public class SensorType
{
	/*
	CPU_LOAD           (1,  "cl",   "CPU Load",            120,    0,     2),
	HDD_TEMP           (2,  "d",    "HDD Temperature",     120,    0,    50),
	COMP_FAN           (3,  "f",    "Fan",                null,  800,  4000),
	COMP_MEM           (4,  "mm",   "Available Memory",   null,  100,    20),
	PROCESSES          (5,  "pp",   "Processes",          null, null,  null),
	PCI_TEMP           (6,  "tp",   "PCI Temperature",    null,    0,    35),
	CPU_TEMP           (7,  "tc",   "CPU Temperature",    null,    0,    75),
	SYS_TEMP           (8,  "ts",   "System Temperature", null,    0,    45),
	PI3_CPU_TEMP       (9,  "ct",   "CPU Temperature",     120,    0,    75),
	MDADM_AVAIL        (10, "md",   "Mdadm Availability",  120,    1,     0),
	MYSQL_SLAVE_STATUS (11, "msns", "Mysql Slave Status",  120,    1,     0),
	MYSQL_SLAVE_LAG    (12, "msnl", "Mysql Slave Lag",    null,    0,  1800),
	ZB_BATTERY         (13, "b",    "Battery",            3600,  100,     0),
	ZB_HUMIDITY        (14, "h",    "Humidity",           null,   30,    80),
	ZB_TEMPERATURE     (15, "t",    "Temperature",        null,  -15,    40),
	ZB_PRESSURE        (16, "p",    "Pressure",           null,  980,  1020),
	ZB_WATER_LEAK      (17, "w",    "Water Leak",         null,    0,     0);
	*/
	UUID type;
	String shortName;
	String humanReadable;
	Integer defaultInterval;
	Double defaultMin;
	Double defaultMax;
	public SensorType(UUID pType, String pShortName, String pHumanReadable, Integer pDefaultInterval, Double pDefaultMin, Double pDefaultMax)
	{
		type = pType;
		shortName = pShortName;
		humanReadable = pHumanReadable;
		defaultInterval = pDefaultInterval;
		defaultMin = pDefaultMin;
		defaultMax = pDefaultMax;
	}
	public SensorType(UUID pType, String pShortName, String pHumanReadable, Integer pDefaultInterval, int pDefaultMin, int pDefaultMax)
	{
		this(pType, pShortName, pHumanReadable, pDefaultInterval, (double)pDefaultMin, (double)pDefaultMax);
	}
}