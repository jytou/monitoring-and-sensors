package jyt.watcher;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class StringUtil
{
	public static String[] split(String pToSplit, String delimiter)
	{
		List<String> res = new ArrayList<>();
		StringTokenizer st = new StringTokenizer(pToSplit, delimiter, false);
		while (st.hasMoreTokens())
			res.add(st.nextToken());
		return res.toArray(new String[res.size()]);
	}
}
