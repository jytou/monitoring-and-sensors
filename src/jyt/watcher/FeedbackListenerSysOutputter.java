package jyt.watcher;

import java.util.Map;

import jyt.watcher.MeasurementListenerOverheating.Card;
import jyt.watcher.MeasurementListenerOverheating.CardGroup;

public class FeedbackListenerSysOutputter implements IFeedbackListener
{
	MeasurementListenerOverheating mOverheatingListener = null;
	private boolean mMeasurementSavedOk = true;
	private int mUnsavedSize;
	private boolean mPreferencesOk = true;

	@Override
	public void setOverheatingListener(MeasurementListenerOverheating pOverheatingListener)
	{
		mOverheatingListener = pOverheatingListener;
	}

	@Override
	public void found(Measurement pMeasurement)
	{
		System.out.println(pMeasurement);
	}

	@Override
	public void endBatch()
	{
		Map<String, Double> preferences = Preferences.getPreferences(this).getValues();
		System.out.print("Preferences: ");
		boolean first = true;
		for (String prefname : preferences.keySet())
		{
			if (first)
				first = false;
			else
				System.out.print(", ");
			System.out.print(prefname + "=" + preferences.get(prefname).doubleValue());
		}
		System.out.println();

		if (mOverheatingListener != null)
		{
			CardGroup[] groups = mOverheatingListener.getCardGroups();
			for (CardGroup group : groups)
				System.out.println("Group " + group.cards.toString() + " temp " + group.meanTemp + " with " + group.nbEnabled + " enabled cards");
		}

		System.out.println("Queue size: " + mUnsavedSize);
		System.out.println("---------------------------");
	}

	@Override
	public void measurementSavedOk(int pUnsavedSize)
	{
		if (!mMeasurementSavedOk)
			System.out.println("************ DB IS BACK, YAY!!! ***************");
		mMeasurementSavedOk = true;
		mUnsavedSize = pUnsavedSize;
	}

	@Override
	public void measurementSavedError(int pUnsavedSize, String pMessage, Throwable pThrowable)
	{
		handleMessageAndError(pMessage, pThrowable);
		mMeasurementSavedOk = false;
		mUnsavedSize = pUnsavedSize;
	}

	public void handleMessageAndError(String pMessage, Throwable pThrowable)
	{
		System.err.println(pMessage + " Exception " + pThrowable.getClass().getSimpleName() + " said " + pThrowable.getLocalizedMessage());
	}

	@Override
	public void stateSaveSuccessful()
	{
		System.out.println("State saved successfully");
	}

	@Override
	public void stateSaveError(String pError, Throwable pThrowable)
	{
		handleMessageAndError(pError, pThrowable);
	}

	@Override
	public void enablingCard(int pCard)
	{
		System.err.println("Enabling card " + pCard);
	}

	@Override
	public void disablingCard(int pCard)
	{
		System.err.println("Disabling card " + pCard);
	}

	@Override
	public void ignoringCard(int pCard)
	{
		System.err.println("Ignoring card " + pCard);
	}

	@Override
	public void changeCardStateError(Card pCard, String pMessage, Throwable pThrowable)
	{
		handleMessageAndError(pMessage + "(card " + pCard.toString() + ")", pThrowable);
	}

	@Override
	public void preferencesReadOk()
	{
		if (!mPreferencesOk)
			System.err.println("DB is back!");
		mPreferencesOk = true;
	}

	@Override
	public void preferencesReadError(String pMessage, Throwable pThrowable)
	{
		handleMessageAndError("Error while reading preferences: " + pMessage, pThrowable);
	}

	@Override
	public void readCGMinerError(String pMessage, Throwable pThrowable)
	{
		handleMessageAndError(pMessage, pThrowable);
	}

	@Override
	public void readCGMinerOK()
	{
	}

	@Override
	public void readGPUInfoOk(int pCardNb)
	{		
	}

	@Override
	public void readGPUInfoError(int pCardNb, String pMessage, Throwable pThrowable)
	{
		handleMessageAndError("Card " + pCardNb + " error: " + pMessage, pThrowable);
	}

	@Override
	public void mirrorStateOK()
	{
	}

	@Override
	public void mirrorStateDegraded(String pDegraded)
	{
		System.err.println("Mirror state degraded: " + pDegraded);
	}
}
