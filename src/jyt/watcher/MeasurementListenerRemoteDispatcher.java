package jyt.watcher;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.time.LocalDateTime;

import jyt.watcher.rt.WatcherRTServer;
import jyt.watcher.rt.WatcherRTServer.ImageData;

public class MeasurementListenerRemoteDispatcher implements IMeasurementListener
{
	private String mCurrentMachineName;
	private Socket mSocket;
	private ObjectOutputStream mOOS;
	private boolean mCam;

	public MeasurementListenerRemoteDispatcher(String pCurrentMachineName, String pRtHost, int pPort, boolean pCam)
	{
		super();
		mCam = pCam;
		mCurrentMachineName = pCurrentMachineName;
		new Thread("RemoteDispatcher")
		{
			public void run()
			{
				int wait = 1000;
				boolean connected = false;
				while (true)
				{
					synchronized (MeasurementListenerRemoteDispatcher.this)
					{
						if (mSocket == null)
							try
							{
								mSocket = new Socket(pRtHost, pPort);
								mOOS = new ObjectOutputStream(mSocket.getOutputStream());
								mOOS.writeObject(mCurrentMachineName);
								connected = true;
								wait = 1000;
								System.out.println("Connected to RT server");
							}
							catch (IOException e)
							{
//								e.printStackTrace();
								mSocket = null;
								if (connected)
									System.err.println("Lost connection with RT server");
								connected = false;
								wait *= 1.5;
								if (wait > 120000)
									wait = 120000;
								System.err.println("Current wait to reconnect to RT server: " + (wait/1000) + " seconds");
							}
					}
					try
					{
						Thread.sleep(wait);
					}
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			}
		}.start();
		if (pCam)
		{
			new Thread("Camera Capture")
			{
				public void run()
				{
					final byte[] buf = new byte[500000];
					while (true)
					{
						try
						{
							final Process p = Runtime.getRuntime().exec("sudo raspistill -w 800 -h 600 -n -t 1000 -q 10 -e jpg -th none -o -");
							try
							{
								final InputStream is = p.getInputStream();
								int off = 0;
								int n;
								do
								{
									while ((n = is.read(buf, off, buf.length - off)) != -1)
										off += n;
								}
								while (p.isAlive());
								while ((n = is.read(buf, off, buf.length - off)) != -1)
									off += n;
								final int nb = off;
								final WatcherRTServer.ImageData imageData = new ImageData();
								imageData.tstamp = LocalDateTime.now();
								imageData.imageData = new byte[nb];
								System.arraycopy(buf, 0, imageData.imageData, 0, nb);
//								System.out.println("Sending " + nb + " bytes...");
								synchronized (MeasurementListenerRemoteDispatcher.this)
								{
									if (mSocket != null)
										try
										{
											mOOS.reset();
											mOOS.writeObject(imageData);
											mOOS.flush();
										}
										catch (Exception e)
										{
											e.printStackTrace();
											mSocket = null;
										}
								}
//								System.out.println("done");
							}
							finally
							{
								p.destroy();
							}
						}
						catch (IOException e)
						{
							e.printStackTrace();
						}
						try
						{
							Thread.sleep(1000);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
				}
			}.start();
		}
	}

	@Override
	public void found(Measurement pMeasurement)
	{
		synchronized (this)
		{
			if (mSocket != null)
				try
				{
					mOOS.reset();
					mOOS.writeObject(pMeasurement);
					mOOS.flush();
				}
				catch (IOException e)
				{
					e.printStackTrace();
					mSocket = null;
				}
		}
	}

	@Override
	public void endBatch()
	{
		// Nothing to do here
	}

}
