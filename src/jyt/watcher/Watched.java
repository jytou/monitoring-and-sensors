package jyt.watcher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Watched
{
	public static final int STATE_ENABLED = 0;
	public static final int STATE_DISABLED = 1;
	public static final int STATE_IGNORED = 2;
	public static final int STATE_SICK = 3;

	private int mId;
	private String mShortName;
	private String mLongName;
	private int mState;
	private Double mCurVal;
	private ZonedDateTime mLastUpdated;
	private Double mMin;
	private Double mMax;
	private Double mWarnMin;
	private Double mWarnMax;
	private Integer mIntervalWarn;// in minutes
	private String mSensorLocation;
	private String mSensorLocShort;
	private String mSensorType;
	private String mSensTypeShort;
	private String mInstanceShort;
	private String mInstanceLong;
	private Double mFactor1;
	private Double mOffset2;

	private static final Map<String, Watched> sWatchedByShortName = new HashMap<String, Watched>();
	private static final Map<Integer, Watched> sWatchedById = new HashMap<Integer, Watched>();
	public static boolean sTestMode = false;
	private static AtomicInteger sInternalIds = new AtomicInteger(0);// for watches that are only in memory in test mode (hack to have watches in memory for the RT watcher)

	public static void init()
	{
		try
		{
			synchronized (sWatchedById)
			{
				sWatchedById.clear();
				sWatchedByShortName.clear();
				try (final Connection c = JDBCConnector.connect())
				{
					try (final Statement s = c.createStatement())
					{
						try (final ResultSet rs = s.executeQuery("select * from watch"))
						{
							while (rs.next())
							{
								final String shortName = rs.getString("shortname");
								final int id = rs.getInt("id");
								final int state = rs.getInt("disabled");
								final Watched w = new Watched(id, shortName, rs.getString("longname"), state);
								w.mCurVal = rs.getDouble("curval");
								if (rs.wasNull())
									w.mCurVal = null;
								final Timestamp lastUpdated = rs.getTimestamp("last_updated");
								w.mLastUpdated = lastUpdated == null ? null : lastUpdated.toLocalDateTime().atZone(ZoneId.systemDefault());
								if (rs.wasNull())
									w.mLastUpdated = null;
								w.mMin = rs.getDouble("bound_min");
								if (rs.wasNull())
									w.mMin = null;
								w.mMax = rs.getDouble("bound_max");
								if (rs.wasNull())
									w.mMax = null;
								w.mWarnMin = rs.getDouble("warn_min");
								if (rs.wasNull())
									w.mWarnMin = null;
								w.mWarnMax = rs.getDouble("warn_max");
								if (rs.wasNull())
									w.mWarnMax = null;
								w.mSensorLocation = rs.getString("loc_long");
								if (rs.wasNull())
									w.mSensorLocation = null;
								w.mSensorLocShort = rs.getString("loc_short");
								if (rs.wasNull())
									w.mSensorLocShort = null;
								w.mSensorType = rs.getString("sensor_type");
								if (rs.wasNull())
									w.mSensorType = null;
								w.mSensTypeShort = rs.getString("st");
								if (rs.wasNull())
									w.mSensTypeShort = null;
								w.mInstanceShort = rs.getString("instance_short");
								if (rs.wasNull())
									w.mInstanceShort = null;
								w.mInstanceLong = rs.getString("instance_long");
								if (rs.wasNull())
									w.mInstanceLong = null;
								w.mIntervalWarn = rs.getInt("interval_warn");
								if (rs.wasNull())
									w.mIntervalWarn = null;
								w.mFactor1 = rs.getDouble("factor1");
								if (rs.wasNull())
									w.mFactor1 = null;
								w.mOffset2 = rs.getDouble("offset2");
								if (rs.wasNull())
									w.mOffset2 = null;
								sWatchedByShortName.put(shortName, w);
								sWatchedById.put(id, w);
							}
						}
					}
				}
			}
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private Watched(int pId, String pShortName, String pLongName, int pState)
	{
		super();
		mId = pId;
		mShortName = pShortName;
		mLongName = pLongName;
		mState = pState;
	}

	public int getId()
	{
		return mId;
	}

	public String getShortName()
	{
		return mShortName;
	}

	public String getLongName()
	{
		return mLongName;
	}

	public int getState()
	{
		return mState;
	}

	public void setState(int pState)
	{
		mState = pState;
	}

	public static Watched registerWatch(String pShortName, String pLongName, String pSensorName, String pSensorLocation, String pSensorLocShort, String pSensorType, String pSenTypeShort, String pInstanceLong, String pInstanceShort, Integer pIntervalWarn, Double pBoundMin, Double pBoundMax, Double pWarnMin, Double pWarnMax, Double pFactor1, Double pOffset2) throws SQLException
	{
		if (pSensorLocation == null)
			throw new RuntimeException("Location cannot be null");
		if (pSensorType == null)
			throw new RuntimeException("Type cannot be null");
		if (!sWatchedByShortName.containsKey(pShortName))
		{
			System.err.println("Inserting " + pShortName + " / " + pLongName + " / " + pSensorName + " / " + pSensorLocation + " / " + pSensorLocShort + " / " + pSensorType + " / " + pSenTypeShort + " / " + pInstanceLong + " / " +  pInstanceShort);
			if (!sTestMode)
			{
				try
				{
					try (final Connection c = JDBCConnector.connect())
					{
						try (final PreparedStatement s = c.prepareStatement("insert into watch(loc_long, loc_short, sensor_type, st, instance_long, instance_short, shortname, longname, sensorname, interval_warn, bound_min, bound_max, warn_min, warn_max, factor1, offset2) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS))
						{
							int param = 1;
							s.setString(param++, pSensorLocation);
							s.setString(param++, pSensorLocShort);
							s.setString(param++, pSensorType);
							s.setString(param++, pSenTypeShort);
							s.setString(param++, pInstanceLong);
							s.setString(param++, pInstanceShort);
							s.setString(param++, pShortName);
							s.setString(param++, pLongName);
							s.setString(param++, pSensorName);
							if (pIntervalWarn == null) s.setNull(param++, Types.INTEGER); else s.setInt   (param++, pIntervalWarn);
							if (pBoundMin     == null) s.setNull(param++, Types.DOUBLE ); else s.setDouble(param++, pBoundMin);
							if (pBoundMax     == null) s.setNull(param++, Types.DOUBLE ); else s.setDouble(param++, pBoundMax);
							if (pWarnMin      == null) s.setNull(param++, Types.DOUBLE ); else s.setDouble(param++, pWarnMin);
							if (pWarnMax      == null) s.setNull(param++, Types.DOUBLE ); else s.setDouble(param++, pWarnMax);
							if (pFactor1      == null) s.setNull(param++, Types.DOUBLE ); else s.setDouble(param++, pFactor1);
							if (pOffset2      == null) s.setNull(param++, Types.DOUBLE ); else s.setDouble(param++, pOffset2);
							s.execute();
							try (final ResultSet rsGenerated = s.getGeneratedKeys())
							{
								rsGenerated.next();
								final int id = rsGenerated.getInt(1);
								createWatch(pShortName, pLongName, pSensorLocation, pSensorLocShort, pSensorType, pSenTypeShort, pInstanceLong, pInstanceShort, pIntervalWarn, pBoundMin, pBoundMax, pWarnMin, pWarnMax, pFactor1, pOffset2, id);
							}
						}
						if (!c.getAutoCommit())
							c.commit();
					}
				}
				catch (ClassNotFoundException e)
				{
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
			else
			{
				createWatch(pShortName, pLongName, pSensorLocation, pSensorLocShort, pSensorType, pSenTypeShort, pInstanceLong, pInstanceShort, pIntervalWarn, pBoundMin, pBoundMax, pWarnMin, pWarnMax, pFactor1, pOffset2, sInternalIds.decrementAndGet());
			}
		}
		else if (!sWatchedByShortName.get(pShortName).mLongName.equals(pLongName))
			throw new RuntimeException(pLongName + " conflicts with " + sWatchedByShortName.get(pShortName).mLongName);
		else
		{
			// make sure we're not adding some info for the transitional period
			final Watched watch = sWatchedByShortName.get(pShortName);
			boolean needsSave = false;
			if (differNull(watch.mIntervalWarn, pIntervalWarn))
			{
				watch.mIntervalWarn = pIntervalWarn;
				System.out.println("pIntervalWarn " + pIntervalWarn);
				needsSave = true;
			}
			if (differNull(watch.mMin, pBoundMin))
			{
				watch.mMin = pBoundMin;
				System.out.println("pBoundMin " + pBoundMin);
				needsSave = true;
			}
			if (differNull(watch.mMax, pBoundMax))
			{
				watch.mMax = pBoundMax;
				System.out.println("pBoundMax " + pBoundMax);
				needsSave = true;
			}
			if (differNull(watch.mWarnMin, pWarnMin))
			{
				watch.mWarnMin = pWarnMin;
				System.out.println("pWarnMin " + pWarnMin);
				needsSave = true;
			}
			if (differNull(watch.mWarnMax, pWarnMax))
			{
				watch.mWarnMax = pWarnMax;
				System.out.println("pWarnMax " + pWarnMax);
				needsSave = true;
			}
			if (differNull(watch.mFactor1, pFactor1))
			{
				watch.mFactor1 = pFactor1;
				System.out.println("pFactor " + pFactor1);
				needsSave = true;
			}
			if (differNull(watch.mOffset2, pOffset2))
			{
				watch.mOffset2 = pOffset2;
				System.out.println("pOffset " + pOffset2);
				needsSave = true;
			}
			if (differNull(watch.mSensorLocation, pSensorLocation))
			{
				watch.mSensorLocation = pSensorLocation;
				System.out.println("pSensorLocation " + pSensorLocation);
				needsSave = true;
			}
			if (differNull(watch.mSensorLocShort, pSensorLocShort))
			{
				watch.mSensorLocShort = pSensorLocShort;
				System.out.println("pSensorLocShort " + pSensorLocShort);
				needsSave = true;
			}
			if (differNull(watch.mSensorType, pSensorType))
			{
				watch.mSensorType = pSensorType;
				System.out.println("pSensorType " + pSensorType);
				needsSave = true;
			}
			if (differNull(watch.mSensTypeShort, pSenTypeShort))
			{
				watch.mSensTypeShort = pSenTypeShort;
				System.out.println("pSenTypeShort " + pSenTypeShort);
				needsSave = true;
			}
			if (differNull(watch.mInstanceLong, pInstanceLong))
			{
				watch.mInstanceLong = pInstanceLong;
				System.out.println("pInstanceLong " + pInstanceLong);
				needsSave = true;
			}
			if (differNull(watch.mInstanceShort, pInstanceShort))
			{
				watch.mInstanceShort = pInstanceShort;
				System.out.println("pInstanceShort " + pInstanceShort);
				needsSave = true;
			}
			if (needsSave)
			{
				System.err.println("Updating " + pShortName + " / " + pLongName + " / " + pSensorName + " / " + pSensorLocation + " / " + pSensorLocShort + " / " + pSensorType + " / " + pSenTypeShort + " / " + pInstanceLong + " / " +  pInstanceShort);
				if (!sTestMode)
					try
					{
						try (final Connection c = JDBCConnector.connect())
						{
							try (final PreparedStatement s = c.prepareStatement("update watch set loc_long=?, loc_short=?, sensor_type=?, st=?, instance_long=?, instance_short=?, shortname=?, longname=?, sensorname=?, interval_warn=?, bound_min=?, bound_max=?, warn_min=?, warn_max=?, factor1=?, offset2=? where id=?"))
							{
								int param = 1;
								s.setString(param++, pSensorLocation);
								s.setString(param++, pSensorLocShort);
								s.setString(param++, pSensorType);
								s.setString(param++, pSenTypeShort);
								s.setString(param++, pInstanceLong);
								s.setString(param++, pInstanceShort);
								s.setString(param++, pShortName);
								s.setString(param++, pLongName);
								s.setString(param++, pSensorName);
								if (pIntervalWarn == null) s.setNull(param++, Types.INTEGER); else s.setInt   (param++, pIntervalWarn);
								if (pBoundMin     == null) s.setNull(param++, Types.DOUBLE ); else s.setDouble(param++, pBoundMin);
								if (pBoundMax     == null) s.setNull(param++, Types.DOUBLE ); else s.setDouble(param++, pBoundMax);
								if (pWarnMin      == null) s.setNull(param++, Types.DOUBLE ); else s.setDouble(param++, pWarnMin);
								if (pWarnMax      == null) s.setNull(param++, Types.DOUBLE ); else s.setDouble(param++, pWarnMax);
								if (pFactor1      == null) s.setNull(param++, Types.DOUBLE ); else s.setDouble(param++, pFactor1);
								if (pOffset2      == null) s.setNull(param++, Types.DOUBLE ); else s.setDouble(param++, pOffset2);
								s.setInt(param++, watch.mId);
								s.execute();
							}
							if (!c.getAutoCommit())
								c.commit();
						}
					}
					catch (ClassNotFoundException e)
					{
						e.printStackTrace();
						throw new RuntimeException(e);
					}
			}
		}
		return getWatchedByShortName(pShortName);
	}

	private static void createWatch(String pShortName, String pLongName, String pSensorLocation, String pSensorLocShort, String pSensorType, String pSenTypeShort, String pInstanceLong, String pInstanceShort, Integer pIntervalWarn, Double pBoundMin, Double pBoundMax, Double pWarnMin, Double pWarnMax, Double pFactor1, Double pOffset2, final int id)
	{
		Watched w = new Watched(id, pShortName, pLongName, 0);
		w.mSensorLocation = pSensorLocation;
		w.mSensorLocShort = pSensorLocShort;
		w.mSensorType = pSensorType;
		w.mSensTypeShort = pSenTypeShort;
		w.mInstanceLong = pInstanceLong;
		w.mInstanceShort = pInstanceShort;
		if (pIntervalWarn != null) w.mIntervalWarn = pIntervalWarn;
		if (pBoundMin     != null) w.mMin          = pBoundMin;
		if (pBoundMax     != null) w.mMax          = pBoundMax;
		if (pWarnMin      != null) w.mWarnMin      = pWarnMin;
		if (pWarnMax      != null) w.mWarnMax      = pWarnMax;
		if (pFactor1      != null) w.mFactor1      = pFactor1;
		if (pOffset2      != null) w.mOffset2      = pOffset2;
		synchronized (sWatchedById)
		{
			sWatchedByShortName.put(pShortName, w);
			sWatchedById.put(id, w);
		}
	}

	private static <T> boolean differNull(T d1, T d2)
	{
		if (d1 == null)
			return d2 != null;
		else
			return !d1.equals(d2);
	}

	public static Watched getWatchedById(int pId)
	{
		synchronized (sWatchedById)
		{
			if (!sWatchedById.containsKey(pId))
				throw new IllegalArgumentException("No watch found for id " + pId);
			return sWatchedById.get(pId);
		}
	}

	public static boolean existsWatch(String pShortName)
	{
		synchronized (sWatchedById)
		{
			return sWatchedByShortName.containsKey(pShortName);
		}
	}

	public static Watched getWatchedByShortName(String pShortName)
	{
		synchronized (sWatchedById)
		{
			if (!sWatchedByShortName.containsKey(pShortName))
				throw new IllegalArgumentException("No watch found for short name " + pShortName);
			return sWatchedByShortName.get(pShortName);
		}
	}

	public static Collection<Watched> getAllWatched()
	{
		synchronized (sWatchedById)
		{
			return new ArrayList<>(sWatchedById.values());
		}
	}

	public Double getCurVal()
	{
		return mCurVal;
	}
	public ZonedDateTime getLastUpdated()
	{
		return mLastUpdated;
	}

	public Integer getIntervalWarn()
	{
		return mIntervalWarn;
	}
	public void setIntervalWarn(Integer pIntervalWarn)
	{
		mIntervalWarn = pIntervalWarn;
	}
	public Double getMin()
	{
		return mMin;
	}
	public void setMin(Double pMin)
	{
		mMin = pMin;
	}
	public Double getMax()
	{
		return mMax;
	}
	public void setMax(Double pMax)
	{
		mMax = pMax;
	}

	public Double getWarnMin()
	{
		return mWarnMin;
	}

	public void setWarnMin(Double pWarnMin)
	{
		mWarnMin = pWarnMin;
	}

	public Double getWarnMax()
	{
		return mWarnMax;
	}

	public void setWarnMax(Double pWarnMax)
	{
		mWarnMax = pWarnMax;
	}

	public String getSensorLocation()
	{
		return mSensorLocation;
	}

	public String getSensorLocShort()
	{
		return mSensorLocShort;
	}

	public String getSensorType()
	{
		return mSensorType;
	}

	public String getSensTypeShort()
	{
		return mSensTypeShort;
	}

	public String getInstanceShort()
	{
		return mInstanceShort;
	}

	public String getInstanceLong()
	{
		return mInstanceLong;
	}

	public Double getFactor1()
	{
		return mFactor1;
	}

	public Double getOffset2()
	{
		return mOffset2;
	}
}
