package jyt.watcher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

/**
 * Class to save the state of a watched item (disabled or not)
 * @author jytou
 */
public class StateSaver extends Thread
{
	private Map<String, LocalWatch> mToSave = new HashMap<String, LocalWatch>();
	private Semaphore mSaveSem = new Semaphore(0);
	private IFeedbackListener mFeedbackListener;
	private static StateSaver sStateSaver = null;

	public synchronized static StateSaver getStateSaver(IFeedbackListener pFeedbackListener)
	{
		if (sStateSaver == null)
			sStateSaver = new StateSaver(pFeedbackListener);
		return sStateSaver;
	}

	private class LocalWatch
	{
		int id;
		String shortName;
		int state;
		public LocalWatch(Watched pWatched)
		{
			super();
			id = pWatched.getId();
			shortName = pWatched.getShortName();
			state = pWatched.getState();
		}
	}

	private StateSaver(IFeedbackListener pFeedbackListener)
	{
		super();
		mFeedbackListener = pFeedbackListener;
		start();
	}

	public void toSave(Watched pWatched)
	{
		synchronized (mToSave)
		{
			mToSave.put(pWatched.getShortName(), new LocalWatch(pWatched));
		}
		mSaveSem.release();
	}

	@Override
	public void run()
	{
		super.run();
		long sleepTime = 1000;
		while (true)
		{
			try
			{
				mSaveSem.acquire();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			boolean empty = false;
			do
			{
				LocalWatch watched = null;
				synchronized (mToSave)
				{
					if (!mToSave.isEmpty())
						watched = mToSave.values().iterator().next();
				}
				if (watched != null)
				{
					try
					{
						save(watched);
						synchronized (mToSave)
						{
							if (watched == mToSave.get(watched.shortName))
								mToSave.remove(watched.shortName);
							empty = mToSave.isEmpty();
						}
						mFeedbackListener.stateSaveSuccessful();
						sleepTime = 1000;
					}
					catch (ClassNotFoundException e)
					{
						e.printStackTrace();
					}
					catch (SQLException e)
					{
						mFeedbackListener.stateSaveError("Could not save change to card " + watched.shortName + " to the database... will retry in " + (sleepTime / 1000) + " s", e);

						mSaveSem.release();
						try
						{
							Thread.sleep(sleepTime);
						}
						catch (InterruptedException e1)
						{
							e1.printStackTrace();
						}
						sleepTime *= 2;
						if (sleepTime > 60000)
							sleepTime = 60000;
					}
				}
			}
			while (!empty);
		}
	}

	public void save(LocalWatch pWatch) throws ClassNotFoundException, SQLException
	{
		Connection conn = JDBCConnector.connect();
		try
		{
			Statement s = conn.createStatement();
			try
			{
				int number = Integer.valueOf(pWatch.shortName.substring(3));
				s.execute("update watch set disabled=" + pWatch.state + " where shortname='mgt" + number + "' or shortname='mgs" + number + "'");
				PreparedStatement ps = conn.prepareStatement("insert into actions (cur_time, disabled, idwatch) values (?, ?, ?)");
				try
				{
					ps.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
					ps.setInt(2, pWatch.state);
					ps.setInt(3, pWatch.id);
					ps.execute();
				}
				finally
				{
					ps.close();
				}
			}
			finally
			{
				s.close();
			}
		}
		finally
		{
			conn.close();
		}
	}
}
