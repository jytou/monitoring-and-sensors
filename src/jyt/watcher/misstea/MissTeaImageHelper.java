package jyt.watcher.misstea;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import javax.imageio.ImageIO;

import jyt.watcher.util.ConsoleProgress;

public class MissTeaImageHelper
{
	public static BufferedImage createRotatedCropped(final BufferedImage pOriginal)
	{
		final int widthOfImage = 440;
		final int heightOfImage = 240;
		final int typeOfImage = pOriginal.getType();

		final BufferedImage newImageFromBuffer = new BufferedImage(widthOfImage, heightOfImage, typeOfImage);
		final Graphics2D graphics2D = newImageFromBuffer.createGraphics();
		graphics2D.rotate(Math.toRadians(-28), pOriginal.getWidth() / 2, pOriginal.getHeight() / 2);
		graphics2D.translate(-20, -149);
		graphics2D.drawImage(pOriginal, null, 0, 0);

		return newImageFromBuffer;
	}

	public static BufferedImage createDiffImage(BufferedImage pImage0, BufferedImage pImage1)
	{
		final BufferedImage diff = new BufferedImage(pImage1.getWidth(), pImage1.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
		final Raster data1 = pImage0.getData();
		final Raster data2 = pImage1.getData();
		final int[] buf1 = new int[3];
		final int[] buf2 = new int[3];
		final int[] diffPixel = new int[1];
		final WritableRaster diffRaster = diff.getRaster();
		for (int xInImage = 0; xInImage < diff.getWidth(); xInImage++)
		{
			for (int yInImage = 0; yInImage < diff.getHeight(); yInImage++)
			{
				data1.getPixel(xInImage, yInImage, buf1);
				data2.getPixel(xInImage, yInImage, buf2);
				diffPixel[0] = Math.abs((buf1[0] + buf1[1] + buf1[2] - buf2[0] - buf2[1] - buf2[2]) / 3);
				diffRaster.setPixel(xInImage, yInImage, diffPixel);
			}
		}
		return diff;
	}

	public static double[] computeTotalDiff(BufferedImage mImageDiff)
	{
		final Raster imageRaster = mImageDiff.getData();
		final int[] tempBuf = new int[1];
		double totalDiff = 0;
		final int imageWidth = imageRaster.getWidth();
		final int imageHeight = imageRaster.getHeight();
		for (int xInImage = 0; xInImage < imageWidth; xInImage++)
			for (int yInImage = 0; yInImage < imageHeight; yInImage++)
			{
				imageRaster.getPixel(xInImage, yInImage, tempBuf);
				totalDiff += tempBuf[0];
			}
		final double mean = 1.0 * totalDiff / (imageWidth * imageHeight);
		double totalStandardDev = 0.0;
		for (int xInImage = 0; xInImage < imageWidth; xInImage++)
			for (int yInImage = 0; yInImage < imageHeight; yInImage++)
			{
				imageRaster.getPixel(xInImage, yInImage, tempBuf);
				double diff = mean - tempBuf[0];
				totalStandardDev += diff * diff;
			}
		totalStandardDev /= imageWidth * imageHeight;
		return new double[] {totalDiff, totalStandardDev};
	}

	public static List<File> findFiles(File pRootDir)
	{
		final List<File> res = new ArrayList<>();
		final File[] files = pRootDir.listFiles();
		Arrays.sort(files, new Comparator<File>()
		{
			@Override
			public int compare(File f1, File f2)
			{
				if (f1.isDirectory())
					if (f2.isDirectory())
						return f1.getName().compareTo(f2.getName());
					else
						return -1;
				else
					if (f2.isDirectory())
						return 1;
					else
						return f1.getName().compareTo(f2.getName());
			}
		});
		for (File file : files)
		{
			if (file.isDirectory() && (!file.getName().startsWith(".")) && (!file.getName().contains("misstea")))
				res.addAll(findFiles(file));
			if ((!file.isDirectory()) && file.getName().endsWith("jpg"))
				res.add(file);
		}
		return res;
	}


	public static int[] histogram(final BufferedImage img)
	{
		final int[] hist = new int[256];
//		Arrays.fill(hist, 0);
		for (int x = 0; x < img.getWidth(); x++)
			for (int y = 0; y < img.getHeight(); y++)
				hist[grey(img.getRGB(x, y))]++;
		return hist;
	}

	public static BufferedImage createHistogramImage(final BufferedImage img)
	{
		final int height = 400;
		final BufferedImage histImg = new BufferedImage(256, height, BufferedImage.TYPE_BYTE_GRAY);
		final int[] hist = histogram(img);
		int max = 0;
		for (int h : hist)
			if (h > max)
				max = h;
		final Graphics2D g = histImg.createGraphics();
		g.setColor(Color.black);
		g.fillRect(0, 0, 256, height);
		g.setColor(Color.white);
		for (int i = 0; i < 256; i++)
			g.drawLine(i, height, i, height - (hist[i] * height / max));
		return histImg;
	}

	private static int grey(int c)
	{
		int r = (c>>16)&0xFF;
		int g = (c>>8)&0xFF;
		int b = (c>>0)&0xFF;
//		return (r+g+b) / 3;
		return Math.max(r, Math.max(g, b));
	}

	public static BufferedImage createHistogramAdjusted(final BufferedImage img)
	{
		final int[] hist = histogram(img);
		int smin = 0;
		for (int i = 0; i < hist.length; i++)
		{
			if (hist[i] > 350)
			{
				smin = i;
				break;
			}
		}
		int smax = hist.length - 1;
		for (int i = hist.length - 1; i >= 0; i--)
		{
			if (hist[i] > 350)
			{
				smax = i;
				break;
			}
		}
		final int dmin = 10;
		final int dmax = 220;
		final int w = img.getWidth();
		final int h = img.getHeight();
		final BufferedImage adjusted = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		final Raster src = img.getData();
		final WritableRaster dest = adjusted.getRaster();
		final int[] pixels = src.getPixels(0, 0, w, h, (int[])null);
		for (int i = 0; i < pixels.length; i++)
			pixels[i] = Math.min(255, Math.max(0, ((pixels[i] - smin) * (dmax - dmin) / (smax - smin)) + dmin));
		dest.setPixels(0, 0, w, h, pixels);
		return adjusted;
	}

	public static double[] computeColorBalance(final BufferedImage img)
	{
		long totalR = 0;
		long totalG = 0;
		long totalB = 0;
		final int[] pixels = img.getRaster().getPixels(0, 0, img.getWidth(), img.getHeight(), (int[])null);
		for (int i = 0; i < pixels.length / 3; i++)
		{
			totalR += pixels[i * 3];
			totalG += pixels[i * 3 + 1];
			totalB += pixels[i * 3 + 2];
		}
		long total = totalR + totalG + totalB;
		return new double[] {1.0 * totalR / total, 1.0 * totalG / total, 1.0 * totalB / total};
	}

	public static long computeLuminosity(final BufferedImage img)
	{
		long total = 0;
		final int[] pixels = img.getRaster().getPixels(0, 0, img.getWidth(), img.getHeight(), (int[])null);
		for (int i = 0; i < pixels.length / 3; i++)
			total += Math.max(pixels[i * 3], Math.max(pixels[i * 3 + 1], pixels[i * 3 + 2]));
		return total;
	}

	public static void main(String[] args) throws IOException
	{
		final String img1 = "20220207181153";
		final String img2 = "20220207181156";
		final BufferedImage i1 = createRotatedCropped(ImageIO.read(new File("/home/jytou/bignas/homes/check3d/laundry/" + img1.substring(0, 8) + "/" + img1.substring(8, 10) + "/" + img1 + ".jpg")));
		final BufferedImage i2 = createRotatedCropped(ImageIO.read(new File("/home/jytou/bignas/homes/check3d/laundry/" + img2.substring(0, 8) + "/" + img2.substring(8, 10) + "/" + img2 + ".jpg")));
		final BufferedImage diff = createDiffImage(i1, i2);
		final double[] diffs = computeTotalDiff(diff);
		System.out.println("diff: " + diffs[0] + ", dev: " + diffs[1]);

//		final String img = "20220207104616";
//		final BufferedImage i = createRotatedCropped(ImageIO.read(new File("/home/jytou/bignas/homes/check3d/laundry/" + img.substring(0, 8) + "/" + img.substring(8, 10) + "/" + img + ".jpg")));
//		final double[] cb = computeColorBalance(i);
//		System.out.println("Color balance: " + cb[0] + ", " + cb[1] + ", " + cb[2]);
//		System.out.println("Luminosity: " + computeLuminosity(i));

//		final List<File> files = findFiles(new File("/home/jytou/bignas/homes/check3d/laundry"));
//		System.out.println("Found " + files.size() + " files");
//		final ConsoleProgress progress = new ConsoleProgress(files.size());
//		for (File file : files)
//		{
//			System.out.println("Processing " + file.getName());
//			final BufferedImage image = ImageIO.read(file);
//			if ((image.getWidth() == 640) && (image.getHeight() == 480))
//				ImageIO.write(createRotatedCropped(image), "jpg", file);
//			progress.increment();
//		}
//		progress.done();

//		final List<File> files = findFiles(new File("/home/jytou/bignas/homes/check3d/laundry"));
//		System.out.println("Found " + files.size() + " files");
//		final ConsoleProgress progress = new ConsoleProgress(files.size());
//		BufferedImage firstImage = createRotatedCropped(ImageIO.read(files.get(0)));
//		for (int i = 1; i < files.size(); i++)
//		{
//			BufferedImage secondImage = createRotatedCropped(ImageIO.read(files.get(i)));
//			
//			firstImage = secondImage;
//			progress.increment();
//		}
//		for (File file : files)
//		{
//			System.out.println("Processing " + file.getName());
//			final BufferedImage image = ImageIO.read(file);
//			if ((image.getWidth() == 640) && (image.getHeight() == 480))
//				ImageIO.write(createRotatedCropped(image), "jpg", file);
//			progress.increment();
//		}
//		progress.done();

//		final String img1 = "20220206140001";
//		final String img2 = "20220206140004";
//		final BufferedImage i1 = createHistogramAdjusted(ImageIO.read(new File("/home/jytou/bignas/homes/check3d/laundry/" + img1.substring(0, 8) + "/" + img1.substring(8, 10) + "/" + img1 + ".jpg")));
//		final BufferedImage i2 = createHistogramAdjusted(ImageIO.read(new File("/home/jytou/bignas/homes/check3d/laundry/" + img2.substring(0, 8) + "/" + img2.substring(8, 10) + "/" + img2 + ".jpg")));
//		final double[] diffs = computeTotalDiff(createDiffImage(i1, i2));
//		System.out.println("diff: " + diffs[0] + ", dev: " + diffs[1]);
//		final double[] colors = computeColorBalance(i1);
//		System.out.println("Colors: " + colors[0] + ", " + colors[1] + ", " + colors[2]);
	}
}
