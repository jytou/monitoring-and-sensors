package jyt.watcher;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import jyt.watcher.rt.WatcherRTServer;

public class Config
{
	private String mLongHost;
	private String mShortHost;
	private IMeasurementGetter[] mGetters;
	private IMeasurementListener[] mListeners;

	public Config(String pFileName, IFeedbackListener pFeedbackListener) throws SQLException, FileNotFoundException, IOException
	{
		final JsonObject conf = Json.parse(new FileReader(pFileName)).asObject();
		mLongHost = conf.get("longHost").asString();
		mShortHost = conf.get("shortHost").asString();
		String myReportedMachineName = mLongHost;
		final JsonArray getters = conf.get("getters").asArray();
		for (JsonValue jsonValue : getters)
		{
			final String type = jsonValue.asObject().get("type").asString();
			if ("DeconzSensors".equals(type))
				myReportedMachineName += "," + WatchMinerEngine.SENSORS_HOME_LONG;
			else if ("Meross".equals(type))
				myReportedMachineName += "," + WatchMinerEngine.POWER_PLUG_MEROSS_LONG;
		}

		final JsonArray listeners = conf.get("listeners").asArray();
		int iListener = 0;
		mListeners = new IMeasurementListener[listeners.size()];
		boolean dbFound = false;
		for (JsonValue jsonValue : listeners)
		{
			final JsonObject currentObject = jsonValue.asObject();
			final String type = currentObject.get("type").asString();
			switch (type)
			{
			case "db":
				if (dbFound)
					throw new RuntimeException("Only one DB is allowed for now");
				dbFound = true;
				JDBCConnector.init(currentObject.get("url").asString(), currentObject.get("user").asString(), currentObject.get("password").asString());
				mListeners[iListener++] = new MeasurementListenerSaver(pFeedbackListener);
				break;
			case "rt":
				mListeners[iListener++] = new MeasurementListenerRemoteDispatcher(myReportedMachineName, currentObject.get("host").asString(), currentObject.getInt("port", WatcherRTServer.PORT), currentObject.getBoolean("cam", false));
				break;

			default:
				throw new RuntimeException("Unknown listener type: " + type);
			}
		}
		if (!dbFound)
			throw new RuntimeException("One DB is required");
		mGetters = new IMeasurementGetter[getters.size()];
		int iGetter = 0;
		Watched.init();
		for (JsonValue jsonValue : getters)
		{
			final JsonObject currentObject = jsonValue.asObject();
			final String type = currentObject.get("type").asString();
			Integer temporization = null;
			try
			{
				@SuppressWarnings("unchecked")
				final Class<? extends IMeasurementGetter> clazz = (Class<? extends IMeasurementGetter>)Class.forName("jyt.watcher.getters.MeasurementGetter" + type);
				final Constructor<? extends IMeasurementGetter> constructor = clazz.getConstructor(String.class, String.class, JsonObject.class);
				final IMeasurementGetter getter = constructor.newInstance(mLongHost, mShortHost, currentObject);
				mGetters[iGetter] = getter;
				temporization = getter.getDefaultTemporization();
				if (currentObject.contains("interval"))
					temporization = (int)currentObject.get("interval").asLong();
				if (temporization != null)
					mGetters[iGetter] = new MeasurementGetterTemporizer(mGetters[iGetter], temporization);
			}
			catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
			{
				e.printStackTrace();
			}
			if (mGetters[iGetter] == null)
			{
				System.err.println("Cannot get for " + currentObject.toString());
				System.exit(1);
			}
			iGetter++;
		}
	}

	public String getLongHost()
	{
		return mLongHost;
	}

	public IMeasurementGetter[] getters()
	{
		return mGetters;
	}

	public IMeasurementListener[] getListeners()
	{
		return mListeners;
	}
}
