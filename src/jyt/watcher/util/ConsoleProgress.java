package jyt.watcher.util;

public class ConsoleProgress
{
	private long mTotal;
	private long mCurrent = 0;
	private int mPercent = 0;

	public ConsoleProgress(long pTotal)
	{
		super();
		mTotal = pTotal;
	}

	public void increment()
	{
		mCurrent++;
		final int newPercent = (int)Math.round(100.0 * mCurrent / mTotal);
		if (mPercent < newPercent)
		{
			System.out.println("" + newPercent + "%");
			mPercent = newPercent;
		}
	}

	public void done()
	{
		System.out.println("Done.");
	}
}
