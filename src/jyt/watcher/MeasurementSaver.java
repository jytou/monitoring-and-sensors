package jyt.watcher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MeasurementSaver
{
	private Connection mConnection = null;
	private PreparedStatement mPSHistory;
	private PreparedStatement mPSLatest;

	public void save(Measurement pMeasurement) throws ClassNotFoundException, SQLException
	{
		if (mConnection == null)
		{
			mConnection = JDBCConnector.connect();
			mPSHistory = mConnection.prepareStatement("insert into measurement (cur_time, id_watch, value) values (?, ?, ?)");
			mPSLatest = mConnection.prepareStatement("update watch set last_updated=?, curval=? where id=?");
		}
		mPSHistory.setTimestamp(1, pMeasurement.getTimestamp());
		mPSHistory.setInt(2, pMeasurement.getIdWatch());
		mPSHistory.setDouble(3, pMeasurement.getValue());
		mPSHistory.execute();
		mPSLatest.setTimestamp(1, pMeasurement.getTimestamp());
		mPSLatest.setDouble(2, pMeasurement.getValue());
		mPSLatest.setInt(3, pMeasurement.getIdWatch());
		mPSLatest.execute();
		if (mPSHistory.getUpdateCount() != 1)
			throw new SQLException("The command didn't throw an exception but didn't insert 1 row");
		if (mPSLatest.getUpdateCount() != 1)
			throw new SQLException("The command didn't throw an exception but didn't update 1 row");
	}

	public void commit() throws SQLException
	{
		if (mPSHistory != null)
			try
			{
				mPSHistory.close();
				mPSLatest.close();
			}
			finally
			{
				mPSHistory = null;
				mPSLatest = null;
				try
				{
					if (mConnection != null)
						mConnection.close();
				}
				finally
				{
					mConnection = null;
				}
			}
	}

	public void disconnect()
	{
	}
}
