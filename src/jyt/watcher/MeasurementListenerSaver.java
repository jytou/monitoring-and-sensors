package jyt.watcher;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedTransferQueue;

public class MeasurementListenerSaver extends Thread implements IMeasurementListener
{
	public static class EndMeasurement extends Measurement
	{
		public EndMeasurement()
		{
			super(-1, null, 0);
		}
	}

	private final static Measurement sEndMeasurement = new EndMeasurement();

	private BlockingQueue<Measurement> mQueue = new LinkedTransferQueue<Measurement>();
	private MeasurementSaver mSaver = new MeasurementSaver();
	private Throwable mThrowable = null;
	private boolean mStatusOK = true;
	private IFeedbackListener mFeedbackListener;

	public MeasurementListenerSaver(IFeedbackListener pFeedbackListener)
	{
		super();
		mFeedbackListener = pFeedbackListener;
		start();
	}

	@Override
	public void found(Measurement pMeasurement)
	{
		try
		{
			mQueue.put(pMeasurement);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void endBatch()
	{
		try
		{
			mQueue.put(sEndMeasurement);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void run()
	{
		super.run();
		while (true)
		{
			try
			{
				final Measurement measurement = mQueue.take();
				if (measurement.getIdWatch() == -1)
					try
					{
						mSaver.commit();
						mThrowable = null;
						mStatusOK = true;
						mFeedbackListener.measurementSavedOk(mQueue.size());
					}
					catch (Throwable e)
					{
						mFeedbackListener.measurementSavedError(mQueue.size(), "Could not commit saving", e);
						mStatusOK = false;
						try
						{
							mSaver.disconnect();
						}
						catch (Exception e1)
						{
							// nothing much we can do here
						}
						mSaver = new MeasurementSaver();
					}
				else
					try
					{
						mSaver.save(measurement);
						mStatusOK = true;
						mThrowable = null;
						mFeedbackListener.measurementSavedOk(mQueue.size());
					}
					catch (Throwable e)
					{
						mStatusOK = false;
						mThrowable = e;
						try
						{
							mSaver.commit();
						}
						catch (Exception e1)
						{
							// likely to fail, be silent for now
						}
						long sleep = 10000;
						while (true)
						{
							mFeedbackListener.measurementSavedError(mQueue.size(), "Error found: sleeping " + (sleep / 1000) + "s...", mThrowable);
							Thread.sleep(sleep);
							mSaver = new MeasurementSaver();
							try
							{
								mSaver.save(measurement);
								mStatusOK = true;
								break;
							}
							catch (Throwable e1)
							{
								mThrowable = e1;
								if (sleep < 120000)
									sleep *= 2;
								mStatusOK = false;
								try
								{
									mSaver.disconnect();
								}
								catch (Exception e2)
								{
								}
							}
						}
					}
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	public boolean isStatusOK()
	{
		return mStatusOK;
	}

	public long getUnsavedSize()
	{
		return mQueue.size();
	}
}
