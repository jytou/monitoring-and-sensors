package jyt.watcher.rt;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.RaspiPin;

public class GPIOTest
{
	public enum State {GREEN, ORANGE, RED};
	public static void main(String[] args) throws InterruptedException
	{
		final GpioController controller = GpioFactory.getInstance();
		final GpioPinDigitalOutput pinGreen = controller.provisionDigitalOutputPin(RaspiPin.GPIO_00);
		final GpioPinDigitalOutput pinOrange = controller.provisionDigitalOutputPin(RaspiPin.GPIO_03);
		final GpioPinDigitalOutput pinRed = controller.provisionDigitalOutputPin(RaspiPin.GPIO_01);
		pinGreen.setMode(PinMode.DIGITAL_OUTPUT);
		State state = State.GREEN;
		boolean on = true;
		GpioPinDigitalOutput pin = pinGreen;
		while (true)
		{
			if (on)
			{
				switch (state)
				{
				case GREEN:
					pin = pinGreen;
					state = State.ORANGE;
					break;
				case ORANGE:
					pin = pinOrange;
					state = State.RED;
					break;
				case RED:
					pin = pinRed;
					state = State.GREEN;
					break;
	
				default:
					break;
				}
			}
			pin.setState(on);
			on = !on;
			Thread.sleep(1000);
		}
	}
}
