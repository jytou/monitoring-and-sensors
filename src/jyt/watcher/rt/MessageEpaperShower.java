package jyt.watcher.rt;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import jyt.watcher.Watched;
import jyt.watcher.rt.WatcherRTServer.WatchErrorMessage;

/**
 * Shows messages on e-Paper device
 */
class MessageEpaperShower extends Thread
{
	private final AtomicInteger mRunningThreads;
	private final SortedMap<String, WatchErrorMessage> mMainErrors;
	private final AtomicBoolean mRoomIsDark;
	private final AtomicBoolean mHumanPresence;
	private final AtomicBoolean mDisabled;
	private final AtomicBoolean mQuitMessageEpaperShower;
	private static final String PYTHON3_PATH = "/usr/bin/python3";
	private static final String SHOWMSG_DIR = "/home/pi/showtext";
	private static final String SHOWMSG_PY = SHOWMSG_DIR + "/showmsg.py";

	private static final String TARGET_EPAPER = "p";
	private static final String TARGET_ADAFRUIT = "a";

	private boolean mEpaperShowing = true;
	private boolean mAdafruitShowing = true;
	private File mShowMsgDir;

	public MessageEpaperShower(AtomicInteger pRunningThreads, SortedMap<String, WatchErrorMessage> pMainErrors, AtomicBoolean pRoomIsDark, AtomicBoolean pQuitMessageEpaperShower, AtomicBoolean pHumanPresence, AtomicBoolean pDisabled)
	{
		super("MessageEpaperShower");
		mRunningThreads = pRunningThreads;
		mMainErrors = pMainErrors;
		mRoomIsDark = pRoomIsDark;
		mQuitMessageEpaperShower = pQuitMessageEpaperShower;
		mHumanPresence = pHumanPresence;
		mDisabled = pDisabled;
		if (new File(SHOWMSG_PY).exists())
			start();
	}
	@Override
	public void run()
	{
		mRunningThreads.incrementAndGet();
		super.run();
		try
		{
			final List<String> lastMessages = new ArrayList<>();
			final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
			mShowMsgDir = new File(SHOWMSG_DIR);
			String lastMinute = LocalDateTime.now().format(dtf);

			while (!mQuitMessageEpaperShower.get())
			{
				final List<String> curMessages = new ArrayList<>();
				if (mHumanPresence.get() && (!mDisabled.get()))
				{
					final SortedMap<String, WatchErrorMessage> mainErrors = new TreeMap<>();
					synchronized (mMainErrors)
					{
						mainErrors.putAll(mMainErrors);
					}
					final Map<String, List<WatchErrorMessage>> groups = new HashMap<>();
					for (Entry<String, WatchErrorMessage> entry : mainErrors.entrySet())
					{
						final String shortWatch = entry.getKey();
						final WatchErrorMessage msg = entry.getValue();
						if (shortWatch.startsWith("sen"))
						{
							final String groupName = shortWatch.substring(0, "senxx".length());
							List<WatchErrorMessage> msgs = groups.get(groupName);
							if (msgs == null)
								groups.put(groupName, msgs = new ArrayList<>());
							msgs.add(msg);
						}
					}
					for (Entry<String, List<WatchErrorMessage>> entry : groups.entrySet())
					{
						final String groupName = entry.getKey();
						final List<WatchErrorMessage> msgs = entry.getValue();
						if (msgs.size() > 1)
						{
							// it's a group
							// remove all watches
							for (WatchErrorMessage msg : msgs)
								mainErrors.remove(msg.watch);
							// add the group with the first element
							mainErrors.put(groupName, msgs.get(0));
						}
					}
					if (mainErrors.size() > 1)
						for (Entry<String, WatchErrorMessage> entry : mainErrors.entrySet())
						{
							final String shortWatch = entry.getKey();
							final WatchErrorMessage msg = entry.getValue();
							curMessages.add(shortWatch);
							curMessages.add(msg.message);
						}
					else if (!mainErrors.isEmpty())
					// only one error here
					{
						for (Entry<String, WatchErrorMessage> entry : mainErrors.entrySet())
						{
							final String alertAssetName = entry.getKey();
							String[] longWatch;
							if (Watched.existsWatch(alertAssetName))
								longWatch = new String[] { Watched.getWatchedByShortName(alertAssetName).getLongName(), entry.getValue().message };
							else if (alertAssetName.startsWith("sen"))
							// it's a group sensor
							{
								String longName = Watched.getWatchedByShortName(entry.getValue().watch).getLongName();
								final int lastSpace = longName.lastIndexOf(' ');
								if (lastSpace > 0)
									longName = longName.substring(0, lastSpace);
								longWatch = new String[] { longName, entry.getValue().message };
							}
							else
								longWatch = new String[] { alertAssetName + ": " + entry.getValue().message };
							final StringTokenizer st = new StringTokenizer(longWatch[0], " ", false);
							final List<String> bigMessages = new ArrayList<>();
							String curBigMessage = "";
							while (st.hasMoreTokens())
							{
								final String tok = st.nextToken();
								if (curBigMessage.isEmpty())
									curBigMessage = tok;
								else if (curBigMessage.length() + tok.length() + 1 > 12)
								{
									bigMessages.add(curBigMessage);
									curBigMessage = tok;
								}
								else
									curBigMessage += " " + tok;
							}
							if (!curBigMessage.isEmpty())
								bigMessages.add(curBigMessage);
							for (String bigMessage : bigMessages)
							{
								curMessages.add(bigMessage);
								curMessages.add("");
							}
							if (longWatch.length > 1)
								curMessages.set(curMessages.size() - 1, longWatch[1]);
						}
					}
					while (curMessages.size() > 12)
						curMessages.remove(curMessages.size() - 1);
					final boolean isCurrentlyDark = mRoomIsDark.get();
					boolean switchDisplay = false;
					if (isCurrentlyDark)
					{
						if (mEpaperShowing)
						{
							new Thread()
							{
								@Override
								public void run()
								{
									super.run();
									showMessages(TARGET_EPAPER, new String[0]);
								}
							}.start();
							mEpaperShowing = false;
							switchDisplay = true;
						}
					}
					else
					// not dark
					{
						if (mAdafruitShowing)
						{
							new Thread()
							{
								@Override
								public void run()
								{
									super.run();
									showMessages(TARGET_ADAFRUIT, new String[0]);
								}
							}.start();
							mAdafruitShowing = false;
							switchDisplay = true;
						}
					}
					if (curMessages.isEmpty())
					{
						final String curMinute = LocalDateTime.now().format(dtf);
						if ((!curMinute.equals(lastMinute)) || switchDisplay)
						{
							showMessages(isCurrentlyDark ? TARGET_ADAFRUIT : TARGET_EPAPER, new String[] {"-ac"});
							if (isCurrentlyDark)
								mAdafruitShowing = true;
							else
								mEpaperShowing = true;
							lastMinute = curMinute;
						}
						lastMessages.clear();
					}
					else
					{
						lastMinute = "";
						if ((!curMessages.equals(lastMessages)) || switchDisplay)
						{
							showMessages(isCurrentlyDark ? TARGET_ADAFRUIT : TARGET_EPAPER, curMessages.toArray(new String[curMessages.size()]));
							if (isCurrentlyDark)
								mAdafruitShowing = true;
							else
								mEpaperShowing = true;
							lastMessages.clear();
							lastMessages.addAll(curMessages);
						}
					}
				}
				else
				// no human around, don't display anything - make sure the displays are off
				{
					if (mAdafruitShowing)
					{
						showMessages(TARGET_ADAFRUIT, new String[] {"-b"});
						mAdafruitShowing = false;
					}
					if (mEpaperShowing)
					{
						showMessages(TARGET_EPAPER, new String[] {"-b"});
						mEpaperShowing = false;
					}
					lastMessages.clear();
				}

				try
				{
					Thread.sleep(500);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
		finally
		{
			showMessages(TARGET_EPAPER, new String[] {"-b"});
			showMessages(TARGET_ADAFRUIT, new String[] {"-b"});
			mRunningThreads.decrementAndGet();
		}
	}
	private void showMessages(String pTarget, final String[] pParams)
	{
		final String[] params = new String[pParams.length + 3];
		params[0] = PYTHON3_PATH;
		params[1] = SHOWMSG_PY;
		params[2] = pTarget;
		System.arraycopy(pParams, 0, params, 3, pParams.length);
		//System.out.println("Sending: " + Arrays.deepToString(params));
		try
		{
			Runtime.getRuntime().exec(params, new String[0], mShowMsgDir).waitFor();
		}
		catch (IOException | InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}