package jyt.watcher.rt;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingWorker;

import jyt.watcher.JDBCConnector;
import jyt.watcher.Measurement;
import jyt.watcher.Watched;
import jyt.watcher.misstea.MissTeaImageHelper;

public class WatcherRTServer extends JFrame
{
	public static final int PORT = 9402;

	private final int ERR_ERROR = 0x02;
	private final int ERR_WARNING = 0x01;

	private AtomicInteger mCurrentState = new AtomicInteger(0);

	private static class HealthState
	{
		double value;
		double health;
		String error;
		String warning;
		public HealthState(double pValue, double pHealth)
		{
			super();
			value = pValue;
			health = pHealth;
		}
		public HealthState(double pValue, String pError, String pWarning)
		{
			super();
			value = pValue;
			error = pError;
			warning = pWarning;
		}
	}

	public enum SuspendType
	{
		FOREVER,
		SNOOZE,
		MUTE,
	}

	static class SuspendedRule
	{
		LocalDateTime end;
		SuspendType type;
		public SuspendedRule(LocalDateTime pEnd, SuspendType pType)
		{
			super();
			end = pEnd;
			type = pType;
		}
	}

	static class WatchErrorMessage
	{
		String watch;
		String message;
		int errType;
		public WatchErrorMessage(String pWatch, String pMessage, int pErrType)
		{
			super();
			watch = pWatch;
			message = pMessage;
			errType = pErrType;
		}
	}

	public static class ImageData implements Serializable
	{
		public LocalDateTime tstamp;
		public byte[] imageData;
	}

	private static final String MISS_TEA_STATE = "MissTea";
	private static final String AGGR_STATE = "Aggr";

	private static final int MAX_DEVIATION_MISS_TEA = 1500;
	private static final int MIN_DEVIATION_MISS_TEA = 100;
	private static final int MIN_DIFF_SURE_MISS_TEA = 300000;
	private static final int MAX_DIFF_SUSPICIOUS_MISS_TEA = 3000000;
	private static final int MIN_DIFF_SUSPICIOUS_MISS_TEA = 250000;

	private static final String GAETANS_ROOMS_DOOR = "Gaetan's Balcony Door";
	private static final String GAETANS_ROOMS_DOOR_ABBR = "grbd";

	// [Watch Id, Measurement]
	private Map<Integer, Measurement> mMeasurements = new HashMap<>();
	// [machine name, [watch short name]]
	private SortedMap<String, SortedSet<String>> mLines = new TreeMap<>();
	// [watch short name, error - empty string if no error]
	private Map<String, HealthState> mStates = new HashMap<>();
	// [machine name] connected machines
	private Set<String> mConnected = new HashSet<>();
	// [machine name or watch short name, error]
	private final SortedMap<String, WatchErrorMessage> mMainErrors = new TreeMap<>();
	// [machine name or watch short name, expiration time]
	private Map<String, SuspendedRule> mSuspendedRules = new HashMap<>();
	private JPopupMenu mPopupMenu;
	private String mCurrentSelectedWatch = null;

	private MyGPIOController mGpioController = null;

	private boolean mAcceptImages;

	class ImageInfo
	{
		String hostName;
		String targetDir;
		int index;
		Object mCurrentImageLock = new Object();
		LocalDateTime mPreviousImageTstamp;
		BufferedImage mPreviousImage;
		BufferedImage mPreviousRaw;
		BufferedImage mCurrentImage;
		BufferedImage mCurrentHist;
		String mImageTstamp;
		double mTotalDiff;
		double mTotalStandardDev;
		double mRedColor;

		public ImageInfo(String pHostName, int pIndex, String pTargetDir)
		{
			super();
			hostName = pHostName;
			index = pIndex;
			targetDir = pTargetDir;
		}
	}

	private Map<String, ImageInfo> mImageInfos = new HashMap<>();
	private static final Color COLOR_ERROR = new Color(150, 0, 0);

	static String[] composeMessageForAlertEntry(Entry<String, WatchErrorMessage> pAlertEntry)
	{
		final String alertAssetName = pAlertEntry.getKey();
		if (Watched.existsWatch(alertAssetName))
			return new String[] { Watched.getWatchedByShortName(alertAssetName).getLongName() + ":", " => " + pAlertEntry.getValue().message };
		else
			return new String[] { alertAssetName + ": " + pAlertEntry.getValue().message };
	}

	private class MyPanel extends JPanel
	{
		private static final int START_ERROR_FONT_SIZE = 64;
		private static final int INTERVAL_X_SQUARES = 1;
		private static final int INTERVAL_Y_SQUARES = 4;
		private static final int MACHINE_NAME_WIDTH = 100;
		private static final int START_X = 5;
		private static final int START_Y = 5;
		private static final int SQUARE_X_SIZE = 7;
		private static final int SQUARE_Y_SIZE = 12;
		private static final int IMAGE_START_Y = 300;

		private int mouseX = 0;
		private int mouseY = 0;

		public MyPanel()
		{
			super();
			addMouseMotionListener(new MouseMotionAdapter()
			{
				@Override
				public void mouseMoved(MouseEvent e)
				{
					super.mouseMoved(e);
					mouseX = e.getX();
					mouseY = e.getY();
					repaint();
				}
			});
			addMouseListener(new MouseAdapter()
			{
				@Override
				public void mouseClicked(MouseEvent e)
				{
					super.mouseClicked(e);
					mCurrentSelectedWatch = findWatchAt(e.getX(), e.getY());
					if (mCurrentSelectedWatch != null)
					{
						if (e.getButton() == MouseEvent.BUTTON1)
						{
							if (mPopupMenu.isShowing())
								mPopupMenu.setVisible(false);
							mPopupMenu.show(MyPanel.this, e.getX(), e.getY());
						}
					}
				}
			});
		}

		@Override
		protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			g.setColor(Color.black);
			g.fillRect(0, 0, getWidth(), getHeight());
			int cury = START_Y;
			int curx = START_X;
			final Map<String, Point> watchPositions = new HashMap<>();
			synchronized (mLines)
			{
				for (Entry<String, SortedSet<String>> lineEntry : mLines.entrySet())
				{
					final String lineName = lineEntry.getKey();
					final SortedSet<String> watches = lineEntry.getValue();
					synchronized (mConnected)
					{
						Color col;
						synchronized (mSuspendedRules)
						{
							if (mConnected.contains(lineName))
								col = Color.green;
							else if (mSuspendedRules.containsKey(lineName))
								col = Color.darkGray;
							else
								col = Color.red;
						}
						g.setColor(col);
					}
					g.drawString(lineName, curx, cury + 8);
					curx += MACHINE_NAME_WIDTH;
					for (String shortName : watches)
					{
						final HealthState state;
						synchronized (mStates)
						{
							state = mStates.get(shortName);
						}
						Color col;
						if (shortName.equals(MISS_TEA_STATE))
						{
							col = Color.gray;
						}
						else if (shortName.equals(GAETANS_ROOMS_DOOR_ABBR))
						{
							col = Color.green;
						}
						else
						{
							final Watched watch = Watched.getWatchedByShortName(shortName);
							synchronized (watch)
							{
								if (watch.getState() == Watched.STATE_DISABLED)
									col = Color.darkGray;
								else
								{
									if (state == null)
										col = Color.pink;
									else if (state.error != null)
										col = Color.red;
									else if (state.warning != null)
										col = Color.orange;
									else if ((watch.getIntervalWarn() == null) || (watch.getMin() == null) || (watch.getMax() == null))
										col = Color.blue;
									else
									{
										final double health = state.health;
										final int red = (int)(255 - health * 150);
										final int green = (int)(100 + health * 155);
										col = new Color(red, green, 100);
									}
								}
							}
						}
						watchPositions.put(shortName, new Point(curx, cury));
						g.setColor(col);
						g.fillRect(curx, cury, SQUARE_X_SIZE, SQUARE_Y_SIZE);
						curx += SQUARE_X_SIZE + INTERVAL_X_SQUARES;
					}
					cury += SQUARE_Y_SIZE + INTERVAL_Y_SQUARES;
					curx = START_X;
				}
				synchronized (mSuspendedRules)
				{
					for (Entry<String, SuspendedRule> suspendedEntry : mSuspendedRules.entrySet())
					{
						final Point point = watchPositions.get(suspendedEntry.getKey());
						if (point != null)
						{
							if (suspendedEntry.getValue().type.equals(SuspendType.FOREVER))
								g.setColor(Color.green);
							else if (suspendedEntry.getValue().type.equals(SuspendType.SNOOZE))
								g.setColor(Color.yellow);
							else if (suspendedEntry.getValue().type.equals(SuspendType.MUTE))
								g.setColor(Color.white);
							g.fillRect(point.x + 1, point.y + 1, SQUARE_X_SIZE - 2, SQUARE_Y_SIZE - 2);
						}
					}
				}
				final int ovalSize = Math.min(getHeight() - cury - 30, getWidth() - 30);
				final int currentState = mCurrentState.get();
				g.setColor(currentState == 0 ? Color.green : currentState == ERR_WARNING ? Color.orange : COLOR_ERROR);
				g.fillOval(getWidth() / 2 - ovalSize / 2, getHeight() - ovalSize - 10, ovalSize, ovalSize);
				final Font font = getFont();
				if (currentState != 0)
					synchronized (mMainErrors)
					{
						if (!mMainErrors.entrySet().isEmpty())
						{
							int bestFontSize = START_ERROR_FONT_SIZE;
							for (Entry<String, WatchErrorMessage> errorEntry : mMainErrors.entrySet())
							{
								final String[] messages = composeMessageForAlertEntry(errorEntry);
								for (String message : messages)
									while (bestFontSize >= 8)
									{
										g.setFont(font.deriveFont(Font.BOLD, bestFontSize));
										final Rectangle2D bounds = g.getFontMetrics().getStringBounds(message, g);
										if (bounds.getWidth() >= getWidth())
											bestFontSize--;
										else
											break;
									}
							}
							for (Entry<String, WatchErrorMessage> errorEntry : mMainErrors.entrySet())
							{
								final String[] messages = composeMessageForAlertEntry(errorEntry);
								for (String message : messages)
								{
									cury += bestFontSize - 3;
									final Rectangle2D bounds = g.getFontMetrics().getStringBounds(message, g);
									g.setColor(Color.white);
									g.drawString(message, (int)(getWidth() / 2 - bounds.getWidth() / 2)-1, cury + 20-1);
									g.setColor(Color.black);
									g.drawString(message, (int)(getWidth() / 2 - bounds.getWidth() / 2)+1, cury + 20+1);
									g.setColor(errorEntry.getValue().errType == ERR_ERROR ? new Color(255, 150, 150) : new Color(255, 200, 150));
									g.drawString(message, (int)(getWidth() / 2 - bounds.getWidth() / 2), cury + 20);
								}
								cury += 15;
							}
						}
					}
				g.setFont(font);

				int x = mouseX;
				int y = mouseY;
				final String watch = findWatchAt(x, y);
				if ((watch != null) && Character.isLowerCase(watch.charAt(0)))
				{
					final String message1 = Watched.getWatchedByShortName(watch).getLongName();
					HealthState state;
					String message2 = "";
					synchronized (mStates)
					{
						state = mStates.get(watch);
					}
					if (state != null)
						if (state.error != null)
							message2 += "ERROR " + state.error;
						else if (state.warning != null)
							message2 += "WARNING " + state.warning;
						else
							message2 += "Healthy: " + String.format("%1$.2f", state.health) + " (" + String.format("%1$.2f", state.value) + ")";
					final Rectangle2D bounds1 = g.getFontMetrics().getStringBounds(message1, g);
					final Rectangle2D bounds2 = g.getFontMetrics().getStringBounds(message2, g);
					final Rectangle2D bounds = new Rectangle((int)Math.max(bounds1.getWidth(), bounds2.getWidth()), (int)(bounds1.getHeight() + 7 + bounds2.getHeight()));
					if (x + bounds.getWidth() > getWidth())
						x = (int)(getWidth() - bounds.getWidth());
					g.setColor(Color.lightGray);
					g.fillRect(x, y, (int)(bounds.getWidth() + 4 + 15), (int)(bounds.getHeight() + 4));
					g.setColor(Color.black);
					g.drawString(message1, x+15, y+14);
					g.drawString(message2, x+15, (int)(y+14+bounds1.getHeight() + 3));
				}
			}
			for (Map.Entry<String, ImageInfo> entry : mImageInfos.entrySet())
			{
				final String hostName = entry.getKey();
				final ImageInfo imageInfo = entry.getValue();
				synchronized (imageInfo.mCurrentImageLock)
				{
					if (imageInfo.mCurrentImage != null)
					{
//						final int imageHeight = getHeight()-320-10;
//						final double factor = (1.0 * imageHeight / imageInfo.mCurrentImage.getHeight());
						final double factor = (1.0 * (getWidth()-20) / mImageInfos.size() / imageInfo.mCurrentImage.getWidth());
						final int startx = 10 + imageInfo.index * (getWidth()-20) / mImageInfos.size();
						g.drawImage(imageInfo.mCurrentImage, startx, IMAGE_START_Y+80, (int)(imageInfo.mCurrentImage.getWidth() * factor), (int)(imageInfo.mCurrentImage.getHeight() * factor), null);
						g.setFont(getFont().deriveFont(28.0f));
						g.setColor(imageInfo.mPreviousImageTstamp.isAfter(LocalDateTime.now().minusSeconds(30)) ? Color.green : Color.red);
						g.drawString(hostName + " / " + imageInfo.mImageTstamp, startx + 5, IMAGE_START_Y+120);
						g.setFont(getFont().deriveFont(12.0f));
						g.setColor(imageInfo.mRedColor > 0.5 ? Color.gray : Color.green);
						g.drawString("" + imageInfo.mRedColor, startx, IMAGE_START_Y);
						g.setColor(((imageInfo.mTotalDiff > WatcherRTServer.MIN_DIFF_SUSPICIOUS_MISS_TEA) && (imageInfo.mTotalDiff < WatcherRTServer.MAX_DIFF_SUSPICIOUS_MISS_TEA)) ? (imageInfo.mTotalDiff > WatcherRTServer.MIN_DIFF_SURE_MISS_TEA ? Color.red : Color.yellow) : Color.green);
						g.drawString("" + imageInfo.mTotalDiff, startx, IMAGE_START_Y+20);
						g.drawLine(150+startx, IMAGE_START_Y+20, (int)(150+startx+imageInfo.mTotalDiff/50000), IMAGE_START_Y+20);
						g.setColor(((imageInfo.mTotalStandardDev > WatcherRTServer.MIN_DEVIATION_MISS_TEA) && (imageInfo.mTotalStandardDev < WatcherRTServer.MAX_DEVIATION_MISS_TEA)) ? Color.red : Color.green);
						g.drawString("" + imageInfo.mTotalStandardDev, 20+startx, IMAGE_START_Y+40);
						g.drawLine(150+startx, IMAGE_START_Y+40, (int)(150+startx+imageInfo.mTotalStandardDev*2), IMAGE_START_Y+40);
						g.drawImage(imageInfo.mCurrentHist, startx, getHeight()-140, (getWidth()-20) / mImageInfos.size(), 100, null);
					}
				}
			}
//			g.setColor(Color.white);
//			g.drawRect(7, 7, 640, 460);
		}

		private String findWatchAt(int x, int y)
		{
			String watch = null;
			synchronized (mLines)
			{
				if ((x >= START_X) && (x <= START_X + MACHINE_NAME_WIDTH))
				{
					// we may have a machine
					int linenum = (y - START_Y) / (SQUARE_Y_SIZE + INTERVAL_Y_SQUARES);
					if ((linenum >= 0) && (linenum < mLines.size()))
					{
						for (String lineMachine : mLines.keySet())
						{
							if (linenum-- == 0)
							{
								watch = lineMachine;
								break;
							}
						}
					}
				}
				else if ((y >= START_Y) && (y <= START_Y + mLines.size() * (SQUARE_Y_SIZE + INTERVAL_Y_SQUARES)) && (x >= START_X + MACHINE_NAME_WIDTH))
				{
					// we may have a watch
					int linenum = (y - START_Y) / (SQUARE_Y_SIZE + INTERVAL_Y_SQUARES);
					String selected = null;
					for (String lineMachine : mLines.keySet())
					{
						if (linenum-- == 0)
						{
							selected = lineMachine;
							break;
						}
					}
					if (selected != null)
					// a line was selected, there might be a watch in the correct column
					{
						final SortedSet<String> line = mLines.get(selected);
						if (x <= START_X + MACHINE_NAME_WIDTH + line.size() * (SQUARE_X_SIZE + INTERVAL_X_SQUARES))
						{
							int colnum = (x - START_X - MACHINE_NAME_WIDTH) / (SQUARE_X_SIZE + INTERVAL_X_SQUARES);
							for (String w : line)
							{
								if (colnum-- == 0)
								{
									watch = w;
									break;
								}
							}
						}
					}
				}
			}
			return watch;
		}
	}

	private class DataCollector extends Thread
	{
		private Socket mSocket;
		private Clip sCurrentClip;
		public DataCollector(Socket pSocket)
		{
			super("Collector " + pSocket.getRemoteSocketAddress().toString());
			mSocket = pSocket;
		}
		@Override
		public void run()
		{
			super.run();
			String name = null;
			try
			{
				final ObjectInputStream is = new ObjectInputStream(mSocket.getInputStream());
				name = (String)is.readObject();
//				final ObjectOutputStream os = new ObjectOutputStream(mSocket.getOutputStream());
//				os.writeChars(mAcceptImages ? "1" : "0");
				System.out.println(SimpleDateFormat.getInstance().format(new Date()) + ": received connection from " + name);
				synchronized (mConnected)
				{
					for (String n : name.split(","))
						mConnected.add(n);
					mConnected.add(MISS_TEA_STATE);
					mConnected.add(AGGR_STATE);
				}
				do
				{
					final Object obj = is.readObject();
					if (obj instanceof Measurement)
					{
						final Measurement m = (Measurement)obj;
//						System.out.println(name + ": Received measurement " + m.getValue() + " for " + Watched.getWatchedById(m.getIdWatch()).getLongName());
						synchronized (mMeasurements)
						{
							mMeasurements.put(m.getIdWatch(), m);
						}
					}
					else if (obj instanceof ImageData)
					{
						final ImageData imageData = (ImageData)obj;
						final InputStream iis = new ByteArrayInputStream(imageData.imageData);
						final BufferedImage bi = ImageIO.read(iis);
						final ImageInfo imageInfo = mImageInfos.get(name);
						if (imageInfo != null)
						{
							final BufferedImage newImageRotatedCropped = ("Pi3 Water Heater".equals(name) ? MissTeaImageHelper.createRotatedCropped(bi) : bi);
							final BufferedImage adjusted = MissTeaImageHelper.createHistogramAdjusted(newImageRotatedCropped);
							double[] diffs = null;
							if (imageInfo.mPreviousImage != null)
							{
								final BufferedImage diffImage = MissTeaImageHelper.createDiffImage(imageInfo.mPreviousImage, adjusted);
								diffs = MissTeaImageHelper.computeTotalDiff(diffImage);
							}
							final BufferedImage histImage = MissTeaImageHelper.createHistogramImage(newImageRotatedCropped);
							final double[] colors = MissTeaImageHelper.computeColorBalance(adjusted);
							// more red colors => infrared
							// difference with previous has to be higher than normal but not too high either
							// max deviation also has to be within a certain range
							if ((colors[0] > 0.5) && (diffs != null) && (diffs[0] >= MIN_DIFF_SUSPICIOUS_MISS_TEA) && (diffs[0] <= MAX_DIFF_SUSPICIOUS_MISS_TEA) && (diffs[1] > MIN_DEVIATION_MISS_TEA) && (diffs[1] < MAX_DEVIATION_MISS_TEA))
//						if ((colors[0] > 0.5) && (diffs != null) && (diffs[0] >= 100000) && (diffs[0] <= 3000000) && (diffs[1] > 3.5) && (diffs[1] < 150))
							// this is the cat!
							{
								final HealthState hs = new HealthState(1, diffs[0] < MIN_DIFF_SURE_MISS_TEA ? "Miss Tea?" : null, diffs[0] >= MIN_DIFF_SURE_MISS_TEA ? "!!!MISS TEA!!!" : null);
								synchronized (mStates)
								{
									mStates.put(MISS_TEA_STATE, hs);
								}
								if (sCurrentClip == null)
									try
									{
										final AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(WatcherRTServer.class.getResourceAsStream("/catmeow.wav"));
										sCurrentClip = AudioSystem.getClip();
										sCurrentClip.open(audioInputStream);
										final FloatControl gainControl = (FloatControl) sCurrentClip.getControl(FloatControl.Type.MASTER_GAIN);
										gainControl.setValue(-10.0f); // Reduce volume by 10 decibels.
										sCurrentClip.start();
										sCurrentClip.addLineListener(new LineListener()
										{
											@Override
											public void update(LineEvent pEvent)
											{
												if (pEvent.getType().equals(LineEvent.Type.STOP))
												{
													sCurrentClip.close();
													sCurrentClip = null;
												}
											}
										});
									}
									catch (UnsupportedAudioFileException | LineUnavailableException e)
									{
										e.printStackTrace();
									}
							}
							else
								synchronized (mStates)
								{
									mStates.put(MISS_TEA_STATE, new HealthState(1, 1));
								}
	
							if ((diffs != null) && (diffs[0] > 250000) && (diffs[1] > 10))
							{
								String dirPath = "/home/jytou/bignas/homes/check3d/" + imageInfo.targetDir + "/" + imageInfo.mPreviousImageTstamp.format(DateTimeFormatter.ofPattern("yyyyMMdd")) + "/" + imageInfo.mPreviousImageTstamp.format(DateTimeFormatter.ofPattern("HH"));
								Path path = FileSystems.getDefault().getPath(dirPath);
								Files.createDirectories(path);
								Path file = Paths.get(dirPath, imageInfo.mPreviousImageTstamp.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")) + ".jpg");
	//							Files.write(file, mPreviousRaw);
								ImageIO.write(imageInfo.mPreviousRaw, "jpg", file.toFile());
	
								dirPath = "/home/jytou/bignas/homes/check3d/" + imageInfo.targetDir + "/" + imageData.tstamp.format(DateTimeFormatter.ofPattern("yyyyMMdd")) + "/" + imageData.tstamp.format(DateTimeFormatter.ofPattern("HH"));
								path = FileSystems.getDefault().getPath(dirPath);
								Files.createDirectories(path);
								file = Paths.get(dirPath, imageData.tstamp.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")) + ".jpg");
	//							Files.write(file, imageData.imageData);
								ImageIO.write(newImageRotatedCropped, "jpg", file.toFile());
							}
	
							synchronized (imageInfo.mCurrentImageLock)
							{
								imageInfo.mCurrentImage = adjusted;
								imageInfo.mCurrentHist = histImage;
								imageInfo.mPreviousImage = adjusted;
								imageInfo.mPreviousRaw = newImageRotatedCropped;
								imageInfo.mPreviousImageTstamp = imageData.tstamp;
								imageInfo.mImageTstamp = imageData.tstamp.format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"));
								if (diffs != null)
								{
									imageInfo.mTotalDiff = diffs[0];
									imageInfo.mTotalStandardDev = diffs[1];
								}
								imageInfo.mRedColor = colors[0] * 100;
							}
							repaint();
						}
					}
				}
				while (true);
			}
			catch (IOException | ClassNotFoundException| NullPointerException e)
			{
				System.err.println(SimpleDateFormat.getInstance().format(new Date()) + ": lost connection with " + name + " (" + mSocket.getRemoteSocketAddress().toString() + ")");
				if (name != null)
					synchronized (mConnected)
					{
						for (String n : name.split(","))
							mConnected.remove(n);
					}
//				e.printStackTrace();
			}
		}
	}

	private final AtomicInteger mRunningThreads = new AtomicInteger(0);

	// feedback from the user to ignore or mute lines
	BlockingQueue<Integer> mFeedbacks = new LinkedBlockingQueue<>();
	class FeedbackWatcher extends Thread
	{
		public FeedbackWatcher()
		{
			super("FeedbackWatcher");
		}
		@Override
		public void run()
		{
			super.run();
			mRunningThreads.incrementAndGet();
			try
			{
				while (true)
				{
					final int feedback = mFeedbacks.take();
					if (feedback == 0)
						break;
					int minutes;
					String strTime;
					switch (Math.abs(feedback))
					{
					case 1:
						minutes = 5;
						strTime = "5 min";
						break;
					case 2:
						minutes = 60 * 2;
						strTime = "2 hours";
						break;
					case 3:
						minutes = 60 * 10;
						strTime = "10 hours";
						break;
					case 4:
						minutes = 60 * 24 * 2;
						strTime = "2 days";
						break;
					case 5:
						minutes = 60 * 24 * 70;
						strTime = "1 week";
						break;

					default:
						System.err.println("Unknown");
						continue;
					}
					System.out.println((feedback > 0 ? "Muting" : "Snoozing") + " for " + strTime);
					synchronized (mMainErrors)
					{
						for (String shortName : new HashSet<>(mMainErrors.keySet()))
							suspendFor(shortName, minutes, feedback > 0 ? SuspendType.MUTE : SuspendType.SNOOZE);
					}
				}
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			finally
			{
				mRunningThreads.decrementAndGet();
			}
		}
	}

	private final AtomicBoolean mQuitMessageEpaperShower = new AtomicBoolean(false);
	public WatcherRTServer(boolean pAcceptImages) throws IOException, ClassNotFoundException, SQLException
	{
		super("Watcher RT Server");
		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent pEvent)
			{
				super.windowClosing(pEvent);
				if (mGpioController != null)
					mGpioController.quit();
				try
				{
					mFeedbacks.put(0);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				mQuitMessageEpaperShower.set(true);
				while (mRunningThreads.get() > 0)
					try
					{
						Thread.sleep(1000);
					}
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				System.exit(0);
			}
		});
		mAcceptImages = pAcceptImages;
		if (mAcceptImages)
		{
			mImageInfos.put("Pi3 Water Heater", new ImageInfo("Pi3 Water Heater", 0, "laundry"));
			mImageInfos.put("Pi3 Garage Black", new ImageInfo("Pi3 Garage Black", 1, "grgblk"));
			mImageInfos.put("Pi3 Garage Silver", new ImageInfo("Pi3 Garage Silver", 2, "grgslv"));
		}
		setSize(480, 300);
		try
		{
			mGpioController = new MyGPIOController(mFeedbacks);
		}
		catch (Exception e)
		{
			// we don't have it because we're on the wrong architecture!
			System.err.println("*** No GPIO support ***");
		}
		mPopupMenu = new JPopupMenu();
		final JMenuItem menuItemEnableDisable = new JMenuItem("Disable/Enable");
		mPopupMenu.add(menuItemEnableDisable);
		menuItemEnableDisable.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				new SwingWorker<Integer, Integer>()
				{
					@Override
					protected void done()
					{
						super.done();
						repaint();
					}

					@Override
					protected Integer doInBackground() throws Exception
					{
						String machineName;
						synchronized (mLines)
						{
							machineName = mLines.get(mCurrentSelectedWatch) == null ? null : mCurrentSelectedWatch;
						}
						if (machineName == null)
						{
							// it must be a watch
							final Watched watch = Watched.getWatchedByShortName(mCurrentSelectedWatch);
							synchronized (watch)
							{
								if ((watch.getState() == Watched.STATE_DISABLED) || (watch.getState() == Watched.STATE_ENABLED))
								{
									final int newState = watch.getState() == Watched.STATE_DISABLED ? Watched.STATE_ENABLED : Watched.STATE_DISABLED;
									Connection conn = null;
									try
									{
										conn = JDBCConnector.connect();
										conn.createStatement().execute("update watch set disabled=" + newState + " where id=" + watch.getId());
										conn.commit();
										watch.setState(newState);
									}
									catch (ClassNotFoundException | SQLException ex)
									{
										JOptionPane.showMessageDialog(WatcherRTServer.this, "Could not save new state: " + ex.getClass().getName() + " said " + ex.getMessage(), "Save Error", JOptionPane.ERROR_MESSAGE);
									}
									finally
									{
										if (conn != null)
											try
											{
												conn.close();
											}
											catch (SQLException e1)
											{
											}
									}
									if (newState == Watched.STATE_DISABLED)
										synchronized (mMainErrors)
										{
											mMainErrors.remove(watch.getShortName());
										}
								}
							}
						}
						else
						{
							// it is a machine
							final Connection conn = JDBCConnector.connect();
							final PreparedStatement ps = conn.prepareStatement("update source set disabled=? where loc_long=?");
							synchronized (mSuspendedRules)
							{
								if (mSuspendedRules.containsKey(machineName))
								{
									// machine is disabled, we are enabling it
									ps.setInt(1, 0);
									mSuspendedRules.remove(machineName);
								}
								else
								{
									// machine is enabled, we are disabling it
									ps.setInt(1, 1);
									mSuspendedRules.put(machineName, new SuspendedRule(null, SuspendType.FOREVER));
									synchronized (mStates)
									{
										mStates.put(machineName, new HealthState(1.0, 1.0));
									}
									synchronized (mMainErrors)
									{
										mMainErrors.remove(machineName);
									}
								}
							}
							ps.setString(2, machineName);
							ps.execute();
							conn.commit();
							conn.close();
						}
						return null;
					}
				}.execute();
			}
		});
		final JMenu menuItemSnooze = new JMenu("Snooze this for...");
		mPopupMenu.add(menuItemSnooze);
		addSnoozeMenuItems(menuItemSnooze, SNOOZE_ONE, SuspendType.SNOOZE);
		final JMenu menuItemLineSnooze = new JMenu("Snooze LINE for...");
		mPopupMenu.add(menuItemLineSnooze);
		addSnoozeMenuItems(menuItemLineSnooze, SNOOZE_LINE, SuspendType.SNOOZE);
//		final JMenu menuItemAllSnooze = new JMenu("Snooze ALL for...");
//		mPopupMenu.add(menuItemAllSnooze);
//		addSnoozeMenuItems(menuItemAllSnooze, SNOOZE_ALL, SuspendType.SNOOZE);
		final JMenu menuItemMute = new JMenu("Mute this for...");
		mPopupMenu.add(menuItemMute);
		addSnoozeMenuItems(menuItemMute, SNOOZE_ONE, SuspendType.MUTE);
		final JMenu menuItemLineMute = new JMenu("Mute LINE for...");
		mPopupMenu.add(menuItemLineMute);
		addSnoozeMenuItems(menuItemLineMute, SNOOZE_LINE, SuspendType.MUTE);
//		final JMenu menuItemAllMute = new JMenu("Mute ALL for...");
//		mPopupMenu.add(menuItemAllMute);
//		addSnoozeMenuItems(menuItemAllMute, SNOOZE_ALL, SuspendType.SILENCE);
		final JMenuItem menuItemReset = new JMenuItem("Reset defaults from DB");
		menuItemReset.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Watched.init();
				for (Watched w : Watched.getAllWatched())
					fillDefaults(w);
			}
		});
		mPopupMenu.add(menuItemReset);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(new MyPanel());
		setIconImage(ImageIO.read(getClass().getResourceAsStream("/ok.png")));
		Watched.init();
		synchronized (mLines)
		{
			for (Watched watched : Watched.getAllWatched())
			{
				fillDefaults(watched);
				final String machine = watched.getSensorLocation();
				SortedSet<String> line = mLines.get(machine);
				if (line == null)
					mLines.put(machine, line = new TreeSet<>());
				line.add(watched.getShortName());
				if ((watched.getLastUpdated() != null) && (watched.getCurVal() != null))
				{
					final int watchedId = watched.getId();
					mMeasurements.put(watchedId, new Measurement(watchedId, new Timestamp(watched.getLastUpdated().toEpochSecond()*1000), watched.getCurVal()));
					mStates.put(watched.getShortName(), new HealthState(watched.getCurVal(), 1.0));
				}
			}
			mLines.put(MISS_TEA_STATE, new TreeSet<>(Arrays.asList(new String[] {MISS_TEA_STATE})));
			mLines.put(AGGR_STATE, new TreeSet<>(Arrays.asList(new String[] {GAETANS_ROOMS_DOOR_ABBR})));
			Watched.sTestMode = true;// hack to add watches without saving them in DB
			Watched.registerWatch(GAETANS_ROOMS_DOOR_ABBR, GAETANS_ROOMS_DOOR, GAETANS_ROOMS_DOOR, AGGR_STATE, AGGR_STATE, "AGGR", "AGGR", GAETANS_ROOMS_DOOR_ABBR, GAETANS_ROOMS_DOOR, 300, -0.2, 2.0, 0.0, 2.0, 1.0, 0.0);
		}
		// Check machines
		final Connection conn = JDBCConnector.connect();
		final ResultSet rs = conn.createStatement().executeQuery("select loc_short, loc_long, disabled from source");
		final Set<String> existing = new HashSet<>();
		while (rs.next())
		{
			final String machineName = rs.getString("loc_long");
			existing.add(machineName);
			final int disabled = rs.getInt("disabled");
			if (disabled == 1)
			{
				synchronized (mSuspendedRules)
				{
					mSuspendedRules.put(machineName, new SuspendedRule(null, SuspendType.FOREVER));// never expire
				}
			}
		}
		rs.close();
		if (existing.size() < mLines.size())
		{
			final PreparedStatement ps = conn.prepareStatement("insert into source (loc_short, loc_long, disabled) values (?, ?, ?)");
			for (String machineName : mLines.keySet())
				if ((!MISS_TEA_STATE.equals(machineName)) && (!AGGR_STATE.equals(machineName)))
				{
					if (!existing.contains(machineName))
					{
						ps.setString(1, Watched.getWatchedByShortName(mLines.get(machineName).first()).getSensorLocShort());
						ps.setString(2, machineName);
						ps.setInt(3, 0);
						ps.execute();
					}
				}
			conn.commit();
			ps.close();
		}
		conn.close();
		final ServerSocket serverSocket = new ServerSocket(PORT);
		new Thread()
		{
			public void run()
			{
				while (true)
				{
					try
					{
						new DataCollector(serverSocket.accept()).start();
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}

			}
		}.start();

		new Thread()
		{
			public void run()
			{
				while (true)
				{
					Map<Integer, Measurement> measurements;
					synchronized (mMeasurements)
					{
						measurements = new TreeMap<>(mMeasurements);
					}
					for (Watched watched : Watched.getAllWatched())
					{
						// remove expired suspended rules
						removeExpiredSuspension(watched.getShortName());

						boolean connected;
						synchronized (mConnected)
						{
							connected = mConnected.contains(watched.getSensorLocation());
						}
						synchronized (watched)
						{
							if ((watched.getState() == Watched.STATE_ENABLED) && connected)
							{
								Measurement val = measurements.get(watched.getId());
								if (GAETANS_ROOMS_DOOR_ABBR.equals(watched.getShortName()))
								{
									val = null;
									final Double gaetanDoorOpenValue = mStates.get("sengbo").value;
									if (gaetanDoorOpenValue != null)
									{
										final boolean isClosedDoor = Math.abs(gaetanDoorOpenValue.doubleValue()) < 0.1;
										final Double kitchenBalcony = mStates.get("senkbt").value;
										if (kitchenBalcony != null)
										{
											final Double gaetansRoom = mStates.get("sengrt").value;
											final Double gaetansRoomDeep = mStates.get("sendgt").value;
											Double roomTemp = null;
											if (gaetansRoom != null)
											{
												if (gaetansRoomDeep == null)
													roomTemp = gaetansRoom;
												else
													roomTemp = (gaetansRoom * 3 + gaetansRoomDeep) / 4;
											}
											else if (gaetansRoomDeep != null)
												roomTemp = gaetansRoomDeep;
											if (roomTemp != null)
											{
												final double diff = kitchenBalcony.doubleValue() - roomTemp.doubleValue();
												if (isClosedDoor)
												// anything positive indicates hotter outside - good, should remained closed
													val = new Measurement(watched.getShortName(), Math.min(1, diff / 2 + 0.25));
												else
												// anything negative indicates colder outside - good
													val = new Measurement(watched.getShortName(), Math.min(1, -diff / 2 - 0.25));
											}
										}
									}
								}
								if (val == null)
								{
									if (watched.getState() != Watched.STATE_DISABLED)
										reportError(watched.getShortName(), 0.0, "Missing data", ERR_ERROR);
								}
								else
								{
									synchronized (mMainErrors)
									{
										mMainErrors.remove(watched.getShortName());
									}

									Double value = val.getValue();
									if (watched.getFactor1() != null)
										value *= watched.getFactor1();
									if (watched.getOffset2() != null)
										value += watched.getOffset2();
									HealthState health = new HealthState(value, 1.0);

									final long elapsed = val.getTimestamp().toLocalDateTime().until(LocalDateTime.now(), ChronoUnit.MINUTES);
									if ((watched.getIntervalWarn() != null) && (watched.getIntervalWarn() != -1) && (elapsed > watched.getIntervalWarn()))
									{
										if (watched.getState() != Watched.STATE_DISABLED)
										{
											if (elapsed > watched.getIntervalWarn() * 3)
												health.error = "Data too old: " + intervalToString(val.getTimestamp().toLocalDateTime().until(LocalDateTime.now(), ChronoUnit.MILLIS));
											else
												health.warning = "Data old: " + intervalToString(val.getTimestamp().toLocalDateTime().until(LocalDateTime.now(), ChronoUnit.MILLIS));
										}
									}

									if ((watched.getWarnMin() != null) && (value < watched.getWarnMin()))
										health.warning = "Value " + String.format("%1$.2f", value) + " < min";
									if ((watched.getWarnMax() != null) && (value > watched.getWarnMax()))
										health.warning = "Value " + String.format("%1$.2f", value) + " > max";
									if ((watched.getMax() != null) && (value > watched.getMax()))
										health.error = "Value " + String.format("%1$.2f", value) + " > max";
									else if ((watched.getMin() != null) && (value < watched.getMin()))
										health.error = "Value " + String.format("%1$.2f", value) + " < min";
									if ((watched.getMax() != null) && (watched.getMin() != null) && (health.error == null))
									{
										final double middle = (watched.getMax() + watched.getMin()) / 2;
										final double th1 = watched.getMin().intValue() == 0 ? -1 : (watched.getMin() + middle) / 2;
										final double th2 = (watched.getMax() + middle) / 2;
										if (value < th1)
											health.health = (value - watched.getMin()) / (th1 - watched.getMin());
										else if (value > th2)
											health.health = (watched.getMax() - value) / (watched.getMax() - th2);
									}
									if (health.error != null)
										reportError(watched.getShortName(), value, health.error, ERR_ERROR);
									else
									{
										synchronized (mStates)
										{
											mStates.put(watched.getShortName(), health);
										}
										synchronized (mSuspendedRules)
										{
											if ((health.warning != null) && (!mSuspendedRules.containsKey(watched.getShortName())))
												reportError(watched.getShortName(), value, health.warning, ERR_WARNING);
										}
									}
									synchronized (mSuspendedRules)
									{
										if (mSuspendedRules.containsKey(watched.getShortName()) && (health.error == null) && (health.warning == null) && (mSuspendedRules.get(watched.getShortName()).type.equals(SuspendType.SNOOZE)))
											mSuspendedRules.remove(watched.getShortName());
									}
								}
							}
						}
					}
					synchronized (mStates)
					{
						if (mStates.containsKey(MISS_TEA_STATE))
						{
							final HealthState ms = mStates.get(MISS_TEA_STATE);
							if (ms.error != null)
								mMainErrors.put(MISS_TEA_STATE, new WatchErrorMessage(MISS_TEA_STATE, ms.error, ERR_ERROR));
							else if (ms.warning != null)
								mMainErrors.put(MISS_TEA_STATE, new WatchErrorMessage(MISS_TEA_STATE, ms.warning, ERR_WARNING));
						}
					}
					SortedSet<String> machines;
					synchronized (mLines)
					{
						machines = new TreeSet<>(mLines.keySet());
					}
					synchronized (mConnected)
					{
						for (String machine : machines)
						{
							removeExpiredSuspension(machine);
							if (!mConnected.contains(machine))
								reportError(machine, 0.0, "Not responding!", ERR_ERROR);
							else
							{
								synchronized (mStates)
								{
									mStates.put(machine, new HealthState(1.0, 1.0));
								}
								synchronized (mSuspendedRules)
								{
									mSuspendedRules.remove(machine);
								}
								synchronized (mMainErrors)
								{
									mMainErrors.remove(machine);
								}
							}
						}
					}
					int errs = 0;
					synchronized (mMainErrors)
					{
						for (WatchErrorMessage wem : mMainErrors.values())
							errs |= wem.errType;
					}
					mCurrentState.set(errs);
					try
					{
						setIconImage(ImageIO.read(getClass().getResourceAsStream("/" + ((errs & ERR_ERROR) != 0 ? "error" : (errs & ERR_WARNING) != 0 ? "warning" : "ok") + ".png")));
					}
					catch (IOException e1)
					{
						e1.printStackTrace();
					}

					if (mGpioController != null)
					// Show LEDs
						mGpioController.setCurrentState((errs & ERR_ERROR) != 0 ? MyGPIOController.State.RED : ((errs & ERR_WARNING) != 0 ? MyGPIOController.State.ORANGE : MyGPIOController.State.GREEN));
					repaint();
					try
					{
						Thread.sleep(700);
					}
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			}
		}.start();
		new FeedbackWatcher().start();
		new MessageEpaperShower(mRunningThreads, mMainErrors, mGpioController.mRoomIsDark, mQuitMessageEpaperShower, mGpioController.mPresenceDetected, mGpioController.mDisabled);// it starts by itself if needed
		setVisible(true);
	}

	public static String intervalToString(long millis)
	{
		final StringBuilder buf = new StringBuilder();
		int precise = 2;// only two values maximum

		final long days = millis / (24 * 60 * 60 * 1000);
		if (days != 0)
		{
			buf.append(days).append("d ");
			precise--;
		}

		final long hours = (millis / (60 * 60 * 1000)) % 24;
		if ((precise < 2) || (hours != 0))
		{
			buf.append(hours).append("h ");
			precise--;
		}
		else if (precise < 2)
			precise--;

		final long minutes = (millis / (60 * 1000)) % 60;
		if (((precise < 2) || (minutes != 0)) && (precise > 0))
		{
			buf.append(minutes).append("m ");
			precise--;
		}
		else if (precise < 2)
			precise--;

		final long seconds = (millis / (1000)) % 60;
		if (precise == 2)
			buf.append(seconds).append("s ");

//		buf.append(millis % 1000).append("ms");

		return buf.toString();
	}

	private void addSnoozeMenuItems(final JMenu pMenuItemSnooze, int pSnoozeScope, SuspendType pSuspendType)
	{
		addSnoozeMenuItem(pMenuItemSnooze, "1 minute", 1, pSnoozeScope, pSuspendType);
		addSnoozeMenuItem(pMenuItemSnooze, "2 minutes", 2, pSnoozeScope, pSuspendType);
		addSnoozeMenuItem(pMenuItemSnooze, "5 minutes", 5, pSnoozeScope, pSuspendType);
		addSnoozeMenuItem(pMenuItemSnooze, "10 minutes", 10, pSnoozeScope, pSuspendType);
		addSnoozeMenuItem(pMenuItemSnooze, "30 minutes", 30, pSnoozeScope, pSuspendType);
		addSnoozeMenuItem(pMenuItemSnooze, "1 hour", 60, pSnoozeScope, pSuspendType);
		addSnoozeMenuItem(pMenuItemSnooze, "2 hours", 120, pSnoozeScope, pSuspendType);
		addSnoozeMenuItem(pMenuItemSnooze, "5 hours", 60*5, pSnoozeScope, pSuspendType);
		addSnoozeMenuItem(pMenuItemSnooze, "10 hours", 60*10, pSnoozeScope, pSuspendType);
		addSnoozeMenuItem(pMenuItemSnooze, "1 day", 60*24, pSnoozeScope, pSuspendType);
		addSnoozeMenuItem(pMenuItemSnooze, "2 days", 60*24*2, pSnoozeScope, pSuspendType);
		addSnoozeMenuItem(pMenuItemSnooze, "1 week", 60*24*7, pSnoozeScope, pSuspendType);
		addSnoozeMenuItem(pMenuItemSnooze, "1 month", 60*24*30, pSnoozeScope, pSuspendType);
	}

	protected void removeExpiredSuspension(String pRule)
	{
		synchronized (mSuspendedRules)
		{
			SuspendedRule suspended = mSuspendedRules.get(pRule);
			if ((suspended != null) && (suspended.end != null) && (!SuspendType.FOREVER.equals(suspended.end)) && (suspended.end.isBefore(LocalDateTime.now())))
				mSuspendedRules.remove(pRule);
		}
	}

	private final static int SNOOZE_ONE = 0;
	private final static int SNOOZE_LINE = 1;
	private final static int SNOOZE_ALL = 2;

	private void addSnoozeMenuItem(JMenu pMenuItemSnooze, String pMenuLabel, int pNbMinutes, int pSnoozeScope, SuspendType pSuspendType)
	{
		JMenuItem mi = new JMenuItem(pMenuLabel);
		pMenuItemSnooze.add(mi);
		mi.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if (pSnoozeScope == SNOOZE_ONE)
					suspendFor(mCurrentSelectedWatch, pNbMinutes, pSuspendType);
				else if (SNOOZE_LINE == pSnoozeScope)
				{
					String lineFound = null;
					synchronized (mLines)
					{
						for (String line : mLines.keySet())
						{
							final SortedSet<String> watches = mLines.get(line);
							for (String watch : watches)
							{
								if (mCurrentSelectedWatch.equals(watch))
								{
									lineFound = line;
									break;
								}
							}
							if (lineFound != null)
								break;
						}
						synchronized (mStates)
						{
							for (String watch : mLines.get(lineFound))
							{
								final HealthState state = mStates.get(watch);
								if (state != null)
								{
									if ((state.error != null) || (state.warning != null))
										suspendFor(watch, pNbMinutes, pSuspendType);
								}
							}
						}
					}
				}
				else
				{
					// snooze all
					synchronized (mLines)
					{
						for (String line : mLines.keySet())
						{
							final SortedSet<String> watches = mLines.get(line);
							for (String watch : watches)
								synchronized (mStates)
								{
									final HealthState state = mStates.get(watch);
									if (state != null)
									{
										if ((state.error != null) || (state.warning != null))
											suspendFor(watch, pNbMinutes, pSuspendType);
									}
								}
						}
					}
				}
			}
		});
	}

	protected int reportError(String pRule, Double pValue, String pMessage, int pErrorLevel)
	{
		SuspendedRule suspended;
		synchronized (mSuspendedRules)
		{
			suspended = mSuspendedRules.get(pRule);
		}
		synchronized (mStates)
		{
			mStates.put(pRule, new HealthState(pValue, pErrorLevel == ERR_ERROR ? pMessage : null, pErrorLevel == ERR_WARNING ? pMessage : null));
		}
		synchronized (mMainErrors)
		{
			if (suspended == null)
			// not suspended
			{
				mMainErrors.put(pRule, new WatchErrorMessage(pRule, pMessage, pErrorLevel));
				return ERR_ERROR;
			}
			else
			{
				// suspended, no error
				mMainErrors.remove(pRule);
				return 0;
			}
		}
	}

	private void fillBounds(Watched pWatched, Double pMin, Double pMax, Double pWarnMin, Double pWarnMax)
	{
		if (pWatched.getMin() == null)
			pWatched.setMin(pMin);
		if (pWatched.getMax() == null)
			pWatched.setMax(pMax);
		if (pWatched.getWarnMin() == null)
			pWatched.setWarnMin(pWarnMin);
		if (pWatched.getWarnMax() == null)
			pWatched.setWarnMax(pWarnMax);
	}

	private void fillDefaults(Watched pWatched)
	{
		if (pWatched.getIntervalWarn() == null)
		{
			if (pWatched.getShortName().startsWith("sen"))
				pWatched.setIntervalWarn(360);
			else if ("dt".equals(pWatched.getSensTypeShort()))
				pWatched.setIntervalWarn(10);
			else if (pWatched.getSensTypeShort().startsWith("ups"))
				pWatched.setIntervalWarn(5);
			else if (pWatched.getSensTypeShort().startsWith("ms"))
				pWatched.setIntervalWarn(15);
			else if (pWatched.getSensTypeShort().equals("md") || pWatched.getSensTypeShort().equals("zfs"))
				pWatched.setIntervalWarn(120);
			else if (pWatched.getSensTypeShort().startsWith("g"))
				pWatched.setIntervalWarn(5*60);
			else if (pWatched.getShortName().startsWith("pow"))
				pWatched.setIntervalWarn(15*60);
			else if (pWatched.getSensTypeShort().equals("np"))
				pWatched.setIntervalWarn(5*60);
			else if (pWatched.getShortName().contains("sds011"))
				pWatched.setIntervalWarn(40);
			else
				pWatched.setIntervalWarn(5);
		}
		switch (pWatched.getSensTypeShort())
		{
		case "b":
			// battery of some device, should be between 30 and 100
			fillBounds(pWatched, 30.0, 140.0, 40.0, 140.0);
			break;
		case "cl":
			// This is a CPU load, best not to go over 100
			fillBounds(pWatched, 0.0, 300.0, 0.0, 200.0);
			break;
		case "cput":
			// This is a CPU temperature, better not go over 75 - but you should modulate for each CPU
			fillBounds(pWatched, -20.0, 80.0, -10.0, 75.0);
			break;
		case "dt":
			// This is a hard disk temperature
			fillBounds(pWatched, -10.0, 65.0, -5.0, 60.0);
			break;
		case "f":
			// This is a fan, typical rotation is between 1000 and 2500 - but can depend on every single fan
			fillBounds(pWatched, 800.0, 3000.0, 1000.0, 2500.0);
			break;
		case "h":
			// Humidity, not sure what to do here - if values are outside those ranges, something must be very wrong
			fillBounds(pWatched, 0.0, 100.0, 15.0, 90.0);
			break;
		case "mmr":
			// memory used
			fillBounds(pWatched, 0.0, 95.0, 0.0, 85.0);
			break;
		case "mms":
			// memory used including SWAP
			fillBounds(pWatched, 0.0, 80.0, 0.0, 70.0);
			break;
		case "msl":
			// mysql lag time
			fillBounds(pWatched, -5000.0, 15000.0, 0.0, 12000.0);
			break;
		case "syst":
			// System temperature
			fillBounds(pWatched, -20.0, 75.0, -10.0, 65.0);
			break;
		case "t":
			// Ambient temperature
			fillBounds(pWatched, -25.0, 50.0, -15.0, 40.0);
			break;
		case "w":
			// Water leak
			fillBounds(pWatched, 0.0, 0.5, 0.0, 0.5);
			break;
		case "upsc":
			// UPS charge
			fillBounds(pWatched, 30.0, 130.0, 40.0, 130.0);
			break;
		case "upsl":
			// UPS load
			fillBounds(pWatched, 0.0, 70.0, 0.0, 50.0);
			break;
		case "gf":
			// GPU Fans
			fillBounds(pWatched, 10.0, 120.0, 20.0, 120.0);
			break;
		case "gt":
			// GPU temperature
			fillBounds(pWatched, 0.0, 80.0, 0.0, 70.0);
			break;
		case "gm":
			// GPU memory
			fillBounds(pWatched, 0.0, 130.0, 0.0, 110.0);
			break;
		case "gl":
			// GPU load
			fillBounds(pWatched, 0.0, 130.0, 0.0, 110.0);
			break;
		case "gw":
			// GPU power
			fillBounds(pWatched, 0.0, 150.0, 0.0, 120.0);
			break;
		case "pow":
			// meross plugs
			fillBounds(pWatched, 0.0, 1000.0, 0.0, 500.0);
			break;
		case "np":
			// network ping
			fillBounds(pWatched, -2000.0, 8000.0, -1000.0, 4000.0);
			break;
		case "http":
			// http request
			fillBounds(pWatched, -4999.0, 20000.0, -3000.0, 10000.0);
			break;
		case "25":
			// small particles 2.5
			fillBounds(pWatched, 0.0, 30.0, 0.1, 40.0);
			break;
		case "10":
			// small particles 10
			fillBounds(pWatched, 0.0, 50.0, 0.05, 70.0);
			break;
		default:
			if ((pWatched.getMin() == null) && (pWatched.getMax() == null))
				System.err.println("Unsure what to do with measure for " + pWatched.getLongName());
			break;
		}
	}

	private void suspendFor(final String pWatchShortName, int pNbMinutes, SuspendType pSuspendType)
	{
		synchronized (mSuspendedRules)
		{
			if (mSuspendedRules.containsKey(pWatchShortName))
				mSuspendedRules.remove(pWatchShortName);
			mSuspendedRules.put(pWatchShortName, new SuspendedRule(LocalDateTime.now().plusMinutes(pNbMinutes), pSuspendType));
			synchronized (mMainErrors)
			{
				mMainErrors.remove(pWatchShortName);
			}
		}
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException
	{
		JDBCConnector.init(args[0], args[1], args[2]);
		new WatcherRTServer(args.length == 3);
	}
}
