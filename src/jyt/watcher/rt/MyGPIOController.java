package jyt.watcher.rt;

import java.time.LocalDateTime;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;

public class MyGPIOController
{
	public enum State {
		GREEN, ORANGE, RED, EMPTY
	};

	private GpioController mController;
	private GpioPinDigitalOutput mPinGreen;
	private GpioPinDigitalOutput mPinGreen2;
	private GpioPinDigitalOutput mPinOrange;
	private GpioPinDigitalOutput mPinRed;
	private GpioPinDigitalOutput mPinLightPaper;
	private GpioPinDigitalInput mPinLightSensor;
	private GpioPinDigitalInput mPinPresenceSensor;
	private State mCurrentState = State.EMPTY;
	private GpioPinDigitalInput mySnoozeSwitch;
	private GpioPinDigitalInput myMuteSwitch;
	private AtomicBoolean mQuitting = new AtomicBoolean(false);
	private Semaphore mRealQuit = new Semaphore(0);
	BlockingQueue<Integer> mFeedbacks;
	AtomicBoolean mRoomIsDark = new AtomicBoolean(false);
	private LocalDateTime mLastPresenceDetected = LocalDateTime.now();
	AtomicBoolean mPresenceDetected = new AtomicBoolean(true);
	AtomicBoolean mDisabled = new AtomicBoolean(false);

	private final static int SLEEP_TIME = 50;
	private final static int LOOPS_LED_REFRESH = 20;
	private final static int LOOP_RED_REFRESH = 12;

	class SwitchCounter
	{
		final int threshCounting = 20;
		int cnt = 0;
		boolean counting = false;
		boolean prevState = false;
		int clicks = 0;

		public int processState(boolean curState)
		{
			if (curState)
			{
				if (!prevState)
				{
					clicks++;
					counting = true;
//					System.out.println("clicks++");
				}
				if ((clicks == 1) && counting)
				{
					cnt++;
//					if (cnt % 10 == 0)
//						System.out.println("cnt " + cnt);
					if (cnt > 2000 / SLEEP_TIME)
					{
						System.out.println("found disable");
						cnt = 0;
						clicks = 0;
						counting = false;
						return -1;
					}
				}
			}
			else
			{
				if (prevState)
				{
					cnt = 0;
				}
				else
				{
					if (counting && (cnt++ >= threshCounting))
					{
						counting = false;
						System.out.println("Counted " + clicks);
						int c = clicks;
						clicks = 0;
						cnt = 0;
						return c;
					}
				}
			}

			prevState = curState;
			return 0;
		}

		public void reset()
		{
			prevState = false;
		}
	}

	public MyGPIOController(BlockingQueue<Integer> pFeedbacks)
	{
		mFeedbacks = pFeedbacks;
		try
		{
			mController = GpioFactory.getInstance();
			mPinGreen = mController.provisionDigitalOutputPin(RaspiPin.GPIO_00);
			mPinGreen2 = mController.provisionDigitalOutputPin(RaspiPin.GPIO_04);
			mPinOrange = mController.provisionDigitalOutputPin(RaspiPin.GPIO_03);
			mPinRed = mController.provisionDigitalOutputPin(RaspiPin.GPIO_01);
			mPinLightPaper = mController.provisionDigitalOutputPin(RaspiPin.GPIO_07);
			mPinLightSensor = mController.provisionDigitalInputPin(RaspiPin.GPIO_27);
			mPinPresenceSensor = mController.provisionDigitalInputPin(RaspiPin.GPIO_24);
			mPinGreen.setMode(PinMode.DIGITAL_OUTPUT);
			mPinGreen2.setMode(PinMode.DIGITAL_OUTPUT);
			mPinOrange.setMode(PinMode.DIGITAL_OUTPUT);
			mPinRed.setMode(PinMode.DIGITAL_OUTPUT);
			mPinLightPaper.setMode(PinMode.DIGITAL_OUTPUT);
			mPinLightSensor.setMode(PinMode.DIGITAL_INPUT);
			mPinPresenceSensor.setMode(PinMode.DIGITAL_INPUT);
			setCurrentState(State.EMPTY);
			mPinGreen.setState(false);
			mPinGreen2.setState(false);
			mPinOrange.setState(false);
			mPinRed.setState(false);
			mPinLightPaper.setState(false);

			myMuteSwitch = mController.provisionDigitalInputPin(RaspiPin.GPIO_22, PinPullResistance.PULL_UP);
			mySnoozeSwitch = mController.provisionDigitalInputPin(RaspiPin.GPIO_26, PinPullResistance.PULL_UP);
			final SwitchCounter muteSwitchCounter = new SwitchCounter();
			final SwitchCounter snoozeSwitchCounter = new SwitchCounter();

			new Thread()
			{
				public void run()
				{
					// for leds
					int i = 0;
					MyGPIOController.State s = MyGPIOController.State.EMPTY;
					GpioPinDigitalOutput currentPin = null;
					boolean isPaperLit = false;
					boolean wasPresent = false;
					LocalDateTime lastRoomDarkChange = LocalDateTime.now().minusDays(1);

					// for feedback counting
					while (!mQuitting.get())
					{
						// Make leds blink
						synchronized (MyGPIOController.this)
						{
							MyGPIOController.State s2 = mCurrentState;
							if (!mDisabled.get())
							{
								if (s != s2)
								{
									if (currentPin != null)
									{
										System.out.println("Disabling previous pin");
										currentPin.setState(false);
										mPinGreen2.setState(false);
									}
									i = 0;
									switch (s2)
									{
									case GREEN:
										currentPin = mPinGreen;
										break;
									case ORANGE:
										currentPin = mPinOrange;
										break;
									case RED:
										currentPin = mPinRed;
										break;
									case EMPTY:
										currentPin = null;
										break;
	
									default:
										break;
									}
									if (currentPin != null)
									{
										System.out.println("Set state " + s2.toString());
										currentPin.setState(true);
									}
									s = s2;
								}
								else
								{
									i++;
									if (i >= LOOPS_LED_REFRESH * 2)
										i = 0;
									if (MyGPIOController.State.RED.equals(s2))
										currentPin.setState(i % LOOP_RED_REFRESH < LOOP_RED_REFRESH / 2);
									else if (MyGPIOController.State.ORANGE.equals(s2))
										currentPin.setState(i < LOOPS_LED_REFRESH);
									else if (MyGPIOController.State.GREEN.equals(s2))
									{
										currentPin.setState(i < LOOPS_LED_REFRESH);
										mPinGreen2.setState(i >= LOOPS_LED_REFRESH);
									}
								}
	
								// Darkness detection
								final boolean isRoomDark = mPinLightSensor.getState().isHigh();
								if (isRoomDark != mRoomIsDark.get())
								{
									final LocalDateTime now = LocalDateTime.now();
									if ((lastRoomDarkChange == null) || (lastRoomDarkChange.plusSeconds(10).isBefore(now)))
									{
										mRoomIsDark.set(isRoomDark);
										lastRoomDarkChange = now;
										//System.out.println("Detected room " + (isRoomDark ? "dark" : "light"));
									}
								}
	
								// Human presence detection
								final boolean presenceDetected = mPinPresenceSensor.getState().isHigh();
								if (presenceDetected)
								{
									mLastPresenceDetected = LocalDateTime.now();
									if (!wasPresent)
									{
										mPresenceDetected.set(true);
										mPinLightPaper.setState(true);
									}
								}
								else if (mLastPresenceDetected.plusMinutes(3).isBefore(LocalDateTime.now()))
								// more than 3 minutes
								{
									if (wasPresent)
									{
										mPresenceDetected.set(false);
										mPinLightPaper.setState(false);
									}
								}
								wasPresent = presenceDetected;
	
								if ((MyGPIOController.State.RED.equals(s2) || MyGPIOController.State.ORANGE.equals(s2)) && (mPinLightSensor.getState().isHigh()))
								// light the e-paper panel
								{
									if (!isPaperLit)
									{
										//mPinLightPaper.setState(true);
										isPaperLit = true;
									}
								}
								else
								// turn off light
								{
									if (isPaperLit)
									{
										mPinLightPaper.setState(false);
										isPaperLit = false;
									}
								}
							}
						}

						// now count clicks if needed
						int snoozeClicks = snoozeSwitchCounter.processState(!mySnoozeSwitch.isHigh());
						final boolean curDisabled = mDisabled.get();
						if ((snoozeClicks == -1) || (curDisabled && (snoozeClicks == 2)))
						// 2 clicks also does the trick to enable back
						{
							// make sure we don't interpret them as snooze later
							snoozeClicks = -1;
							// we need to change the disabled status
							final boolean newDisabled = !mDisabled.get();
							mDisabled.set(newDisabled);
							if (newDisabled)
							{
								// need to disable everything
								mPinGreen.setState(false);
								mPinGreen2.setState(false);
								mPinOrange.setState(false);
								mPinRed.setState(false);
								mPinLightPaper.setState(false);
								wasPresent = false;
							}
							else
								s = MyGPIOController.State.EMPTY;
						}
						if (MyGPIOController.State.RED.equals(mCurrentState) || MyGPIOController.State.ORANGE.equals(mCurrentState))
						{
							if (snoozeClicks > 0)
								try
								{
									mFeedbacks.put(-snoozeClicks);
								}
								catch (InterruptedException e)
								{
									e.printStackTrace();
								}

							final int muteClicks = muteSwitchCounter.processState(!myMuteSwitch.isHigh());
							if (muteClicks > 0)
								try
								{
									mFeedbacks.put(muteClicks);
								}
								catch (InterruptedException e)
								{
									e.printStackTrace();
								}
						}
						else
						{
							muteSwitchCounter.reset();
						}

						try
						{
							Thread.sleep(SLEEP_TIME);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
					mRealQuit.release();
				}
			}.start();
		}
		catch (UnsatisfiedLinkError e)
		{
			// ignore if not on Raspi
			mRealQuit.release();
		}
	}

	public void quit()
	{
		mQuitting.set(true);
		synchronized (this)
		{
			mCurrentState = State.EMPTY;
		}
		try
		{
			mRealQuit.acquire();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		if (mPinGreen != null)
		{
			mPinGreen.setState(false);
			mPinGreen2.setState(false);
			mPinOrange.setState(false);
			mPinRed.setState(false);
			mPinLightPaper.setState(false);
			mController.unprovisionPin(mPinGreen);
			mController.unprovisionPin(mPinGreen2);
			mController.unprovisionPin(mPinOrange);
			mController.unprovisionPin(mPinRed);
			mController.unprovisionPin(mPinLightPaper);
			mController.unprovisionPin(mPinLightSensor);
			mController.unprovisionPin(mPinPresenceSensor);
			mController.unprovisionPin(myMuteSwitch);
			mController.unprovisionPin(mySnoozeSwitch);
		}
	}

	public void setCurrentState(State pState)
	{
		if (mController == null)
			return;
		synchronized (this)
		{
			mCurrentState = pState;
		}
	}
}
