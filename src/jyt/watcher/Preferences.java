package jyt.watcher;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Not used anymore.
 * @author jytou
 */
public class Preferences extends Thread
{
	private Map<String, Double> mValues = new HashMap<String, Double>();
	private static Preferences sPreferences;
	private IFeedbackListener mFeedbackListener;

	public synchronized static Preferences getPreferences(IFeedbackListener pFeedbackListener)
	{
		if (sPreferences == null)
			sPreferences = new Preferences(pFeedbackListener);
		return sPreferences;
	}

	public Preferences(IFeedbackListener pFeedbackListener)
	{
		super("Preferences");
		mFeedbackListener = pFeedbackListener;
		readPreferences();
		start();
	}

	public synchronized void readPreferences()
	{
		Connection connection = null;
		try
		{
			connection = JDBCConnector.connect();
			Statement s = connection.createStatement();
			try
			{
				ResultSet rs = s.executeQuery("select * from prefs");
				try
				{
					synchronized (mValues)
					{
						while (rs.next())
						{
							final String name = rs.getString("name");
							final double value = rs.getDouble("double_value");
							mValues.put(name, value);
						}
					}
				}
				finally
				{
					rs.close();
				}
				
			}
			finally
			{
				s.close();
			}
			mFeedbackListener.preferencesReadOk();
		}
		catch (SQLException e)
		{
			// cannot do anything here
			mFeedbackListener.preferencesReadError("Error while reading preferences", e);
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (connection != null)
				try
				{
					connection.close();
				}
				catch (SQLException e)
				{
					mFeedbackListener.preferencesReadError("Error while closing the connection for reading preferences", e);
				}
		}
	}

	@Override
	public void run()
	{
		super.run();
		while (true)
		{
			try
			{
				Thread.sleep(300000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			readPreferences();
		}
	}

	public Double getValue(String pName, Double pDefault)
	{
		Double value = null;
		synchronized (mValues)
		{
			value = mValues.get(pName);
		}
		if (value == null)
			return pDefault;
		else
			return value;
	}

	public Map<String, Double> getValues()
	{
		return mValues;
	}
}
