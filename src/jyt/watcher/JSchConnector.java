package jyt.watcher;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Properties;

import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class JSchConnector
{
	private OutputStream mOutputStream;
	private InputStreamReader mInputStream;
	private BufferedReader mReader;
	private BufferedWriter mWriter;
	private Session mSession;
	private ChannelShell mChannel;

	public JSchConnector(String user, String host, String password) throws JSchException, IOException
	{
		super();
		JSch jSch = new JSch();
		mSession = jSch.getSession(user, host);
		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");
		mSession.setConfig(config);
		mSession.setPassword(password);
		mSession.connect();
		mChannel = (ChannelShell) mSession.openChannel("shell");
		mChannel.setPtySize(150, 150, 640, 480);
		mChannel.connect();
		mInputStream = new InputStreamReader(mChannel.getInputStream());
		mReader = new BufferedReader(mInputStream);
		mOutputStream = mChannel.getOutputStream();
		mWriter = new BufferedWriter(new OutputStreamWriter(mOutputStream));
	}

	public OutputStream getOutputStream()
	{
		return mOutputStream;
	}
	public InputStreamReader getInputStream()
	{
		return mInputStream;
	}
	public BufferedReader getReader()
	{
		return mReader;
	}
	public BufferedWriter getWriter()
	{
		return mWriter;
	}

	public String readLine() throws IOException
	{
		final String line = mReader.readLine();
//		System.out.println(">> " + line);
//		System.out.flush();
		return line;
	}

	public void writeLine(String pLine) throws IOException
	{
//		System.out.println("<< " + pLine);
//		System.out.flush();
		mWriter.write(pLine);
		mWriter.newLine();
		mWriter.flush();
	}

	public void sendChar(char c) throws IOException
	{
//		System.out.println("<< " + c);
//		System.out.flush();
		mWriter.append(c);
		mWriter.flush();
	}

	public void close()
	{
		mChannel.disconnect();
		mSession.disconnect();
	}
}
